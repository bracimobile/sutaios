//
//  STHomeTopView.h
//  SUTA
//
//  Created by 张海军 on 2017/3/22.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface STHomeTopView : UIView
@property (weak, nonatomic) IBOutlet UILabel *upLabel;
@property (weak, nonatomic) IBOutlet UILabel *modelLabel; // 型号

+ (instancetype)homeTopView;

@end
