//
//  STFAQController.m
//  SUTA
//
//  Created by 张海军 on 2017/3/23.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import "STFAQController.h"

@interface STFAQController ()<UIWebViewDelegate>
/// wenbview
@property (nonatomic, strong) UIWebView *webView;
@end

@implementation STFAQController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupInit];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 初始化
- (void)setupInit
{
    self.NaviTitle.text = @"FAQ";
    self.NavBackView.backgroundColor = [UIColor blackColor];
    self.webView = [[UIWebView alloc] init];
    self.webView.delegate = self;
    
    //加载本地网页
    NSString* path = [[NSBundle mainBundle] pathForResource:@"FAQ" ofType:@".html"];
    NSURL* url = [NSURL fileURLWithPath:path];
    NSURLRequest* request = [NSURLRequest requestWithURL:url];
    
    //NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"https://www.baidu.com"]];
    [self.webView loadRequest:request];
    [self.view addSubview:self.webView];
    [SVProgressHUD showWithStatus:@"loading..."];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    self.webView.frame = CGRectMake(0, START_Y, self.view.hj_width, self.view.hj_height - START_Y);
}

#pragma mark - webviewDelegate
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [SVProgressHUD showWithStatus:@"loading..."];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [SVProgressHUD dismiss];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [ToastManager showToastWithString:@"Load Failed"];
}

@end
