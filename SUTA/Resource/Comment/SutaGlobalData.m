//
//  SutaGlobalData.m
//  remote
//
//  Created by ben on 17/3/1.
//  Copyright © 2017年 suta. All rights reserved.
//

#import "SutaGlobalData.h"

#pragma mark - 全局常量

// service uuid
NSString *const UUID_COMM_SERVICE = @"0000ff00-0000-1000-8000-00805f9b34fb";
// write characteristic uuid  特征  写的
NSString *const UUID_WRITE_CHARACTERISTIC = @"0000fff1-0000-1000-8000-00805f9b34fb";
// notify characteristic uuid  特征 notify的
NSString *const UUID_NOTIFY_CHARACTERISTIC = @"0000fff2-0000-1000-8000-00805f9b34fb";


//头部上
Byte headUpCommand[] = {0x01,0x00,0x00,0x00,0x01};
//头部下
Byte headDownCommand[] = {0x01,0x00,0x00,0x00,0x02};
//脚步上
Byte footUpCommand[] = {0x01,0x00,0x00,0x00,0x04};
//脚步下
Byte footDownCommand[] = {0x01,0x00,0x00,0x00,0x08};
//腰部上
Byte waistUpCommand[] = {0x01,0x00,0x00,0x00,0x10};
//腰部下
Byte waistDownCommand[] = {0x01,0x00,0x00,0x00,0x20};
//背部上
Byte backUpCommand[] = {0x01,0x00,0x00,0x00,0x40};
//背部下
Byte backDownCommand[] = {0x01,0x00,0x00,0x00,0x80};
//头部振动强度
Byte headVibrationCommand[] = {0x01,0x00,0x00,0x01,0x00};
//脚步振动强度
Byte footVibrationCommand[] = {0x01,0x00,0x00,0x02,0x00};
//振动模式 （MASSAGE）
Byte massageCommand[] = {0x01,0x00,0x00,0x04,0x00};
//零重力功能 (ZEROG)
Byte zeroGCommand[] = {0x01,0x00,0x00,0x08,0x00};
//平躺功能 (FLAT)
Byte flatCommand[] = {0x01,0x00,0x00,0x10,0x00};
//记忆位置功能1(M1)
Byte m1Command[] = {0x01,0x00,0x00,0x20,0x00};
//记忆位置功能2（M2）
Byte m2Command[] = {0x01,0x00,0x00,0x40,0x00};
//保存当前位置在M1
Byte saveInM1Command[] = {0x01,0x00,0x00,0x80,0x00};
//保存当前位置在M2
Byte saveInM2Command[] = {0x01,0x00,0x01,0x00,0x00};
//TIMER/ALL OFF
Byte timerAllOffCommand[] = {0x01,0x00,0x02,0x00,0x00};
//床灯功能
 Byte bedLampCommand[] = {0x01,0x00,0x04,0x00,0x00};
//恢复默认 MEMORY A
Byte recoverMemoryACommand[] = {0x01,0x00,0x08,0x00,0x00};
//保存当前位置在ZEROG
Byte savePositionInZeroGCommand[] = {0x01,0x00,0x20,0x00,0x00};

//左右床同时动作（l）
//左床动作(L)
//Byte saveInM2Command[] = {0x01,0x01,0x00,0x00,0x00};
//右床动作(R)
//Byte saveInM2Command[] = {0x01,0x02,0x00,0x00,0x00};


//抬起动作 每一个指令发完后，需要发抬起指令
Byte upliftCommand[] = {0x01,0x00,0x00,0x00,0x00};


//900i
NSString *const COMMAND = @"01";
//左右床同时动作
NSString *const LEFT_AND_RIGHT_BED =@"00";
//左床动作L
NSString *const LEFT_BED = @"01";
//右床动作R
NSString *const RIGHT_BED = @"02";
//头部上
NSString *const DATA_HEAD_UP = @"000001";
//头部下
NSString *const DATA_HEAD_DOWN = @"000002";
//脚步上
NSString *const DATA_FOOT_UP = @"000004";
//脚步下
NSString *const DATA_FOOT_DOWN = @"000008";
//腰部上
NSString *const DATA_WAIST_UP = @"000010";
//腰部下
NSString *const DATA_WAIST_DOWN = @"000020";
//背部上
NSString *const DATA_BACK_UP = @"000040";

//背部下
NSString *const DATA_BACK_DOWN = @"000080";

//头部震动强度
NSString *const DATA_HEAD_SHAKE = @"000100";


//脚步震动强度
NSString *const DATA_FOOT_SHAKE = @"000200";

//震动模式 MASSAGE
NSString *const DATA_MASSAGE = @"000400";

//零重力功能 ZEROG
NSString *const DATA_ZEROG = @"000800";

//平躺功能 flat
NSString *const DATA_FLAT = @"001000";

//记忆位置功能 1 （M1）
NSString *const DATA_M1 = @"002000";

//记忆位置功能 2 （M2）
NSString *const DATA_M2 = @"004000";

//保存当前位置在M1 长按3秒开启

NSString *const DATA_SAVE_M1 = @"008000";

//保存当前位置在M2 长按3秒开启

NSString *const DATA_SAVE_M2 = @"010000";

//TIMER/ALL OFF
NSString *const DATA_TIME_ALL_OFF = @"020000";
//床灯功能
NSString *const DATA_BED_LIGHT = @"040000";

//恢复默认 MEMORY A 长按6秒开启
NSString *const DATA_RECOVER_MEMORY_A = @"080000";

//保持当前位置在ZEROG 长按3秒开启
NSString *const DATA_SAVE_ZEROG= @"200000";




//500i
//振动模式 l
NSString *const DATA_SHAKE_MODEL_l= @"100000";
//振动模式 ll
NSString *const DATA_SHAKE_MODEL_ll= @"000400";
//振动模式 lll
NSString *const DATA_SHAKE_MODEL_lll= @"400000";
//记忆位置功能 MEMORY
NSString *const DATA_POSITION_MEMORY= @"002000";

//保存当前位置在MEMORY 长按3秒
NSString *const DATA_SAVE_MEMORY= @"008000";


//


CGFloat screenWidth = 0.0;
CGFloat screenHeight = 0.0;

CGFloat  centerScreenHeight = 0.0;
CGFloat  centerScreenWidth = 0.0;

CGFloat Blackheight = 0.0;
CGFloat Blackwidth =0.0;

CGFloat  leftMargin = 0.0;
CGFloat  rightMargin = 0.0;

CGFloat  imgBgX = 0.0;
CGFloat  imgBgY = 0.0;
CGFloat  imgBgWidth = 0.0;
CGFloat  imgBgHeight = 0.0;

CGFloat  LIRBgX = 0.0;
CGFloat  LIRBgY = 0.0;
CGFloat  LIRBgWidth = 0.0;
CGFloat  LIRBgHeight = 0.0;

CGFloat  LIRX = 0.0;
CGFloat  LIRY = 0.0;
CGFloat  LIRWidth = 0.0;
CGFloat  LIRHeight = 0.0;


//10 灯
 CGFloat  LightTenX = 0.0;
 CGFloat  LightTenY  = 0.0;
 CGFloat  LightTenWidth  = 0.0;
 CGFloat  LightTenHeight = 0.0;

 CGFloat  LightTenLabelX = 0.0;
 CGFloat  LightTenLabelY  =0.0;
 CGFloat  LightTenLabelWidth = 0.0;
 CGFloat  LightTenLabelHeight = 0.0;


//20 灯
 CGFloat  LightTwentyX = 0.0;
 CGFloat  LightTwentyY =0.0;
 CGFloat  LightTwentyWidth =0.0;
 CGFloat  LightTwentyHeight =0.0;

 CGFloat  LightTwentyLabelX =0.0;
 CGFloat  LightTwentyLabelY =0.0;
 CGFloat  LightTwentyLabelWidth =0.0;
 CGFloat  LightTwentyLabelHeight =0.0;


//30 灯
 CGFloat  LightThirtyX  = 0.0;
 CGFloat  LightThirtyY =0.0;
 CGFloat  LightThirtyWidth =0.0;
 CGFloat  LightThirtyHeight =0.0;

 CGFloat  LightThirtyLabelX = 0.0;
 CGFloat  LightThirtyLabelY = 0.0 ;
 CGFloat  LightThirtyLabelWidth =0.0 ;
 CGFloat  LightThirtyLabelHeight = 0.0;



CGFloat  headShakeX = 0.0;
CGFloat  headShakeY = 0.0;
CGFloat  headShakeWidth = 0.0;
CGFloat  headShakeHeight = 0.0;

CGFloat  switchModeX = 0.0;
CGFloat  switchModeY = 0.0;
CGFloat  switchModeWidth = 0.0;
CGFloat  swithcModeHeight = 0.0;

CGFloat  footShakeX = 0.0;
CGFloat  footShakeY = 0.0;
CGFloat  footShakeWidth = 0.0;
CGFloat  footShakeHeight = 0.0;


CGFloat  btnlX = 0.0;
CGFloat  btnlY = 0.0;
CGFloat  btnlWidth = 0.0;
CGFloat  btnlHeight = 0.0;

CGFloat  btnllX = 0.0;
CGFloat  btnllY = 0.0;
CGFloat  btnllWidth = 0.0;
CGFloat  btnllHeight = 0.0;

CGFloat  btnlllX = 0.0;
CGFloat  btnlllY = 0.0;
CGFloat  btnlllWidth = 0.0;
CGFloat  btnlllHeight = 0.0;

CGFloat  backUpX = 0.0;
CGFloat  backUpY = 0.0;
CGFloat  backUpWidth = 0.0;
CGFloat  backUpHeight = 0.0;

CGFloat  backDownX = 0.0;
CGFloat  backDownY = 0.0;
CGFloat  backDownWidth = 0.0;
CGFloat  backDownHeight = 0.0;

CGFloat  zerogX = 0.0;
CGFloat  zerogY = 0.0;
CGFloat  zerogWidth = 0.0;
CGFloat  zerogHeight = 0.0;

CGFloat  memoryX = 0.0;
CGFloat  memoryY = 0.0;
CGFloat  memoryWidth = 0.0;
CGFloat  memoryHeight = 0.0;


CGFloat  footUpX = 0.0;
CGFloat  footUpY = 0.0;
CGFloat  footUpWidth = 0.0;
CGFloat  footUpHeight = 0.0;

CGFloat  footDownX = 0.0;
CGFloat  footDownY = 0.0;
CGFloat  footDownWidth = 0.0;
CGFloat  footDownHeight = 0.0;


CGFloat  btnMinusX = 0.0;
CGFloat  btnMinusY = 0.0;
CGFloat  btnMinusWidth = 0.0;
CGFloat  btnMinusHeight = 0.0;


CGFloat  headUpX = 0.0;
CGFloat  headUpY = 0.0;
CGFloat  headUpWidth = 0.0;
CGFloat  headUpHeight = 0.0;

CGFloat  headDownX = 0.0;
CGFloat  headDownY = 0.0;
CGFloat  headDownWidth = 0.0;
CGFloat  headDownHeight = 0.0;


CGFloat  waistUpX = 0.0;
CGFloat  waistUpY = 0.0;
CGFloat  waistUpWidth = 0.0;
CGFloat  waistUpHeight = 0.0;


CGFloat  waistDownX = 0.0;
CGFloat  waistDownY = 0.0;
CGFloat  waistDownWidth = 0.0;
CGFloat  waistDownHeight = 0.0;

CGFloat  btnLX = 0.0;
CGFloat  btnLY = 0.0;
CGFloat  btnLWidth = 0.0;
CGFloat  btnLHeight = 0.0;

CGFloat  btnCenterLX = 0.0;
CGFloat  btnCenterLY = 0.0;
CGFloat  btnCenterLWidth = 0.0;
CGFloat  btnCenterLHeight = 0.0;

CGFloat  btnRX = 0.0;
CGFloat  btnRY = 0.0;
CGFloat  btnRWidth = 0.0;
CGFloat  btnRHeight = 0.0;



@implementation SutaGlobalData

+ (NSData *) combineCommand : (NSString *)command middleCommand
                            : (NSString *)middle lastCommand
                            : (NSString *)dataLast;
{
    NSString *combineString =
            [[NSString alloc]initWithFormat:@"%@%@%@", command, middle, dataLast];
    MYLog(@"combineCommand -- %@",combineString);
    NSData *myData = [self hexToBytes:combineString];
    //NSData *data = [combineString dataUsingEncoding:NSUTF8StringEncoding];
    //NSData *combineData = [NSData alloc] initWithBytes:combineByte length:5
    MYLog(@"mydata ==%@",myData.description);
    return myData;
}


+(NSData*) hexToBytes : (NSString *) originalText  {
    NSMutableData* data = [NSMutableData data];
    int idx;
    for (idx = 0; idx+2 <= originalText.length; idx+=2) {
        NSRange range = NSMakeRange(idx, 2);
        NSString* hexStr = [originalText substringWithRange:range];
        NSScanner *scanner = [NSScanner scannerWithString:hexStr];
        unsigned int intValue;
        [scanner scanHexInt:&intValue];
        [data appendBytes:&intValue length:1];
    }
    return data;
}


//+ (NSString *)convertDataToHexStr:(NSData *)data {
//    if (!data || [data length] == 0) {
//        return @"";
//    }
//    NSMutableString *string = [[NSMutableString alloc] initWithCapacity:[data length]];
//    
//    [data enumerateByteRangesUsingBlock:^(const void *bytes, NSRange byteRange, BOOL *stop) {
//        unsigned char *dataBytes = (unsigned char*)bytes;
//        for (NSInteger i = 0; i < byteRange.length; i++) {
//            NSString *hexStr = [NSString stringWithFormat:@"%x", (dataBytes[i]) & 0xff];
//            if ([hexStr length] == 2) {
//                [string appendString:hexStr];
//            } else {
//                [string appendFormat:@"0%@", hexStr];
//            }
//        }
//    }];
//    
//    return string;
//}

+ (NSString *)convertDataToHexStr:(NSData *)data {
    if (!data || [data length] == 0) {
        return @"";
    }
    NSMutableString *string = [[NSMutableString alloc] initWithCapacity:[data length]];
    
    [data enumerateByteRangesUsingBlock:^(const void *bytes, NSRange byteRange, BOOL *stop) {
        unsigned char *dataBytes = (unsigned char*)bytes;
        for (NSInteger i = 0; i < byteRange.length; i++) {
            NSString *hexStr = [NSString stringWithFormat:@"%x", (dataBytes[i]) & 0xff];
            if ([hexStr length] == 2) {
                [string appendString:hexStr];
            } else {
                [string appendFormat:@"0%@", hexStr];
            }
        }
    }];
    
    return string;
}




@end
