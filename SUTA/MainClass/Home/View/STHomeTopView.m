//
//  STHomeTopView.m
//  SUTA
//
//  Created by 张海军 on 2017/3/22.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import "STHomeTopView.h"

@interface STHomeTopView ()

@property (weak, nonatomic) IBOutlet UIView *leftMarginView;
@property (weak, nonatomic) IBOutlet UIView *rightMarginView;

@end

@implementation STHomeTopView
+ (instancetype)homeTopView
{
    STHomeTopView *view = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil] lastObject];
    return view;
}


- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.upLabel.textColor = RGBHEX_(c0a076);
    self.modelLabel.textColor = RGBHEX_(c0a076);
    self.leftMarginView.backgroundColor = RGBHEX_(c0a076);
    self.rightMarginView.backgroundColor = RGBHEX_(c0a076);
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
}


@end
