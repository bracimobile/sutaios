//
//  SnoringViewController.h
//  SUTA
//
//  Created by Pavan Jangid on 26/01/18.
//  Copyright © 2018 ZLB. All rights reserved.
//
#import "STBaseListViewController.h"
#import "STHomeTopView.h"
#import "STContactView.h"
#import "SnoringModel.h"
#import "SnoringTableCell.h"
#import "STBlueManage.h"
#import "CNPPopupController.h"
#import "STPeripheralInfoCell.h"
@interface SnoringViewController : STBaseListViewController
@property (nonatomic, strong) STHomeTopView *homeTopView;
@property (strong, nonatomic) IBOutlet UIView *viewPopup;
@property (weak, nonatomic) IBOutlet UITableView *tblDevice;
@property (weak, nonatomic) IBOutlet UIView *viewNoDevice;
@property (strong, nonatomic) IBOutlet UIButton *btnScan;

@end
