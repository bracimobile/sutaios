//
//  STBaseListViewController.m
//  SUTA
//
//  Created by 张海军 on 2017/3/23.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import "STBaseListViewController.h"

@interface STBaseListViewController ()

@end

@implementation STBaseListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.isGroup) {
        self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    }else{
        self.tableView = [[UITableView alloc] init];
    }
    self.currentPage = 1;
    self.tableView.emptyDataSetSource = self;
    self.tableView.emptyDataSetDelegate = self;
    self.tableView.tableFooterView = [UIView new];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - DZNEmptyDataSetSource

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = nil;

    text = @"空空如也~";
    if (self.loadFaileStr.length) {
        text = self.loadFaileStr;
    }

    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:18.0],
                                 NSForegroundColorAttributeName: RGBHEX_(c0a076)};
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)buttonTitleForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state
{
    NSString *text = nil;

    text = self.loadFailButtonStr; //@"重新加载";
    if (!self.loadFailButtonStr.length) {
        return nil;
    }

    if (!text) {
        return nil;
    }
    
    NSDictionary *normalAttributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:18.0],
                                       NSForegroundColorAttributeName: [UIColor redColor]};
    
    NSDictionary *Attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:18.0],
                                 NSForegroundColorAttributeName: [UIColor grayColor]};
    
    if (state == UIControlStateNormal) {
        return [[NSAttributedString alloc] initWithString:text attributes:normalAttributes];
    }else{
        return [[NSAttributedString alloc] initWithString:text attributes:Attributes];
    }
    
}

- (UIImage *)buttonImageForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state
{
    return self.loadFailButtonTextImage;
}

- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView
{
    if (self.isLoadFaile) {
        return nil;
    }
    UIActivityIndicatorView *indicatiorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [indicatiorView startAnimating];
    return indicatiorView;
}
 

- (CGFloat)verticalOffsetForEmptyDataSet:(UIScrollView *)scrollView
{
    if (self.tableView.tableHeaderView.hj_height) {
        return self.tableView.tableHeaderView.hj_height * 0.5;
    }else{
        return  - (54 * 0.5);
    }
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView
{
    return 12;
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIColor whiteColor];
}

- (void)emptyDataSetWillAppear:(UIScrollView *)scrollView
{
    scrollView.contentOffset = CGPointZero;
}

- (void)emptyDataSetDidAppear:(UIScrollView *)scrollView
{
    scrollView.contentOffset = CGPointZero;
}

#pragma mark - DZNEmptyDataSetDelegate
- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView
{
    if (self.tableView.contentSize.height > SCREEN_HEIGHT) {
        return YES;
    }else{
        return NO;
    }
    //     return YES;
}


- (void)emptyDataSet:(UIScrollView *)scrollView didTapButton:(UIButton *)button
{
//    if (!self.isNetwork) {
//        //        if (SystemVersion < 10.0) {
//        //            NSURL *WiFiURL = [NSURL URLWithString:@"Prefs:root=WIFI"];
//        //            if ([[UIApplication sharedApplication] canOpenURL:WiFiURL]) {
//        //                [[UIApplication sharedApplication] openURL:WiFiURL];
//        //            }
//        //        }else{
//        //            MYLog(@"iOS 10 以上系统");
//        [ToastManager showToastWithString:@"请前往 设置 -> Wi-Fi 设置网络"];
//        //        }
//    }
}


// 刷新空白页
- (void)loadFaileRefreshEmpty
{
    self.isLoadFaile = YES;
    [self.tableView reloadEmptyDataSet];
}

// 当数据为空的时候设置空白页属性
- (void)setEmptyImage:(NSString *)imageN text:(NSString *)title buttonText:(NSString *)buttonText
{
    
    [self.tableView reloadData];
    self.isEmptyStatus = YES;
    NSString *emptyImageN = imageN.length ? imageN : @"img_empty_default";
    self.loadFaileImage = [UIImage imageNamed:emptyImageN];
    self.loadFaileStr = title ? title : @"空空如也~";
    if (buttonText.length) {
        self.loadFailButtonTextImage = [UIImage ImageTitleSize:CGSizeMake(80, 30)
                                                    imageColor:RGBHEX_(f91c4c)
                                                          text:buttonText
                                                      textFont:SysFont_(14.0)
                                                     textColor:[UIColor redColor]];
    }
    [self loadFaileRefreshEmpty];
    //[self endRefreshing];
    
}

// 当加载失败时设置空白页属性
- (void)setFaileImage:(NSString *)imageF text:(NSString *)title buttonText:(NSString *)buttonText
{
    
    [self.tableView reloadData];
    self.isEmptyStatus = NO;
    NSString *faileImageN = imageF.length ? imageF : @"img_Failed-to-load";
    self.loadFaileImage = [UIImage imageNamed:faileImageN];
    self.loadFaileStr = title ? title : @"加载失败~";
    if (buttonText.length) {
        self.loadFailButtonTextImage = [UIImage ImageTitleSize:CGSizeMake(80, 30)
                                                    imageColor:RGBHEX_(f91c4c)
                                                          text:buttonText ? buttonText : @"重新加载"
                                                      textFont:SysFont_(14.0)
                                                     textColor:[UIColor redColor]];
    }
    [self loadFaileRefreshEmpty];
    //[self endRefreshing];
}

#pragma mark - 上下拉刷新 子类中调用的方法
// 刷新数据
- (void)refreshDataWithType:(BaseListRefreshState_type)refreshType
{
    
    self.totalNum = 0;
    //self.modelArray = [NSMutableArray array];
    WS(weakSelf);
    
    if (refreshType == BaseListRefreshState_typeUp) {  // 上拉
        self.tableView.mj_footer = [CMRefreshFooter footerWithRefreshingBlock:^{
            [weakSelf loadDataWithState:NO];
        }];
        self.tableView.mj_header.hidden = YES;
        
        
    }else if (refreshType == BaseListRefreshState_typeDown){ // 下拉
        self.tableView.mj_header = [CMRefreshHeader headerWithRefreshingBlock:^{
            weakSelf.currentPage = 1;
            [weakSelf loadDataWithState:YES];
        }];
        self.tableView.mj_footer.hidden = YES;
        
    }else{
        
        self.tableView.mj_header = [CMRefreshHeader headerWithRefreshingBlock:^{
            weakSelf.currentPage = 1;
            [weakSelf loadDataWithState:YES];
        }];
        
        self.tableView.mj_footer = [CMRefreshFooter footerWithRefreshingBlock:^{
            [weakSelf loadDataWithState:NO];
        }];
        
        
    }
    
}

// 加载数据
- (void)loadDataWithState:(BOOL)state
{
    
}
// 结束刷新
- (void)endRefreshing
{
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
    [self loadFaileRefreshEmpty];
}

// 设置分割线全部填充
- (void)hj_setTableViewSeparator
{
    self.tableView.separatorInset = UIEdgeInsetsZero;
    if ([self.tableView respondsToSelector:@selector(layoutMargins)]) {
        self.tableView.layoutMargins = UIEdgeInsetsZero;
    }
}


@end
