//
//  STPublicMethod.h
//  SUTA
//
//  Created by 张海军 on 17/3/21.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface STPublicMethod : NSObject

+ (CGSize)sizeString:(NSString *)string WithFont:(UIFont *)font constrainedToSize:(CGSize)size;

/**
 是否是iPad
 
 @return YES / NO
 */
+ (BOOL)isIPad;

@end
