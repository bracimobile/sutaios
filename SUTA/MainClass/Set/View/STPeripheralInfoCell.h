//
//  STPeripheralInfoCell.h
//  SUTA
//
//  Created by 张海军 on 2017/3/23.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STBlueModel.h"

@interface STPeripheralInfoCell : UITableViewCell
/// 模型数据
@property (nonatomic, strong) STBlueModel *dataModel;
@end
