//
//  UIImage+Image.m
//  
//
//  Created by NAVY on 15/7/6.
//  Copyright (c) 2015年 NAVY. All rights reserved.
//

#import "UIImage+Image.h"

@implementation UIImage (Image)

+ (UIImage *)imageWithColor:(UIColor *)color
{
    return [self imageWithColor:color height:1.0 width:1.0];
}

+ (UIImage *)imageWithColor:(UIColor *)color height:(CGFloat)height width:(CGFloat)width
{
    // 描述矩形
    CGRect rect = CGRectMake(0.0f, 0.0f, width, height);
    
    // 开启位图上下文
    UIGraphicsBeginImageContext(rect.size);
    // 获取位图上下文
    CGContextRef context = UIGraphicsGetCurrentContext();
    // 使用color演示填充上下文
    CGContextSetFillColorWithColor(context, [color CGColor]);
    // 渲染上下文
    CGContextFillRect(context, rect);
    // 从上下文中获取图片
    UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
    // 结束上下文
    UIGraphicsEndImageContext();
    
    return theImage;
}


+ (UIImage *)ImageTitleSize:(CGSize)imageSize imageColor:(UIColor *)imageColor text:(NSString *)str textFont:(UIFont *)textF textColor:(UIColor*)textC
{
    NSDictionary *attDic = @{ NSFontAttributeName : textF, NSForegroundColorAttributeName :textC};
    CGSize textSize = [str sizeWithAttributes:attDic];
    
    if (textSize.width > imageSize.width) {
        imageSize.width = textSize.width + 20;
    }
    
    UIImage *image = [self imageWithColor:[UIColor clearColor] height:imageSize.height width:imageSize.width];
    
    UIGraphicsBeginImageContextWithOptions (imageSize, NO , 0.0 );
    
    [image drawAtPoint : CGPointMake ( 0 , 0 )];
    
    UIBezierPath *bezierP = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, imageSize.width, imageSize.height) cornerRadius:4.0];
    bezierP.lineWidth = 1;
    [bezierP addClip];
    [RGBHEX_(f91c4c) setStroke];
    [bezierP stroke];
    // 获得一个位图图形上下文
    CGContextRef context= UIGraphicsGetCurrentContext ();
    
    
    CGContextDrawPath (context, kCGPathStroke);
    
    // 画
    
    CGFloat textX = (imageSize.width - textSize.width) * 0.5;
    CGFloat textY = (imageSize.height - textSize.height) * 0.5;
    //[str drawInRect:CGRectMake(textX, textY, textSize.width, textSize.height) withAttributes:attDic];
    
    [str drawAtPoint : CGPointMake (textX, textY) withAttributes:attDic];
    
    // 返回绘制的新图形
    
    UIImage *newImage= UIGraphicsGetImageFromCurrentImageContext ();
    
    
    UIGraphicsEndImageContext ();
    
    return newImage;
    
}


+ (UIImage *)ImageSize:(CGSize)imageSize text:(NSString *)str textFont:(UIFont *)textF textColor:(UIColor*)textC
{
    NSDictionary *attDic = @{ NSFontAttributeName : textF, NSForegroundColorAttributeName :textC};
    CGSize textSize = [str sizeWithAttributes:attDic];
    
    if (textSize.width > imageSize.width) {
        imageSize.width = textSize.width + 20;
    }
    
    UIImage *image = [self imageWithColor:[UIColor clearColor] height:imageSize.height width:imageSize.width];
    
    UIGraphicsBeginImageContextWithOptions (imageSize, NO , 0.0 );
    
    [image drawAtPoint : CGPointMake ( 0 , 0 )];

    // 获得一个位图图形上下文
    CGContextRef context= UIGraphicsGetCurrentContext ();
    CGContextDrawPath (context, kCGPathStroke);
    
    // 画
    CGFloat textX = (imageSize.width - textSize.width) * 0.5;
    CGFloat textY = (imageSize.height - textSize.height) * 0.5;
    //[str drawInRect:CGRectMake(textX, textY, textSize.width, textSize.height) withAttributes:attDic];
    
    [str drawAtPoint : CGPointMake (textX, textY) withAttributes:attDic];
    
    // 返回绘制的新图形
    
    UIImage *newImage= UIGraphicsGetImageFromCurrentImageContext ();
    
    
    UIGraphicsEndImageContext ();
    
    return newImage;
    
}


+ (UIImage *)imageWithOriginal:(NSString *)imageName
{
    UIImage *image = [UIImage imageNamed:imageName];
    image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    return image;
}


+ (UIImage *)resizable:(UIImage *)image {
    CGFloat top = image.size.height * 0.5;
    CGFloat left = image.size.width * 0.5;
    return  [image resizableImageWithCapInsets:UIEdgeInsetsMake(top, left, top - 1, left - 1)];
}

+ (UIImage *)imageWithCircularImage:(UIImage *)image {
    
    // 开启位图上下文
    CGFloat W = image.size.width;
    CGFloat H = image.size.height;
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(W, H), NO, 0);
    
    // 设置裁剪圆
    UIBezierPath *clipPath = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(0, 0, W, H)];
    [clipPath addClip];
    
    // 把图片渲染到上下文种
    [image drawAtPoint:CGPointMake(0, 0)];
    
    // 取出图片
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // 关闭上下文
    UIGraphicsEndImageContext();
    
    return newImage;
}

+ (UIImage *)imageToThumbnail:(UIImage *)image size:(CGSize)size
{
    // 开启位图上下文
    UIGraphicsBeginImageContextWithOptions(size, NO, 0);
    
    // 设置裁剪圆
//    UIBezierPath *clipPath = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(0, 0, size.width, size.height)];
//    [clipPath addClip];
    
    // 把图片渲染到上下文种
    [image drawInRect:CGRectMake(0.0, 0.0, size.width, size.height)];
    
    // 取出图片
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // 关闭上下文
    UIGraphicsEndImageContext();
    
    return newImage;
   
}


/**
 区分是 ipad / iPhone 图片名称
 
 @param name 原始名称
 @return ipad / iPhone 图片名称
 */
+ (UIImage *)ImageIpadOrIphoneName:(NSString *)name
{
    if ([STPublicMethod isIPad]) {
        return [UIImage imageNamed:[NSString stringWithFormat:@"ipad_%@",name]];
    }else{
        return [UIImage imageNamed:name];
    }
}


@end
