//
//  STClockRemindCell.m
//  SUTA
//
//  Created by 张海军 on 2017/5/19.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import "STClockRemindCell.h"
#import "STNewClockModel.h"

@interface STClockRemindCell ()
@property (strong, nonatomic) UISwitch *remindSwitch;

@end

@implementation STClockRemindCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = [UIColor blackColor];
        self.textLabel.textColor = [UIColor whiteColor];
        self.separatorInset = UIEdgeInsetsZero;
        if ([self respondsToSelector:@selector(layoutMargins)]) {
            self.layoutMargins = UIEdgeInsetsZero;
        }
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.remindSwitch = [[UISwitch alloc] init];
        [self.remindSwitch addTarget:self action:@selector(switchChange:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.remindSwitch];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    CGFloat centerx = self.hj_width - self.remindSwitch.hj_width * 0.5 - 16;
    self.remindSwitch.center = CGPointMake(centerx, self.hj_height * 0.5);
}

- (void)setDataModel:(NewClockEditModel *)dataModel
{
    _dataModel = dataModel;
    
    self.textLabel.text = dataModel.titleStr;
    self.detailTextLabel.text = dataModel.detaiStr;
    
}

- (void)switchChange:(UISwitch *)swith
{
    if (self.openSwitchBlock) {
        self.openSwitchBlock(swith.isOn);
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
