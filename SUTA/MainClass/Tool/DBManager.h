//
//  DBManager.h
//  DatabasePackge
//
//  Created by houzhifei on 16/2/25.
//  Copyright © 2016年 tentinet. All rights reserved.
//

/*
 * 数据库使用模型的第一个属性作为唯一键
 */

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
#import "FMDatabaseAdditions.h"
#import "FMDatabaseQueue.h"

/** SQLite五种数据类型 */
#define SQLTEXT     @"TEXT"
#define SQLINTEGER  @"INTEGER"
#define SQLREAL     @"REAL"
#define SQLBLOB     @"BLOB"
#define SQLNULL     @"NULL"

//数据库操作单例 （默认公共数据库）
#define LYXDBManager [DBManager sharedCommentDataManager]

//公共数据库
#define LYGCommenDBManager [DBManager sharedCommentDataManager]
//用户数据库
#define LYGUserDBManager [DBManager sharedUserDataManager]

//控制打印开关 0 关闭打印 1 打开打印
#define __Test 0

#ifdef __Test

#define DBLog(...) NSLog(@"%@", [NSString stringWithFormat:__VA_ARGS__])

#else

#define DBLog(...)

#endif

@interface DBManager : NSObject

/**
 *  公共数据库
 */
+ (instancetype)sharedCommentDataManager;

/**
 *  用户数据库
 */
+ (instancetype)sharedUserDataManager;

/*************************增、改********************************/
/**
 *  插入单个实例对象
 */
- (BOOL)insertAndUpdateModelToDatabase:(id)model;

/**
 *  插入多条记录，支持事物插入
 *
 *  @param array    数据数组
 *  @param complete 插入成功后回调
 */
- (void)insertOrUpdateMultipleData:(NSArray *)array complete:(void (^)(BOOL))complete;

/***************************删********************************/
/**
 *  删除某条记录
 */
- (BOOL)deleteModelInDatabase:(id)model;

/**
 *  删除某个表的所有数据
 */
- (BOOL)deleteAllDatasInTableName:(NSString *)tableName;

/**
 *  从某个表根据条件删除数据
 */
- (BOOL)deleteFromTableName:(NSString *)tableName WithRule:(NSString *)rule;

/***************************查********************************/
/**
 *  返回数据表中有多少条记录
 *
 *  @param tableName 表名
 *
 *  @return 数据数量
 */
- (NSInteger)inquireDataCountInDatabase:(NSString *)tableName;

/**
 *  取出表中所有数据
 *
 *  @param curClass 表名对应的类
 *
 *  @return 数据表的所有数据
 */
- (NSMutableArray *)takeOutInDataBase:(Class)curClass;

/**
 *  根据条件取出数据
 *
 *  @param curClass 表名对应的类
 *  @param rule     取出对应的数据，sql的条件语句,例如：where userId = '12'
 */
- (NSMutableArray *)takeOutInDataBase:(Class)curClass withRule:(NSString *)rule;


/**
 *  返回数据表中有多少条聊天记录
 *
 *  @param tableName 表名
 *
 *  @return 数据数量
 */
- (NSInteger)inquireIMDataCountInDatabase:(NSString *)tableName IMID:(NSString *)imID;
/**********************纯sql增删改查*****************************/
/**
 *  纯sql语句创建表、插入或更新、删除
 *
 *  @param sql sql语句
 */
- (BOOL)editDatabaseBySQL:(NSString *)sql;

/**
 *  纯sql语句查询,返回原始数据
 *
 *  @param sqlStr sql语句
 */
- (FMResultSet *)queryWithsqlStr:(NSString *)sqlStr;

@end
