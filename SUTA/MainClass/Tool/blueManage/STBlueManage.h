//
//  STBlueManage.h
//  SUTA
//
//  Created by 张海军 on 2017/3/23.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "STBlueModel.h"


@interface STBlueManage : NSObject

/// 当前设备蓝牙状态
@property (nonatomic, assign) CBManagerState blueState;
/** 当前需要连接的外围设备 */
@property (nonatomic, strong) CBPeripheral *currentPeripheral;


+ (instancetype)blueManageShare;

- (void)startScanWitTypeName:(NSString *)typeName;

// 主动连接
- (void)connect:(CBPeripheral *)peripheral;

// 主动断开连接
- (void)disconnect;

// 向当前已经连接的外围设备写入数据
- (void)writeCurrentPeripheralWihtData:(NSData *)writeData;
- (void)stopToDetect;
/// 搜索到符合条件的设备的回调
@property (nonatomic, copy) void(^scanConfromEquipmentBlock)(STBlueModel *model);
/// 连接外围设备成功的回调
@property (nonatomic, copy) void(^connetPeripheralSuccessBlock)(STBlueManage *manage);
/// 连接外围设备失败的回调
@property (nonatomic, copy) void(^connectPeripheraldidFailBlock)(CBPeripheral *peripheral);
/// 已经连接的设备断开连接的回调
@property (nonatomic, copy) void(^didDisconnectPeripheralBlock)(CBPeripheral *peripheral);
/// 连接设备后返回的
@property (nonatomic, copy) void(^connectPeripherCallBack)(NSInteger value);

/// 6s 后的一个回调
@property (nonatomic, copy) void(^sixSBlock)();

@end
