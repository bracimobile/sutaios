//
//  UIFactory.h
//  BaseTarget
//
//  Created by zmz on 15/8/31.
//  Copyright (c) 2015年 Shenzhen Tentinet Technology Co,. Ltd.. All rights reserved.
//  控件工具方法

#import <Foundation/Foundation.h>

@interface UIFactory : NSObject

/**
 创建label

 @param frame           frame description
 @param textAlignment   textAlignment description
 @param title           <#title description#>
 @param font            <#font description#>
 @param textColor       <#textColor description#>
 @param backGroundcolor <#backGroundcolor description#>

 @return UILabel
 */
+ (UILabel *)createLabelWithFrame:(CGRect)frame NSTextAlignment:(NSTextAlignment)textAlignment Title:(NSString *)title Font:(UIFont *)font TextColor:(UIColor *)textColor Backgroundcolor:(UIColor *)backGroundcolor;


/**
 创建UIButton （标识用image）

 @param frame       <#frame description#>
 @param title       <#title description#>
 @param normalImage <#normalImage description#>
 @param selectImage <#selectImage description#>
 @param font        <#font description#>
 @param titleColor  <#titleColor description#>
 @param color       <#color description#>
 @param target      <#target description#>
 @param selector    <#selector description#>

 @return <#return value description#>
 */
+ (UIButton *)createButtonWithFrame:(CGRect)frame LabelTitle:(NSString *)title NormalImage:(UIImage *)normalImage HighlightedImage:(UIImage *)HighlightedImage SelectImage:(UIImage *)selectImage TextFont:(UIFont *)font TitleColor:(UIColor *)titleColor BackGorundColor:(UIColor *)color Target:(id)target Action:(SEL)selector;


/**
 创建UIButton （与上不同是设置背景图片）

 @param frame       <#frame description#>
 @param title       <#title description#>
 @param normalImage <#normalImage description#>
 @param selectImage <#selectImage description#>
 @param font        <#font description#>
 @param titleColor  <#titleColor description#>
 @param color       <#color description#>
 @param target      <#target description#>
 @param selector    <#selector description#>

 @return <#return value description#>
 */
+ (UIButton *)createButtonWithFrame:(CGRect)frame LabelTitle:(NSString *)title NormalBackgroundImage:(UIImage *)normalImage HighlightBackgroundImage:(UIImage *)HighlightImage SelectBackgroundImage:(UIImage *)selectImage TextFont:(UIFont *)font TitleColor:(UIColor *)titleColor BackGorundColor:(UIColor *)color Target:(id)target Action:(SEL)selector;

/**
 创建UIView

 @param frame <#frame description#>
 @param color <#color description#>

 @return <#return value description#>
 */
+ (UIView *)createViewWithFrame:(CGRect)frame backgroundColor:(UIColor *)color;

/**
 画线

 @param originaly 高度

 @return line
 */
+ (UIView *)createLineWithOriginaly:(CGFloat)originaly;

/**
 *@brief 左边少ofx
 *@param originaly  Y坐标  ofx X坐标
 */
+ (UIView *)createLineOfFifteenWithOriginaly:(CGFloat)originaly AndOf:(CGFloat)ofx;
/**
 *@brief 左边少ofx 宽为width
 *@param originaly  Y坐标  ofx X坐标
 */
+ (UIView *)createLineOfFifteenWithOriginaly:(CGFloat)originaly AndOf:(CGFloat)ofx Width:(CGFloat)width;

/**
 *  @author 赵坤, 15-03-10 17:03:56
 *
 *  @brief  单线竖线
 *
 *  @param originalx 坐标x
 *  @param Originaly 坐标y
 *  @param height    高度
 *
 *  @return view
 */
+ (UIView *)createLineOfFifteenWithOriginalx:(CGFloat)originalx Originaly:(CGFloat)Originaly height:(CGFloat)height;

/**
 * 添加左lable,右textField
 */
+ (UITextField *)createMessageCollectView:(CGRect)frame
                            leftLabelName:(NSString *)leftLabelName
                          rightPlaceHoder:(NSString *)rightPlaceHolder
                                rightText:(NSString *)rightText
                                 delegate:(id)delegate
                                      tag:(NSInteger)tag
                                   onView:(UIView *)view;

/**
 * 添加左lable,右label
 */
+ (UILabel *)createLabelAndLabel:(CGRect)frame
                       textColor:(UIColor *)color
                   leftLabelName:(NSString *)leftLabelName
                       rightText:(NSString *)rightText
                             tag:(NSInteger)tag
                          onView:(UIView *)view;

/**
 * 添加左lable,右button
 */
+ (UIButton *)createLabelAndButton:(CGRect)frame
                     leftLabelName:(NSString *)leftLabelName
                           btnText:(NSString *)btnText
                            target:(id)target
                          selector:(SEL)selector
                               tag:(NSInteger)tag
                            onView:(UIView *)view;


@end
