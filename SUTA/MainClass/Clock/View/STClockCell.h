//
//  STClockCell.h
//  SUTA
//
//  Created by 张海军 on 2017/5/18.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import <UIKit/UIKit.h>
@class STNewClockModel;

@interface STClockCell : UITableViewCell
/// 模型数据
@property (nonatomic, strong) STNewClockModel *dataModel;
/// index
@property (nonatomic, strong) NSIndexPath *indexPath;
/// 点击删除按钮的回调
@property (nonatomic, copy) void(^deleteButtonDidClick)(NSIndexPath *indexPath);
@end
