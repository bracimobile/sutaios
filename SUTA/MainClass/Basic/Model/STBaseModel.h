//
//  STBaseModel.h
//  SUTA
//
//  Created by 张海军 on 2017/3/22.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface STBaseModel : NSObject
/// name
@property (nonatomic, copy) NSString *titleName;
/** 点击cell跳转的控制器 */
@property (nonatomic, assign)  Class destVcClass;
/** 点击cell的回调 */
@property (nonatomic, copy) void(^cellClickBlock)();

@end
