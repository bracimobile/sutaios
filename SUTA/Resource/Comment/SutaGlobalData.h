//
//  SutaGlobalData.h
//  remote
//
//  Created by ben on 17/3/1.
//  Copyright © 2017年 suta. All rights reserved.
//

#import <Foundation/Foundation.h>
#import  <UIKit/UIKit.h>
//#import "BLETopView.h"
//#import "Singleton.h"

#ifdef __cplusplus
#define UIKIT_EXTERN        extern "C" __attribute__((visibility ("default")))
#else
#define UIKIT_EXTERN            extern __attribute__((visibility ("default")))
#endif


#pragma mark - 全局常量
// service uuid
UIKIT_EXTERN NSString *const UUID_COMM_SERVICE;
// write characteristic uuid
UIKIT_EXTERN NSString *const UUID_WRITE_CHARACTERISTIC;
// notify characteristic uuid
UIKIT_EXTERN NSString *const UUID_NOTIFY_CHARACTERISTIC;


//头部上
UIKIT_EXTERN Byte headUpCommand[] ;
//头部下
UIKIT_EXTERN Byte headDownCommand[];
//脚步上
UIKIT_EXTERN Byte footUpCommand[] ;
//脚步下
UIKIT_EXTERN Byte footDownCommand[] ;
//腰部上
UIKIT_EXTERN Byte waistUpCommand[] ;
//腰部下
UIKIT_EXTERN Byte waistDownCommand[] ;
//背部上
UIKIT_EXTERN Byte backUpCommand[] ;
//背部下
UIKIT_EXTERN Byte backDownCommand[] ;
//头部振动强度
UIKIT_EXTERN Byte headVibrationCommand[] ;
//脚步振动强度
UIKIT_EXTERN Byte footVibrationCommand[] ;
//振动模式 （MASSAGE）
UIKIT_EXTERN Byte massageCommand[] ;
//零重力功能 (ZEROG)
UIKIT_EXTERN Byte zeroGCommand[] ;
//平躺功能 (FLAT)
UIKIT_EXTERN Byte flatCommand[] ;
//记忆位置功能1(M1)
UIKIT_EXTERN Byte m1Command[] ;
//记忆位置功能2（M2）
UIKIT_EXTERN Byte m2Command[] ;
//保存当前位置在M1
UIKIT_EXTERN Byte saveInM1Command[] ;
//保存当前位置在M2
UIKIT_EXTERN Byte saveInM2Command[];
//TIMER/ALL OFF
UIKIT_EXTERN Byte timerAllOffCommand[] ;
//床灯功能
UIKIT_EXTERN Byte bedLampCommand[] ;
//恢复默认 MEMORY A
UIKIT_EXTERN Byte recoverMemoryACommand[] ;
//保存当前位置在ZEROG
UIKIT_EXTERN Byte savePositionInZeroGCommand[] ;

//左右床同时动作（l）
//左床动作(L)
//UIKIT_EXTERN Byte saveInM2Command[] ;
//右床动作(R)
//UIKIT_EXTERN Byte saveInM2Command[] ;


//抬起动作 每一个指令发完后，需要发抬起指令
UIKIT_EXTERN Byte upliftCommand[] ;



UIKIT_EXTERN NSString *const COMMAND ;
//左右床同时动作
UIKIT_EXTERN NSString *const LEFT_AND_RIGHT_BED;
//左床动作L
UIKIT_EXTERN NSString *const LEFT_BED ;
//右床动作R
UIKIT_EXTERN NSString *const RIGHT_BED ;
//头部上
UIKIT_EXTERN NSString *const DATA_HEAD_UP ;
//头部下
UIKIT_EXTERN NSString *const DATA_HEAD_DOWN ;
//脚步上
UIKIT_EXTERN NSString *const DATA_FOOT_UP ;
//脚步下
UIKIT_EXTERN NSString *const DATA_FOOT_DOWN;
//腰部上
UIKIT_EXTERN NSString *const DATA_WAIST_UP ;
//腰部下
UIKIT_EXTERN NSString *const DATA_WAIST_DOWN ;
//背部上
UIKIT_EXTERN NSString *const DATA_BACK_UP ;

//背部下
UIKIT_EXTERN NSString *const DATA_BACK_DOWN ;

//头部震动强度
UIKIT_EXTERN NSString *const DATA_HEAD_SHAKE ;


//脚步震动强度
UIKIT_EXTERN NSString *const DATA_FOOT_SHAKE ;

//震动模式 MASSAGE
UIKIT_EXTERN NSString *const DATA_MASSAGE ;

//零重力功能 ZEROG
UIKIT_EXTERN NSString *const DATA_ZEROG ;

//平躺功能 flat
UIKIT_EXTERN NSString *const DATA_FLAT ;

//记忆位置功能 1 （M1）
UIKIT_EXTERN NSString *const DATA_M1 ;

//记忆位置功能 2 （M2）
UIKIT_EXTERN NSString *const DATA_M2 ;

//保存当前位置在M1 长按3秒开启

UIKIT_EXTERN NSString *const DATA_SAVE_M1 ;

//保存当前位置在M2 长按3秒开启

UIKIT_EXTERN NSString *const DATA_SAVE_M2 ;

//TIMER/ALL OFF
UIKIT_EXTERN NSString *const DATA_TIME_ALL_OFF ;
//床灯功能
UIKIT_EXTERN NSString *const DATA_BED_LIGHT ;

//恢复默认 MEMORY A 长按6秒开启
UIKIT_EXTERN NSString *const DATA_RECOVER_MEMORY_A ;

//保持当前位置在ZEROG 长按3秒开启
UIKIT_EXTERN NSString *const DATA_SAVE_ZEROG;



//500i
//振动模式 l
UIKIT_EXTERN NSString *const DATA_SHAKE_MODEL_l;
//振动模式 ll
UIKIT_EXTERN NSString *const DATA_SHAKE_MODEL_ll;
//振动模式 lll
UIKIT_EXTERN NSString *const DATA_SHAKE_MODEL_lll;
//记忆位置功能 MEMORY
UIKIT_EXTERN NSString *const DATA_POSITION_MEMORY;
//保存当前位置在MEMORY 长按3秒
UIKIT_EXTERN NSString *const DATA_SAVE_MEMORY;

UIKIT_EXTERN  CGFloat  screenHeight;
UIKIT_EXTERN  CGFloat  screenWidth ;

UIKIT_EXTERN  CGFloat  centerScreenHeight;
UIKIT_EXTERN  CGFloat  centerScreenWidth ;
UIKIT_EXTERN  CGFloat  leftMargin ;
UIKIT_EXTERN  CGFloat  rightMargin ;

UIKIT_EXTERN CGFloat Blackheight;
UIKIT_EXTERN CGFloat Blackwidth;


UIKIT_EXTERN  CGFloat  imgBgX ;
UIKIT_EXTERN  CGFloat  imgBgY ;
UIKIT_EXTERN  CGFloat  imgBgWidth ;
UIKIT_EXTERN  CGFloat  imgBgHeight ;


UIKIT_EXTERN CGFloat  LIRBgX ;
UIKIT_EXTERN CGFloat  LIRBgY ;
UIKIT_EXTERN CGFloat  LIRBgWidth ;
UIKIT_EXTERN CGFloat  LIRBgHeight;


UIKIT_EXTERN CGFloat  LIRX ;
UIKIT_EXTERN CGFloat  LIRY ;
UIKIT_EXTERN CGFloat  LIRWidth ;
UIKIT_EXTERN CGFloat  LIRHeight ;




//20 灯
UIKIT_EXTERN CGFloat  LightTenX ;
UIKIT_EXTERN CGFloat  LightTenY ;
UIKIT_EXTERN CGFloat  LightTenWidth ;
UIKIT_EXTERN CGFloat  LightTenHeight ;

UIKIT_EXTERN CGFloat  LightTenLabelX ;
UIKIT_EXTERN CGFloat  LightTenLabelY ;
UIKIT_EXTERN CGFloat  LightTenLabelWidth ;
UIKIT_EXTERN CGFloat  LightTenLabelHeight ;


//20 灯
UIKIT_EXTERN CGFloat  LightTwentyX ;
UIKIT_EXTERN CGFloat  LightTwentyY ;
UIKIT_EXTERN CGFloat  LightTwentyWidth ;
UIKIT_EXTERN CGFloat  LightTwentyHeight ;

UIKIT_EXTERN CGFloat  LightTwentyLabelX ;
UIKIT_EXTERN CGFloat  LightTwentyLabelY ;
UIKIT_EXTERN CGFloat  LightTwentyLabelWidth ;
UIKIT_EXTERN CGFloat  LightTwentyLabelHeight ;

//30 灯
UIKIT_EXTERN CGFloat  LightThirtyX ;
UIKIT_EXTERN CGFloat  LightThirtyY ;
UIKIT_EXTERN CGFloat  LightThirtyWidth ;
UIKIT_EXTERN CGFloat  LightThirtyHeight ;

UIKIT_EXTERN CGFloat  LightThirtyLabelX ;
UIKIT_EXTERN CGFloat  LightThirtyLabelY ;
UIKIT_EXTERN CGFloat  LightThirtyLabelWidth ;
UIKIT_EXTERN CGFloat  LightThirtyLabelHeight ;




UIKIT_EXTERN CGFloat  headShakeX ;
UIKIT_EXTERN CGFloat  headShakeY ;
UIKIT_EXTERN CGFloat  headShakeWidth ;
UIKIT_EXTERN CGFloat  headShakeHeight ;

UIKIT_EXTERN CGFloat  switchModeX ;
UIKIT_EXTERN CGFloat  switchModeY ;
UIKIT_EXTERN CGFloat  switchModeWidth ;
UIKIT_EXTERN CGFloat  swithcModeHeight ;

UIKIT_EXTERN CGFloat  footShakeX ;
UIKIT_EXTERN CGFloat  footShakeY ;
UIKIT_EXTERN CGFloat  footShakeWidth ;
UIKIT_EXTERN CGFloat  footShakeHeight ;


UIKIT_EXTERN CGFloat  btnlX ;
UIKIT_EXTERN CGFloat  btnlY ;
UIKIT_EXTERN CGFloat  btnlWidth ;
UIKIT_EXTERN CGFloat  btnlHeight ;

UIKIT_EXTERN CGFloat  btnllX ;
UIKIT_EXTERN CGFloat  btnllY ;
UIKIT_EXTERN CGFloat  btnllWidth ;
UIKIT_EXTERN CGFloat  btnllHeight ;

UIKIT_EXTERN CGFloat  btnlllX ;
UIKIT_EXTERN CGFloat  btnlllY ;
UIKIT_EXTERN CGFloat  btnlllWidth ;
UIKIT_EXTERN CGFloat  btnlllHeight ;

UIKIT_EXTERN CGFloat  backUpX ;
UIKIT_EXTERN CGFloat  backUpY ;
UIKIT_EXTERN CGFloat  backUpWidth ;
UIKIT_EXTERN CGFloat  backUpHeight ;

UIKIT_EXTERN CGFloat  backDownX ;
UIKIT_EXTERN CGFloat  backDownY ;
UIKIT_EXTERN CGFloat  backDownWidth ;
UIKIT_EXTERN CGFloat  backDownHeight ;

UIKIT_EXTERN CGFloat  zerogX ;
UIKIT_EXTERN CGFloat  zerogY ;
UIKIT_EXTERN CGFloat  zerogWidth ;
UIKIT_EXTERN CGFloat  zerogHeight ;

UIKIT_EXTERN CGFloat  memoryX ;
UIKIT_EXTERN CGFloat  memoryY ;
UIKIT_EXTERN CGFloat  memoryWidth ;
UIKIT_EXTERN CGFloat  memoryHeight ;


UIKIT_EXTERN CGFloat  footUpX ;
UIKIT_EXTERN CGFloat  footUpY ;
UIKIT_EXTERN CGFloat  footUpWidth;
UIKIT_EXTERN CGFloat  footUpHeight ;

UIKIT_EXTERN CGFloat  footDownX ;
UIKIT_EXTERN CGFloat  footDownY ;
UIKIT_EXTERN CGFloat  footDownWidth ;
UIKIT_EXTERN CGFloat  footDownHeight ;


UIKIT_EXTERN CGFloat  btnMinusX ;
UIKIT_EXTERN CGFloat  btnMinusY ;
UIKIT_EXTERN CGFloat  btnMinusWidth ;
UIKIT_EXTERN CGFloat  btnMinusHeight ;


UIKIT_EXTERN CGFloat  headUpX ;
UIKIT_EXTERN CGFloat  headUpY ;
UIKIT_EXTERN CGFloat  headUpWidth ;
UIKIT_EXTERN CGFloat  headUpHeight ;

UIKIT_EXTERN CGFloat  headDownX ;
UIKIT_EXTERN CGFloat  headDownY ;
UIKIT_EXTERN CGFloat  headDownWidth ;
UIKIT_EXTERN CGFloat  headDownHeight ;


UIKIT_EXTERN CGFloat  waistUpX ;
UIKIT_EXTERN CGFloat  waistUpY ;
UIKIT_EXTERN CGFloat  waistUpWidth ;
UIKIT_EXTERN CGFloat  waistUpHeight ;


UIKIT_EXTERN CGFloat  waistDownX ;
UIKIT_EXTERN CGFloat  waistDownY ;
UIKIT_EXTERN CGFloat  waistDownWidth;
UIKIT_EXTERN CGFloat  waistDownHeight ;

UIKIT_EXTERN CGFloat  btnLX ;
UIKIT_EXTERN CGFloat  btnLY ;
UIKIT_EXTERN CGFloat  btnLWidth ;
UIKIT_EXTERN CGFloat  btnLHeight ;

UIKIT_EXTERN CGFloat  btnCenterLX ;
UIKIT_EXTERN CGFloat  btnCenterLY ;
UIKIT_EXTERN CGFloat  btnCenterLWidth ;
UIKIT_EXTERN CGFloat  btnCenterLHeight ;

UIKIT_EXTERN CGFloat  btnRX ;
UIKIT_EXTERN CGFloat  btnRY ;
UIKIT_EXTERN CGFloat  btnRWidth ;
UIKIT_EXTERN CGFloat  btnRHeight;


@interface SutaGlobalData : NSObject

+ (NSData *) combineCommand : (NSString *)command middleCommand
                            : (NSString *)middle  lastCommand
                            : (NSString *)data;

+ (NSString *)convertDataToHexStr:(NSData *)data;

+(NSData*) hexToBytes : (NSString *) originalText;

@end
