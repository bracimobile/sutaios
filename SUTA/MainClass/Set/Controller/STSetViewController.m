//
//  STSetViewController.m
//  SUTA
//
//  Created by 张海军 on 17/3/21.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import "STSetViewController.h"
#import "STHomeTopView.h"
#import "STBlueManage.h"
#import "STPeripheralInfoCell.h"

static NSString *ID = @"STPeripheralInfoCellId";

@interface STSetViewController ()<UITableViewDelegate, UITableViewDataSource, UIAlertViewDelegate>
/// topView
@property (nonatomic, strong) STHomeTopView *homeTopView;
/// 模型数组
@property (nonatomic, strong) NSMutableArray *modelArray;
/// 当前选中的
@property (nonatomic, strong) NSIndexPath *seleteIndexPath;
@end

@implementation STSetViewController
#pragma mark - life cycle
- (instancetype)init
{
    if (self = [super init]) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshList) name:BEGINSCARNDEVICE object:nil];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupInit];
    
    // 连接外围设备成功
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connetPeripheralSuccess) name:CONNETPERIPHERALSUCCESS object:nil];
    
    // 断开已连接的外围设备
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(disconnectPeripheral) name:CONNETPERIPHERALDISCONNECT object:nil];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.homeTopView.frame = CGRectMake(0, 0, SCREEN_WIDTH, START_Y);
    self.tableView.frame = CGRectMake(0, START_Y, self.view.hj_width, self.view.hj_height - START_Y);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    WS(weakSelf);
    [STBlueManage blueManageShare].scanConfromEquipmentBlock = ^(STBlueModel *model){
      
        BOOL isHas = NO;
        
        for (STBlueModel *oldM in weakSelf.modelArray) {
            if ([oldM.peripheralUUID isEqualToString:model.peripheralUUID]) {
                oldM.peripheralRssi = model.peripheralRssi;
                isHas = YES;
            }
        }
        
        if (!isHas) {
            [weakSelf.modelArray addObject:model];
        }
        
        if (weakSelf.modelArray.count == 0) {
            if (CURRENT_SELETE_SUTATYPE.length) {
                [weakSelf setEmptyImage:nil text:[NSString stringWithFormat:@"%@ device not found",CURRENT_SELETE_SUTATYPE] buttonText:@"Search for other devices"];
                
            }
        }
        
        [weakSelf.tableView reloadData];
    };
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-  (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - 初始化
- (void)setupInit
{
    // 移除 NAv
    [self.NavBackView removeFromSuperview];
    [self.NavLeftBtn removeFromSuperview];
    [self.NavRightBtn removeFromSuperview];
    
    
    // 添加顶部
    self.homeTopView = [STHomeTopView homeTopView];
    self.homeTopView.upLabel.text = @"DeviceList";
    self.homeTopView.modelLabel.hidden = YES;
    [self.view addSubview:self.homeTopView];
    
    // tableView
    [self refreshDataWithType:BaseListRefreshState_typeUp];
    self.tableView.mj_header.hidden = YES;
    self.tableView.mj_footer.hidden = YES;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.rowHeight = 68;
    [self hj_setTableViewSeparator];
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([STPeripheralInfoCell class]) bundle:nil] forCellReuseIdentifier:ID];
    [self.view addSubview:self.tableView];
    
    [self setEmptyImage:nil text:@"SUTA device not found" buttonText:nil];
    
}

#pragma mark - notification
- (void)connetPeripheralSuccess
{
    [SVProgressHUD dismiss];
    self.tabBarController.selectedIndex = 0;
    [self.modelArray removeAllObjects];
    [self.tableView reloadData];
    [[STBlueManage blueManageShare] startScanWitTypeName:CURRENT_SELETE_SUTATYPE];
}

- (void)refreshList
{
    [self.modelArray removeAllObjects];
    if (CURRENT_SELETE_SUTATYPE.length) {
        [self setEmptyImage:nil text:[NSString stringWithFormat:@"%@ device not found",CURRENT_SELETE_SUTATYPE] buttonText:@"Search for other devices"];
        
    }
    [self.tableView reloadData];
}


- (void)disconnectPeripheral
{
    [self refreshList];
}

#pragma mark - table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.modelArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    STPeripheralInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    cell.dataModel = self.modelArray[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    STBlueManage *manage = [STBlueManage blueManageShare];
    

    self.seleteIndexPath = indexPath;
    STBlueModel *dataModel = self.modelArray[indexPath.row];
    WS(weakSelf);
    // 连接对应蓝牙
    if(manage.blueState == CBManagerStatePoweredOff){
        [ToastManager showToastWithString:@"Open Bluetooth, please"];
        return;
    }
    [manage connect:dataModel.peripheral];
//    [SVProgressHUD showWithStatus:[NSString stringWithFormat:@"【%@】Device connection",dataModel.peripheral.name]];
    manage.connectPeripheraldidFailBlock = ^ (CBPeripheral *peripheral) {
        
        NSString *title = [NSString stringWithFormat:@"【%@】Device connection failed",peripheral.name];
        UIAlertView *failAlert = [[UIAlertView alloc] initWithTitle:title message:@"Reconnection ?" delegate:weakSelf cancelButtonTitle:@"Cancel" otherButtonTitles:@"Reconnect", nil];
        [failAlert show];
    };
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        STBlueModel *dataModel = self.modelArray[self.seleteIndexPath.row];
        [[STBlueManage blueManageShare] connect:dataModel.peripheral];
    }
}


- (void)emptyDataSet:(UIScrollView *)scrollView didTapButton:(UIButton *)button
{
    NSString *typeName = nil;
    if ([CURRENT_SELETE_SUTATYPE isEqualToString:SUTA_500I]) {
        typeName = SUTA_900I;
    }else{
        typeName = SUTA_500I;
    }
    [[STBlueManage blueManageShare] startScanWitTypeName:typeName];
}

#pragma mark - lazy
- (NSMutableArray *)modelArray
{
    if (!_modelArray) {
        _modelArray = [NSMutableArray array];
    }
    return _modelArray;
}

@end
