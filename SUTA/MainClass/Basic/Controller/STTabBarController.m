//
//  STTabBarController.m
//  SUTA
//
//  Created by 张海军 on 17/3/21.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import "STTabBarController.h"
#import "STNavigationController.h"
#import "STHomeViewController.h"
#import "STNewClockViewController.h"
#import "STHelpViewController.h"
#import "STSetViewController.h"
#import "SnoringViewController.h"
#import "STTabBar.h"

@interface STTabBarController ()

@end

@implementation STTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    STTabBar *tabbar = [[STTabBar alloc] init];
    [tabbar setBackgroundImage:[UIImage imageWithColor:[UIColor blackColor]]];
    tabbar.delegate = self;
    [self setValue:tabbar forKey:@"tabBar"];
    
    // 添加子控制器
    [self setupChildVc:[[STHomeViewController alloc] init] title:nil image:@"remote_normal" selectedImage:@"remote_active"];
    [self setupChildVc:[[STNewClockViewController alloc] init] title:nil image:@"clock_normal" selectedImage:@"clock_active"];
    [self setupChildVc:[[STHelpViewController alloc] init] title:nil image:@"question_normal" selectedImage:@"question_active"];
    [self setupChildVc:[[STSetViewController alloc] init] title:nil image:@"setting_normal" selectedImage:@"setting_active"];
    [self setupChildVc:[[SnoringViewController alloc] init] title:nil image:@"Snoring_normal" selectedImage:@"Snoring_active"];
    self.selectedIndex = 2;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 初始化
/**
 * 初始化子控制器
 */
- (void)setupChildVc:(UIViewController *)vc title:(NSString *)title image:(NSString *)image selectedImage:(NSString *)selectedImage{
    
    // 设置文字和图片
    vc.tabBarItem.title = title;
    UIImage * defaultImage = [UIImage imageWithOriginal:image];
    vc.tabBarItem.image = defaultImage;
    vc.tabBarItem.selectedImage = [UIImage imageWithOriginal:selectedImage];
    
    UIEdgeInsets insets = UIEdgeInsetsMake(6, 0, -6, 0);
    vc.tabBarItem.imageInsets = insets;
    
    STNavigationController *nav = [[STNavigationController alloc]initWithRootViewController:vc];
    [self addChildViewController:nav];
    
}

@end
