//
//  CommenMethod.h
//  Etoou
//
//  Created by tentinet on 13-11-6.
//  Copyright (c) 2013年 Tentinet. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSData+TrimReturn.h"

#define StringFromBool(_x_)   (_x_)?@"1" :@"0"
#define StringFromFloat(_x_)  [NSString stringWithFormat:@"%f",_x_]
//#define StringFromInt(_x_)  [NSString stringWithFormat:@"%ld",(long)_x_]
#define StringFromNSInteger(_x_)  [NSString stringWithFormat:@"%ld",_x_]
#define checkNull(__X__)        ((__X__) == [NSNull null] || ([(__X__) isKindOfClass:[NSString class]] && [(__X__) isEqualToString:@""])) ? nil : (__X__)
#define NumberToString(_x_)  _x_ == nil ? @"" : [NSString stringWithFormat:@"%@",_x_]

#define BYDLogDebug(format, ...)  \
[CommenMethod debugMsg:[NSString stringWithFormat:format, ##__VA_ARGS__] \
cls:self file:__FILE__ function:__FUNCTION__ line:__LINE__]

//#import "UploadImageInfo.h"

typedef enum {
    kIT150,
    kIT300,
    kIT450,
    kITNoraml
}ImageSizeType;

@interface CommenMethod : NSObject

+(NSArray*) getCardType;
+(NSString *) getImageByName:(NSString*)imageName Type:(ImageSizeType) type;
+(NSString *) renameFile:(NSString*) filePath toType:(NSString *) type;

+(NSString *)trimString:(NSString *)str;
+ (void)createDirectoryInSandBox:(NSString *)dircName; //在沙盒中创建文件夹
//+ (void)saveHTMLFileToUserFolder:(NSString *)htmlString andFileName:(NSString *)fileName; //把html源代码保存到用户文件夹下
+ (NSString *)processNullString:(NSString *)string;
+ (BOOL)validatePassword:(NSString *)pwd; //验证密码有效性，数字，字母，下划线 6-18位
+ (void)debugMsg:(NSString *)message cls:(id)classObj file:(const char *)file function:(const char *)func line:(const unsigned int)line;
+ (NSString *)docPath;
+ (NSString *)touch:(NSString *)path;
+ (NSInteger )securityInt:(id)param;

+ (NSArray *)groupDataSource:(NSArray *)source numberOfColumn:(NSInteger)number; //把一维数组，按给定的列数拆分成二维数组

+ (BOOL)isPhoneNumber:(NSString *)string;   //是否为手机号码
+ (BOOL)NSStringIsValidEmail:(NSString *)checkString;   //是否为电子邮箱

+ (NSString *)price:(NSString *)price;  //价格（¥xxx起)格式

//文件操作
+ (NSString *)sendboxTempFolder;
+ (BOOL)saveImage:(UIImage *)image atPath:(NSString *)path fileName:(NSString *)fileName;

//+ (NSString *)getImageNames:(NSArray *)imageInfoArr;

+ (NSString *)voiceLengthToString:(double)voiceLength;
+ (NSString *)formatVoiceLength:(NSString *)length;

//获取设备型号
+ (NSString *)deviceString;

//获取app版本
+ (NSString *)appVersion;

//转换#FFFFFF格式颜色
+ (UIColor *)RGBColor:(NSString *)string;

/**
 * @author houzhifei, 16-05-17 14:05:34
 *
 * @brief 压缩图片到指定大小
 *
 * @param originalData  原始图片大小
 * @param specifiedSize 指定大小（kb）
 *
 * @return 压缩后的图片大小
 */
+ (NSData *)compressImage:(NSData *)originalData specifiedSize:(NSInteger)specifiedSize;
+(NSString *)getRemainTimeFormat:(NSTimeInterval)time;
@end
