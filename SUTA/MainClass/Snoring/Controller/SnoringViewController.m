//
//  SnoringViewController.m
//  SUTA
//
//  Created by Pavan Jangid on 26/01/18.
//  Copyright © 2018 ZLB. All rights reserved.
//

#import "SnoringViewController.h"
#import "bridgeClass.h"
#import "SharedInstance.h"
#import "AppDelegate.h"
#import "SutaGlobalData.h"
@interface SnoringViewController ()<UITableViewDelegate,UITableViewDataSource,CNPPopupControllerDelegate>
{
    BOOL isPopupPresented;
    int nAlarmType;
    int m_nDetectedSoundType;
    BOOL SoundPlaying,kpushDetection, isSwitchOn, isDetectDevice;
    int totalCount;
    NSTimer *starTimer;
    NSTimer *stopTimer;
    NSTimer *resumeTimer;
    NSTimer *displayTimer;
    NSTimer *stopHardwareTimer;
    NSTimeInterval timeIntervalDisplay;
}
@property (nonatomic, strong) NSMutableArray *modelArray;
@property (nonatomic, strong) NSMutableArray *arrDeviceList;

@property (nonatomic, strong) CNPPopupController *popupController;

@end

@implementation SnoringViewController
@synthesize viewPopup,arrDeviceList;
#pragma mark - life cycle
- (instancetype)init
{
    if (self = [super init]) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshList) name:BEGINSCARNDEVICE object:nil];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    isSwitchOn = false;
    isDetectDevice = false;
    [self setupInit];
    [self loadData];
    APPDELEGATE.kdetectionMode = false;
    
    timeIntervalDisplay = 0;
    
    // 连接外围设备成功
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connetPeripheralSuccess) name:CONNETPERIPHERALSUCCESS object:nil];
    
    // 断开已连接的外围设备
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(disconnectPeripheral) name:CONNETPERIPHERALDISCONNECT object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(engineActivity) name:@"activityEngine" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopDetect) name:@"stopDetections" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(PauseEngine) name:@"PauseEngine" object:nil];
    // Do any additional setup after loading the view.
}
- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.homeTopView.frame = CGRectMake(0, 0, SCREEN_WIDTH, START_Y);
    self.tableView.frame = CGRectMake(0, START_Y, self.view.hj_width, self.view.hj_height - START_Y);
    
}

- (void)viewWillAppear:(BOOL)animated
{
    //For testing
//    totalCount = 1;
//    timeIntervalDisplay = SUTA_SOUND_DETECT_RESUME;
//    [self startRefreshTime];
//    [self startTimer];
    [super viewWillAppear:animated];
    if(APPDELEGATE.kdetectionMode == true){
        APPDELEGATE.kdetectionMode = false;
        [self btnSwitch_Click:nil];
    }
    [STBlueManage blueManageShare].scanConfromEquipmentBlock = ^(STBlueModel *model){
        
        BOOL isHas = NO;
        
        for (STBlueModel *oldM in arrDeviceList) {
            if ([oldM.peripheralUUID isEqualToString:model.peripheralUUID]) {
                oldM.peripheralRssi = model.peripheralRssi;
                isHas = YES;
            }
        }
        
        if (!isHas) {
            [arrDeviceList addObject:model];
        }
        
        if (arrDeviceList.count == 0) {
            self.viewNoDevice.hidden=false;
        }else{
            self.viewNoDevice.hidden=true;
        }
        [self.viewPopup setNeedsLayout];
        [self.tblDevice reloadData];
    };
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-  (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self stopTimer];
}
#pragma mark - Timer method
- (void) startTimer
{
    [self stopTimer];
    totalCount += 1;
    NSTimeInterval stopInterval = 0.0;
    if (totalCount == 1) {
        [self invalidateStopHardware];
        stopInterval = SUTA_SOUND_DETECT_FIRST;
    }else if (totalCount == 2) {
        [self invalidateStopHardware];
        stopInterval = SUTA_SOUND_DETECT_SECOND;
    }else if (totalCount == 3) {
        [self invalidateStopHardware];
        stopInterval = SUTA_SOUND_DETECT_THIRD;
    }else if (totalCount == 4) {
        stopInterval = SUTA_SOUND_DETECT_FOURTH;
        
        stopHardwareTimer = [NSTimer scheduledTimerWithTimeInterval: stopInterval + SUTA_SOUND_DETECT_STOP_HARDWARE  target: self selector:@selector(stopHardwareCall) userInfo: nil repeats:NO];

    }else{
        [self stopTimer];
        totalCount = 0;
        return;
    }
    
    NSLog(@"Sound Detected %d time",totalCount);
    
    starTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0 target: self selector:@selector(callTrigerHardware) userInfo: nil repeats:YES];
    stopTimer = [NSTimer scheduledTimerWithTimeInterval: stopInterval target: self selector:@selector(callStopHardware) userInfo: nil repeats:NO];
    
}

- (void) stopTimer
{
    if (starTimer) {
        [starTimer invalidate];
        starTimer = nil;
    }
    
    if (stopTimer) {
        [stopTimer invalidate];
        stopTimer = nil;
    }
    NSLog(@"Timer Stopped");
    if (isSwitchOn) {//change
        resumeTimer = [NSTimer scheduledTimerWithTimeInterval: SUTA_SOUND_DETECT_RESUME target: self selector:@selector(resumeSoundDetection) userInfo: nil repeats:YES];
        
        if (totalCount > 0 && totalCount < 4) {
            [self invalidateDisplayTimer];
            timeIntervalDisplay = SUTA_SOUND_DETECT_RESUME;
            [self startRefreshTime];
        }
    }
}
#pragma mark - Display timer function START
-(void)invalidateDisplayTimer{
    timeIntervalDisplay = 0;
    if (displayTimer != nil) {
        [displayTimer invalidate];
        displayTimer = nil;
    }
    [self.tblDevice reloadData];
}
-(void)startRefreshTime{
    [self.tableView reloadData];
    displayTimer = [NSTimer scheduledTimerWithTimeInterval: 1 target: self selector:@selector(refreshTime) userInfo: nil repeats:YES];
    [NSRunLoop.mainRunLoop addTimer:displayTimer forMode:NSRunLoopCommonModes];
}

-(void)refreshTime{
    [self reduceTimer:1];
}
-(void)reduceTimer:(NSTimeInterval)reduceSeconds{
    if (reduceSeconds == -1) {
        timeIntervalDisplay = 0;
    } else {

        timeIntervalDisplay = timeIntervalDisplay - reduceSeconds;
        if (timeIntervalDisplay < 0) {
            timeIntervalDisplay = 0;
        }
    }
    
    if (timeIntervalDisplay > 0) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.modelArray.count - 1 inSection:0];
        SnoringTableCell * cell = [self.tableView cellForRowAtIndexPath:indexPath];
        if (cell != nil) {
            cell.lblTitle.text = [CommenMethod getRemainTimeFormat:timeIntervalDisplay];
        }
    } else {
        
        [self invalidateDisplayTimer];
        [self.tableView reloadData];
    }
}
//Display timer function END
-(void)resumeSoundDetection{
    if (resumeTimer) {
        [resumeTimer invalidate];
        resumeTimer = nil;
    }
    [self startDetection];
}
-(void)invalidateStopHardware{
    if (stopHardwareTimer) {
        [stopHardwareTimer invalidate];
        stopHardwareTimer = nil;
    }
}

-(void)stopHardwareCall{
    //disable the timer
    [self invalidateStopHardware];
   
    //send flat command to hardware
    NSString *stopCommand = @"0100001000";
    NSData *data = [SutaGlobalData hexToBytes:stopCommand];
    
    if (data) {
        [[STBlueManage blueManageShare] writeCurrentPeripheralWihtData:data];
    }
}
#pragma mark - Triger and Stop Hardware

-(void)callTrigerHardware{
    NSData *data = nil;
    NSString *bineCommandStr = COMMAND;
    NSString *lastCommandStr = DATA_BACK_UP;
    NSString *defBedSide = LEFT_AND_RIGHT_BED;
    if (bineCommandStr.length && lastCommandStr.length) {
        data =[SutaGlobalData combineCommand:bineCommandStr middleCommand:defBedSide lastCommand:lastCommandStr];
    }
    
    if (data) {
        [[STBlueManage blueManageShare] writeCurrentPeripheralWihtData:data];
    }
}
-(void)callStopHardware{
    
    NSString *stopCommand = @"0100000000";
    NSData *data = [SutaGlobalData hexToBytes:stopCommand];
    
    if (data) {
        [[STBlueManage blueManageShare] writeCurrentPeripheralWihtData:data];
    }
    [self stopTimer];
    if (totalCount == 4) {
        [self invalidateDisplayTimer];
        timeIntervalDisplay = SUTA_SOUND_DETECT_STOP_HARDWARE;
        [self startRefreshTime];
    }
    
}
#pragma mark -UI Setup and basic method
- (void)setupInit
{
    self.btnScan.layer.cornerRadius = 0.5;
    self.btnScan.layer.masksToBounds = true;
    
    // 移除 NAv
    [self.NavBackView removeFromSuperview];
    [self.NavLeftBtn removeFromSuperview];
    [self.NavRightBtn removeFromSuperview];
    
    // 添加顶部
    self.homeTopView = [STHomeTopView homeTopView];
    self.homeTopView.upLabel.text = @"POSITIONS";
    self.homeTopView.modelLabel.hidden = YES;
    [self.view addSubview:self.homeTopView];
    
    // tableview
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = UITableViewAutomaticDimension;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    if ([self.tableView respondsToSelector:@selector(layoutMargins)]) {
        self.tableView.layoutMargins = UIEdgeInsetsZero;
    }
    [self.tblDevice registerNib:[UINib nibWithNibName:NSStringFromClass([STPeripheralInfoCell class]) bundle:nil] forCellReuseIdentifier:@"STPeripheralInfoCellId"];
    [self.view addSubview:self.tableView];
}

- (void)loadData
{
    _modelArray = [[NSMutableArray alloc]init];
    arrDeviceList = [[NSMutableArray alloc]init];
    totalCount = 0;
    m_nDetectedSoundType = 0;
    SnoringModel *snoringM = [[SnoringModel alloc] init];
    snoringM.titleName = @"Detecting Snoring Sound";
    snoringM.cellType = @"Switch";
    [self.modelArray addObject:snoringM];
    
    snoringM = [[SnoringModel alloc] init];
    snoringM.titleName = isSwitchOn ? @"Status : CONNECTED" : @"Status : DISCONNECTED";
    snoringM.cellType = @"Header";
    [self.modelArray addObject:snoringM];
    
    snoringM = [[SnoringModel alloc] init];
    snoringM.titleName = @"1) If the app detected snoring sound for the first time the app will raise the head part 7 degrees";
    snoringM.cellType = @"Header";
    [self.modelArray addObject:snoringM];
    
    snoringM = [[SnoringModel alloc] init];
    snoringM.titleName = @"2) If the app detect the snoring sounds for a second time will raise the head part 13.5 degrees";
    snoringM.cellType = @"Header";
    [self.modelArray addObject:snoringM];
    
    snoringM = [[SnoringModel alloc] init];
    snoringM.titleName = @"3) If the app detect the snoring sounds for a third time will raise the head part 20 degrees";
    snoringM.cellType = @"Header";
    [self.modelArray addObject:snoringM];
    
    snoringM = [[SnoringModel alloc] init];
    snoringM.titleName = @"4) If the app detect the snoring sounds for a fourth time will raise the head part 26.4 degrees";
    snoringM.cellType = @"Header";
    [self.modelArray addObject:snoringM];
   
    snoringM = [[SnoringModel alloc] init];
    snoringM.titleName = @"";
    snoringM.cellType = @"Timer";
    [self.modelArray addObject:snoringM];
    
    [self.tableView reloadData];
}

-(void)btnSwitch_Click:(id)sender{
//    UIButton *btn = (UIButton *)sender;
//    NSIndexPath *index= [NSIndexPath indexPathForRow:btn.tag inSection:0];
//    SnoringTableCell * cell = [self.tableView cellForRowAtIndexPath:index];
//
    isDetectDevice = !isDetectDevice;
    if (isSwitchOn) {
        NSLog(@"Switch is Off!");
        isSwitchOn = false;
        [self loadData];
        [self stopDetection];
//        [self stopDetect];
        [self disconnectPeripheral];
        [self stopTimer];
        [self terminateEngine];
        [self disconnectPeriphre];
        [self invalidateDisplayTimer];
    }else{
        NSLog(@"Switch is ON!");
        [self scanDevice];
        [self showPopupWithStyle:CNPPopupStyleCentered];
    }
}
/*
-(void)btnSwitch_Click:(id)sender{
    isDetectDevice = !isDetectDevice;
    if (isSwitchOn && APPDELEGATE.kdetectionMode) {
        APPDELEGATE.kdetectionMode = false;
        NSLog(@"Switch is Off!");
        isSwitchOn = false;
        [self loadData];
        [self stopDetection];
        //        [self stopDetect];
        [self disconnectPeripheral];
        [self stopTimer];
        //     [self terminateEngine];
        [self disconnectPeriphre];
    }else{
        NSLog(@"Switch is ON!");
        APPDELEGATE.kdetectionMode = true;
//        [self scanDevice];
//        [self showPopupWithStyle:CNPPopupStyleCentered];
        [self connetPeripheralSuccess];
    }
}*/
-(void)scanDevice{
    
    [[STBlueManage blueManageShare] startScanWitTypeName:SUTA_500I];
    
}
#pragma mark - notification
- (void)connetPeripheralSuccess
{
    
    if (isDetectDevice == false) {
        return;
    }
    NSLog(@"connetPeripheralSuccess!");

    [self engineActivity];
    [self.popupController dismissPopupControllerAnimated:YES];
    isSwitchOn = true;

    nAlarmType = 0;
    SoundPlaying = false;
    
    [SVProgressHUD dismiss];
    [arrDeviceList removeAllObjects];
    [self loadData];
    [[STBlueManage blueManageShare] startScanWitTypeName:SUTA_500I];
    //Added later
    [self startDetection];
    
}
/*
- (void)connetPeripheralSuccess
{
    
    
    NSLog(@"connetPeripheralSuccess!");
    
    [self engineActivity];
    [self.popupController dismissPopupControllerAnimated:YES];
    isSwitchOn = true;
    
    nAlarmType = 0;
    SoundPlaying = false;
    
    [SVProgressHUD dismiss];
    [arrDeviceList removeAllObjects];
    [self loadData];
    [[STBlueManage blueManageShare] startScanWitTypeName:SUTA_500I];
    //Added later
    [self startDetection];
    
}*/


- (void)refreshList
{
    isSwitchOn = false;
    [self loadData];
    [arrDeviceList removeAllObjects];
}

- (void)disconnectPeripheral
{
    [self refreshList];
}
- (void)disconnectPeriphre
{
    STBlueManage *mamage = [STBlueManage blueManageShare];
    [mamage disconnect];
}

- (void)terminateEngine
{
    NSLog(@"terminateEngine");
    [bridgeClass terminateEngine];
}

#pragma mark -Show Popup
- (void)showPopupWithStyle:(CNPPopupStyle)popupStyle {
    
    if (isPopupPresented) {
        return;
    }
    viewPopup.frame = CGRectMake(0, 0, SCREEN_WIDTH - 50, (SCREEN_WIDTH - 50));
    self.popupController = [[CNPPopupController alloc] initWithContents:@[viewPopup]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:YES];
}
#pragma mark - CNPPopupController Delegate

- (void)popupControllerDidDismiss:(nonnull CNPPopupController *)controller{
    //  NSLog(@"Dismissed CNPPopupController");
    isPopupPresented = false;
}

- (void)popupControllerWillPresent:(nonnull CNPPopupController *)controller{
    //  NSLog(@"Popup controller presented.");
    isPopupPresented = true;
}
#pragma mark - Popup Button Action

- (IBAction)btn_ClosePopup:(id)sender {
    [self.popupController dismissPopupControllerAnimated:YES];
}
- (IBAction)btn_ScanPopup:(id)sender {
    [self scanDevice];
}

#pragma mark - table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView.tag == 0){
        return self.modelArray.count;
    }
    else{
        return arrDeviceList.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag == 0){
        SnoringModel * model = self.modelArray[indexPath.row];
        
        if ([model.cellType isEqualToString:@"Switch"]) {
            SnoringTableCell *cell = (SnoringTableCell *)[tableView dequeueReusableCellWithIdentifier:@"SnoringTableCell0"];
            if (!cell) {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SnoringTableCell" owner:self options:nil];
                cell = [nib objectAtIndex:0];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                [cell.btnSwitch addTarget:self action:@selector(btnSwitch_Click:) forControlEvents:UIControlEventValueChanged];
                
            }
            if (isSwitchOn) {
                [cell.btnSwitch setOn:true animated:false];
            }else{
                [cell.btnSwitch setOn:false animated:false];
            }
            cell.btnSwitch.tag = indexPath.row;
            cell.lblTitle.text = model.titleName;
            return cell;
        }else{
            SnoringTableCell *cell = (SnoringTableCell *)[tableView dequeueReusableCellWithIdentifier:@"SnoringTableCell1"];
            if (!cell) {
                NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"SnoringTableCell" owner:self options:nil];
                cell = [nib objectAtIndex:1];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
             if ([model.cellType isEqualToString:@"Timer"]){
                 cell.lblTitle.textAlignment = NSTextAlignmentCenter;
                 if (totalCount > 0){
                     if (timeIntervalDisplay > 0){
                         cell.lblTitle.text = [CommenMethod getRemainTimeFormat:timeIntervalDisplay];
                     }else{
                         cell.lblTitle.text = @"00:00";
                     }
                 }else{
                     cell.lblTitle.text = @"";
                 }
                 cell.lblTitle.textColor = [UIColor blueColor];
                 cell.lblTitle.font = [UIFont systemFontOfSize:34];
             }else{
                 
                 cell.lblTitle.textAlignment = NSTextAlignmentLeft;
                 cell.lblTitle.text = model.titleName;
                 if (indexPath.row == (totalCount+1) && indexPath.row > 1){
                     cell.lblTitle.textColor = [UIColor blueColor];
                     cell.lblTitle.font = [UIFont boldSystemFontOfSize:17];
                 }else{
                     cell.lblTitle.textColor = [UIColor blackColor];
                     cell.lblTitle.font = [UIFont systemFontOfSize:17];
                 }
             }
            
            return cell;
        }
    }else{
        STPeripheralInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"STPeripheralInfoCellId"];
        cell.dataModel = arrDeviceList[indexPath.row];
        return cell;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag == 1 && self.arrDeviceList.count > 0){
        STBlueManage *manage = [STBlueManage blueManageShare];
        
        STBlueModel *dataModel = self.arrDeviceList[indexPath.row];
        NSLog(@"Device selected %@", dataModel.peripheralName);

        if(manage.blueState == CBManagerStatePoweredOff){
            [ToastManager showToastWithString:@"Open Bluetooth, please"];
            return;
        }
        [manage connect:dataModel.peripheral];
        manage.connectPeripheraldidFailBlock = ^ (CBPeripheral *peripheral) {
            
            NSString *title = [NSString stringWithFormat:@"【%@】Device connection failed",peripheral.name];
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:title
                                          message:@"Reconnection ?"
                                          preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction* ok = [UIAlertAction
                                 actionWithTitle:@"Reconnect"
                                 style:UIAlertActionStyleDefault
                                 handler:^(UIAlertAction * action){
                                     [[STBlueManage blueManageShare] connect:dataModel.peripheral];
                                 }];
            UIAlertAction* cancel = [UIAlertAction
                                     actionWithTitle:@"Cancel"
                                     style:UIAlertActionStyleDestructive
                                     handler:^(UIAlertAction * action){
                                         
                                     }];
            [alert addAction:ok];
            [alert addAction:cancel];
            [self.navigationController presentViewController:alert animated:YES completion:nil];
        };
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}
-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}
#pragma mark - SnoRelief code for sound detection

- (void)engineActivity{
//    if(![bridgeClass get_g_isEngineTerminated]){
//        return;
//    }
    
    [SharedInstance shared].profileMode = @"Home Mode";
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(detectingSounds) name:@"DetectMe" object:nil];
    
    [bridgeClass set_g_isEngineTerminated:false];
    [bridgeClass set_g_bDetecting:false];
    
    [SharedInstance shared].detectSound = false;
    
    [self initEngine];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self detectingSounds];
    });
    
    [self initData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopDetection) name:@"stopDetection" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(restartEngine) name:@"restartEngine" object:nil];
}
//-(void) stopDetect{
//    if([bridgeClass get_g_bDetecting]){
//        [bridgeClass set_g_isEngineTerminated:true];
//    }
//}

//-(void)stopBluetoothDetect{
//    NSLog(@"Bluetooth Trminated");
//    [[STBlueManage blueManageShare]stopToDetect];
//}
-(void) restartEngine{
    [bridgeClass restartEngine];
}
-(void) initData{
    [self loadThresholdDeltas];
    
    int RST_COUNT = (int)[bridgeClass get_RST_COUNT];
    
    for(int i = 0; i < RST_COUNT ; i++){
        NSString * strDetectPath = [bridgeClass getDetectPath1:@"Home Mode" recordSoundType:(int32_t)(i)];
        NSString * strDet = [strDetectPath stringByReplacingOccurrencesOfString:@"file://" withString:@""];
        [bridgeClass initData:strDet :(int32_t)(i)];
    }
}
-(void) PauseEngine{
    if(APPDELEGATE.kdetectionMode == false){
        kpushDetection = true;
    }
}

-(void) initEngine{
    [bridgeClass set_g_pDetectMgr_SAMPLE_FREQ];
    [self applyDetectedParamToEngine];
    [bridgeClass initEngine];
    [self loadThresholdDeltas];
}
-(void) applyDetectedParamToEngine{
    if ([bridgeClass get_g_pDetectMgr_isnill] == false) {
        return;
    }
    [bridgeClass set_SetDetectSmokeAlarmOnly:false];
}
-(void) loadThresholdDeltas{
    NSString * strKey = @"";
    NSString * strMode = @"";
    if([[NSUserDefaults standardUserDefaults] valueForKey: @"Mode"] == nil ){
        strMode = @"Home";
    }else{
        strMode = [[NSUserDefaults standardUserDefaults] valueForKey: @"Mode"];
    }
    int RST_COUNT = (int)[bridgeClass get_RST_COUNT];
    for(int i = 0; i < RST_COUNT ; i++){
        strKey = [NSString stringWithFormat:@"MATCH_RATE_DELTA_%@_%d",strMode,i];
        [bridgeClass set_MATCHING_RATE_THRESHOLD_DELTAS:(int32_t)(i) :strKey];
    }
    strKey = [NSString stringWithFormat:@"UNIVERSAL_THRESHOLD_DELTA_%@",strMode];
    [bridgeClass set_UNIVERSAL_THRESHOLD_DELTA:strKey];
}
-(void)detectingSounds{
    NSLog(@"detectingSounds");

    nAlarmType = m_nDetectedSoundType;
    int nAlarmIdx = 0;
    while(true) {
   // NSLog(@"detectingSounds While true");
        if([bridgeClass get_g_isEngineTerminated]){
            break;
        }
        APPDELEGATE.isEngineWorking = true;
        while ([bridgeClass get_ReadData:[bridgeClass get_g_fBufData] :[bridgeClass get_FRAME_LEN]]) {
            if([bridgeClass get_g_isEngineTerminated]){
                break;
            }
            APPDELEGATE.isEngineWorking = true;
            if ([bridgeClass get_g_pDetectMgr_isnill] == true){
                if ([bridgeClass get_g_bDetecting] || [bridgeClass get_g_pDetectMgr_IsRecordingOrPreparing]){
                    if ([bridgeClass get_g_pDetectMgr_Process:[bridgeClass get_g_fBufData] :[bridgeClass get_FRAME_LEN] :(int32_t)(nAlarmType):(int32_t)(nAlarmIdx)]){
                        if(SoundPlaying == true){
                            
                        }else{
                            nAlarmType = (int)([bridgeClass globalsMethodVarible_get_g_pDetectMgr_alarmType]);
                            
                            m_nDetectedSoundType = nAlarmType;
                            
                            dispatch_async(dispatch_get_main_queue(), ^(void){
                                [self applyDetectedParamToEngine];
                            });
                            
                            if(isSwitchOn == true){
                                NSLog(@"[self soundDetected:nAlarmType]");
                                [self soundDetected:nAlarmType];
                            }
                        }
                    }
                }
            }
        }
        usleep(150000);
    }
}
-(void) updateDoorbellDetectedFromNotification:(NSNotification *)soundType{
    NSMutableArray * soundIndex =  soundType.object;
    int soundIndx = (int)( [soundIndex objectAtIndex:0]);
    if(APPDELEGATE.kdetectionMode == false){
        APPDELEGATE.kdetectionMode = true;
    }
    [self soundDetected:soundIndx];
}

-(void) soundDetected:(int)soundType{
    [self stopDetection];//startstop
    if(kpushDetection == false){
        APPDELEGATE.kdetectionMode = true;
    }
    kpushDetection = false;
    dispatch_async(dispatch_get_main_queue(), ^(void){
        NSLog(@"Timer Started");
        [self startTimer];
    });
}

-(void) startDetection{
    [bridgeClass set_g_bDetecting:true];
    APPDELEGATE.kdetectionMode = true;
    SharedInstance * sharedInstace = [SharedInstance shared];
    NSLog(@"Sound Detection started");

    if(APPDELEGATE.kdetectionMode == true){
        sharedInstace.background = false;
        int RST_COUNT = (int)[bridgeClass get_RST_COUNT];
        int nTotalCnt = 0;
        for(int i = 0; i < RST_COUNT ; i++){
            nTotalCnt = [bridgeClass get_g_DetectData:(int32_t)(nTotalCnt) :(int32_t)(i)];
        }
    }else{
        sharedInstace.background = false;
    }
}
-(void) stopDetection{
    [ToastManager showToastWithString:@"Sound detection stoped"];
    APPDELEGATE.kdetectionMode = false;
    [bridgeClass set_g_bDetecting:false];
    NSLog(@"Sound Detection stopped");
}



@end;
