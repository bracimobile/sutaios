//
//  STHomeViewController.m
//  SUTA
//
//  Created by 张海军 on 17/3/21.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import "STHomeViewController.h"
#import "STHomeTopView.h"
#import "STHomeSetView.h"
#import "STNineZeroModelView.h"
#import "STFiveZeroModelView.h"
#import "STBlueManage.h"

@interface STHomeViewController ()
/// topView
@property (nonatomic, strong) STHomeTopView *homeTopView;
/// topView
@property (nonatomic, strong) STHomeSetView *typeSetView;
/// 900 i
@property (nonatomic, strong) STNineZeroModelView *nineZeroView;
/// 500 i
@property (nonatomic, strong) STFiveZeroModelView *fiveZeroView;
/// 当前动画view
@property (nonatomic, strong) STZeroModelBaseView *animationView;
@end

@implementation STHomeViewController
#pragma mark - life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupInit];
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    // 进入前台
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(continueAnimation) name:UIApplicationWillEnterForegroundNotification object:nil];
    
    
    
    // 进入后台
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(disconnectPeriphre) name:UIApplicationDidEnterBackgroundNotification object:nil];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.homeTopView.frame = CGRectMake(0, 0, SCREEN_WIDTH, START_Y);
    self.typeSetView.frame = CGRectMake(0, START_Y, self.view.hj_width, self.view.hj_height - START_Y);
    self.nineZeroView.frame = CGRectMake(0, START_Y, self.view.hj_width, self.view.hj_height - START_Y);
    self.fiveZeroView.frame = CGRectMake(0, START_Y, self.view.hj_width, self.view.hj_height - START_Y);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    WS(weakSelf);
    [STBlueManage blueManageShare].connetPeripheralSuccessBlock = ^(STBlueManage *manage){
        
        self.typeSetView.hidden = YES;
        
    
        if ([CURRENT_SELETE_SUTATYPE isEqualToString:SUTA_500I]) {
            self.fiveZeroView.hidden = NO;
            self.nineZeroView.hidden = YES;
            self.homeTopView.modelLabel.text = SUTA_500I;
        }else if ([CURRENT_SELETE_SUTATYPE isEqualToString:SUTA_900I]){
            self.fiveZeroView.hidden = YES;
            self.nineZeroView.hidden = NO;
            self.homeTopView.modelLabel.text = SUTA_900I;
        }
        
    };
    
    [STBlueManage blueManageShare].didDisconnectPeripheralBlock = ^(CBPeripheral *peripheral){
        if ([CURRENT_SELETE_SUTATYPE isEqual:SUTA_500I]) {
            [self.fiveZeroView  setLightColorForIndex:4];
        }else if([CURRENT_SELETE_SUTATYPE isEqual:SUTA_900I]){
            [self.nineZeroView  setLightColorForIndex:4];
        }
        self.homeTopView.modelLabel.text = @"disconnect";
    };
    
    // 连接设备返回值
    [STBlueManage blueManageShare].connectPeripherCallBack = ^(NSInteger value){
        NSInteger newValue = value + 1;
        if (newValue == 3) {
            newValue = 0;
        }
//        NSLog(@"newValue = %zd",newValue);
        self.nineZeroView.seleteLongButtonCount = newValue;
        self.fiveZeroView.seleteLongButtonCount = newValue;
        
        if ([CURRENT_SELETE_SUTATYPE isEqual:SUTA_500I]) {
            [self.fiveZeroView  setLightColorForIndex:self.fiveZeroView.seleteLongButtonCount];
        }else if([CURRENT_SELETE_SUTATYPE isEqual:SUTA_900I]){
            [self.nineZeroView  setLightColorForIndex:self.nineZeroView.seleteLongButtonCount];
        }
        
        
    };
    
    [STBlueManage blueManageShare].sixSBlock = ^(){
        
        if ([CURRENT_SELETE_SUTATYPE isEqual:SUTA_500I]) {
            
        }else if([CURRENT_SELETE_SUTATYPE isEqual:SUTA_900I]){
            
            [self.nineZeroView btnPressedDown:self.nineZeroView.btnLight];
            [self.nineZeroView btnPressedCancel:self.nineZeroView.btnLight];
        }
        
        
    };
    
    [self AnimationTypeView];
    
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    if (self.animationView.isAnimationing) {
        [self.animationView.currentAnimView.layer removeAllAnimations];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 初始化
- (void)setupInit
{
    WS(weakSelf);
    
    // 移除 NAv
    [self.NavBackView removeFromSuperview];
    [self.NavLeftBtn removeFromSuperview];
    [self.NavRightBtn removeFromSuperview];
    
    // 添加顶部
    self.homeTopView = [STHomeTopView homeTopView];
    self.homeTopView.modelLabel.text = @"";
    [self.view addSubview:self.homeTopView];
    
    // 型号设置view
    self.typeSetView = [[STHomeSetView alloc] init];
    //self.typeSetView.hidden = YES;
    self.typeSetView.setButtonDidClick = ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:BEGINSCARNDEVICE object:nil];
        [[STBlueManage blueManageShare] startScanWitTypeName:CURRENT_SELETE_SUTATYPE];
        [weakSelf.tabBarController setSelectedIndex:3];
    };
    [self.view addSubview:self.typeSetView];
    
    
    // 900 i
    self.nineZeroView = [[STNineZeroModelView alloc] init];
    [self.view addSubview:self.nineZeroView];
    self.nineZeroView.lightAnimBlock = ^(UIView *view){
        [weakSelf lightAnimWithView:view];
        weakSelf.animationView = weakSelf.nineZeroView;
        weakSelf.nineZeroView.currentAnimView = view;
    };
    self.nineZeroView.hidden = YES;
    
    self.fiveZeroView = [[STFiveZeroModelView alloc] init];
    [self.view addSubview:self.fiveZeroView];
    self.fiveZeroView.lightAnimBlock = ^(UIView *view){
        [weakSelf lightAnimWithView:view];
        weakSelf.animationView = weakSelf.fiveZeroView;
        weakSelf.fiveZeroView.currentAnimView = view;
    };
    self.fiveZeroView.hidden = YES;
    
}

- (void)lightAnimWithView:(UIView *)view
{
    [UIView animateWithDuration:0.3 animations:^{
        [UIView setAnimationRepeatCount:MAXFLOAT];
        [UIView setAnimationRepeatAutoreverses:YES];
        //动画变化
        view.alpha = 0.1;
    }];
}

- (void)AnimationTypeView
{
    if (self.animationView.isAnimationing) {
        self.animationView.currentAnimView.alpha = 1;
        [UIView animateWithDuration:0.3 animations:^{
            [UIView setAnimationRepeatCount:MAXFLOAT];
            [UIView setAnimationRepeatAutoreverses:YES];
            //动画变化
            self.animationView.currentAnimView.alpha = 0.1;
        }];
    }
}

- (void)continueAnimation
{
    [self AnimationTypeView];
    STBlueManage *mamage = [STBlueManage blueManageShare];
    if (CURRENT_SELETE_SUTATYPE.length) {
        [mamage startScanWitTypeName:CURRENT_SELETE_SUTATYPE];
    }
    if (mamage.currentPeripheral) {
         [ mamage connect:mamage.currentPeripheral];
    }
}

- (void)disconnectPeriphre
{
    STBlueManage *mamage = [STBlueManage blueManageShare];
    [mamage disconnect];
}


@end
