//
//  STPeripheralInfoCell.m
//  SUTA
//
//  Created by 张海军 on 2017/3/23.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import "STPeripheralInfoCell.h"

@interface STPeripheralInfoCell ()
@property (weak, nonatomic) IBOutlet UILabel *nameLable;
@property (weak, nonatomic) IBOutlet UILabel *macAddressLable;
@property (weak, nonatomic) IBOutlet UILabel *rssiLabel;

@end

@implementation STPeripheralInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.separatorInset = UIEdgeInsetsZero;
    if ([self respondsToSelector:@selector(layoutMargins)]) {
        self.layoutMargins = UIEdgeInsetsZero;
    }
}

- (void)setDataModel:(STBlueModel *)dataModel
{
    _dataModel = dataModel;
    self.nameLable.text = dataModel.peripheralName;
    self.macAddressLable.text = dataModel.peripheralUUID;
    self.rssiLabel.text = [NSString stringWithFormat:@"%zd",dataModel.peripheralRssi];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    
}

@end
