//
//  UIFactory.m
//  BaseTarget
//
//  Created by zmz on 15/8/31.
//  Copyright (c) 2015年 Shenzhen Tentinet Technology Co,. Ltd.. All rights reserved.
//

#import "UIFactory.h"
#import <CoreText/CoreText.h>

@implementation UIFactory
+ (UILabel *)createLabelWithFrame:(CGRect)frame NSTextAlignment:(NSTextAlignment)textAlignment Title:(NSString *)title Font:(UIFont *)font TextColor:(UIColor *)textColor Backgroundcolor:(UIColor *)backGroundcolor
{
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.textAlignment = textAlignment;
    label.text = title;
    label.font = font;
    label.textColor = textColor ? textColor : [UIColor blackColor];
    label.backgroundColor = backGroundcolor ? backGroundcolor : [UIColor clearColor];
    
    return label;
}

+ (UIButton *)createButtonWithFrame:(CGRect)frame LabelTitle:(NSString *)title NormalImage:(UIImage *)normalImage HighlightedImage:(UIImage *)HighlightedImage SelectImage:(UIImage *)selectImage TextFont:(UIFont *)font TitleColor:(UIColor *)titleColor BackGorundColor:(UIColor *)color Target:(id)target Action:(SEL)selector
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = frame;
    [button setTitle:title forState:UIControlStateNormal];
    

    
    [button setTitleColor:(titleColor ? titleColor : [UIColor blackColor]) forState:UIControlStateNormal];
    [button setBackgroundColor:(color ? color : [UIColor clearColor])];
    [button setImage:normalImage forState:UIControlStateNormal];
    [button setImage:HighlightedImage forState:UIControlStateHighlighted];
    [button setImage:selectImage forState:UIControlStateSelected];
    button.titleLabel.font = font ? font : [UIFont systemFontOfSize:14];
    
    [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}


+ (UIButton *)createButtonWithFrame:(CGRect)frame LabelTitle:(NSString *)title NormalBackgroundImage:(UIImage *)normalImage HighlightBackgroundImage:(UIImage *)HighlightImage SelectBackgroundImage:(UIImage *)selectImage TextFont:(UIFont *)font TitleColor:(UIColor *)titleColor BackGorundColor:(UIColor *)color Target:(id)target Action:(SEL)selector
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = frame;
    [button setTitle:title forState:UIControlStateNormal];
    
    [button setBackgroundImage:selectImage forState:UIControlStateSelected];
    [button setBackgroundImage:HighlightImage forState:UIControlStateHighlighted];
    [button setBackgroundImage:normalImage forState:UIControlStateNormal];
    
    [button setTitleColor:(titleColor ? titleColor : [UIColor blackColor]) forState:UIControlStateNormal];
    [button setBackgroundColor:(color ? color : [UIColor clearColor])];
    button.titleLabel.font = font ? font : [UIFont systemFontOfSize:14];
    [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}
+ (UIView *)createLineWithOriginaly:(CGFloat)originaly
{
    UIView *_lineView = [UIFactory createViewWithFrame:CGRectMake(0,originaly, SCREEN_WIDTH, 0.5) backgroundColor:RGBHEX_(#f0f0f0)];

    return _lineView;
}
+ (UIView *)createLineOfFifteenWithOriginaly:(CGFloat)originaly AndOf:(CGFloat)ofx
{
    UIView *_lineView = [UIFactory createViewWithFrame:CGRectMake(ofx,originaly, SCREEN_WIDTH - ofx, 0.5) backgroundColor:RGBHEX_(#f0f0f0)];
    
    return _lineView;
}

+ (UIView *)createLineOfFifteenWithOriginaly:(CGFloat)originaly AndOf:(CGFloat)ofx Width:(CGFloat)width
{
    UIView *_lineView = [UIFactory createViewWithFrame:CGRectMake(ofx,originaly, width, 0.5) backgroundColor:RGBHEX_(#f0f0f0)];
    
    return _lineView;
}


+ (UIView *)createLineOfFifteenWithOriginalx:(CGFloat)originalx Originaly:(CGFloat)Originaly height:(CGFloat)height
{
    UIView *_lineView = [UIFactory createViewWithFrame:CGRectMake(originalx,Originaly,0.5, height) backgroundColor:RGBHEX_(#f0f0f0)];
    
    return _lineView;
}

+ (UIView *)createViewWithFrame:(CGRect)frame backgroundColor:(UIColor *)color
{
    UIView *view = [[UIView alloc] initWithFrame:frame];
    view.backgroundColor = color ? color:[UIColor clearColor];
    
    return view;
}

/**
 * 添加左lable,右textField
 */
+ (UITextField *)createMessageCollectView:(CGRect)frame
                            leftLabelName:(NSString *)leftLabelName
                          rightPlaceHoder:(NSString *)rightPlaceHolder
                                rightText:(NSString *)rightText
                                 delegate:(id)delegate
                                      tag:(NSInteger)tag
                                   onView:(UIView *)view {
    UIView *backView = [[UIView alloc] initWithFrame:frame];
    backView.backgroundColor = RGBHEX_(#f4f4f4);
    backView.layer.cornerRadius = 4;
    backView.layer.masksToBounds = YES;
    [view addSubview:backView];
    
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:SysFont_(14), kCTFontAttributeName, nil];
    CGFloat leftWidth = [leftLabelName sizeWithAttributes:dic].width + 1;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, leftWidth, frame.size.height)];
    label.backgroundColor = [UIColor clearColor];
    label.text = leftLabelName;
    label.textColor = RGBHEX_(#424242);
    label.font = SysFont_(14);
    [backView addSubview:label];
    
    UITextField *text = [[UITextField alloc] initWithFrame:CGRectMake(leftWidth + 10, 0, frame.size.width - leftWidth - 10, frame.size.height)];
    text.borderStyle = UITextBorderStyleNone;
    text.text = rightText;
    text.textColor = RGBHEX_(#424242);
    text.tag = tag;
    text.font = SysFont_(14);
    text.delegate = delegate;
    text.placeholder = rightPlaceHolder;
    [backView addSubview:text];
    
    return text;
}

/**
 * 添加左lable,右label
 */
+ (UILabel *)createLabelAndLabel:(CGRect)frame
                       textColor:(UIColor *)color
                   leftLabelName:(NSString *)leftLabelName
                       rightText:(NSString *)rightText
                             tag:(NSInteger)tag
                          onView:(UIView *)view {
    UIView *backView = [[UIView alloc] initWithFrame:frame];
    backView.backgroundColor = [UIColor clearColor];
    backView.layer.cornerRadius = 4;
    backView.layer.masksToBounds = YES;
    [view addSubview:backView];
    
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:SysFont_(14), kCTFontAttributeName, nil];
    CGFloat leftWidth = [leftLabelName sizeWithAttributes:dic].width + 1;
    
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, leftWidth, frame.size.height)];
    label1.backgroundColor = [UIColor clearColor];
    label1.textColor = color ? color : [UIColor blackColor];
    label1.text = leftLabelName;
    label1.font = SysFont_(14);
    [backView addSubview:label1];
    
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(leftWidth + 10, 0, frame.size.width - leftWidth - 10, frame.size.height)];
    label2.backgroundColor = [UIColor clearColor];
    label2.textColor = color ? color : [UIColor blackColor];
    label2.text = rightText;
    label2.font = SysFont_(14);
    [backView addSubview:label2];
    
    return label2;
}

/**
 * 添加左lable,右button
 */
+ (UIButton *)createLabelAndButton:(CGRect)frame
                     leftLabelName:(NSString *)leftLabelName
                           btnText:(NSString *)btnText
                            target:(id)target
                          selector:(SEL)selector
                               tag:(NSInteger)tag
                            onView:(UIView *)view {
    UIView *backView = [[UIView alloc] initWithFrame:frame];
    backView.backgroundColor = RGBHEX_(#f4f4f4);
    backView.layer.cornerRadius = 4;
    backView.layer.masksToBounds = YES;
    [view addSubview:backView];
    
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:SysFont_(14), kCTFontAttributeName, nil];
    CGFloat leftWidth = [leftLabelName sizeWithAttributes:dic].width + 1;
    
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, leftWidth, frame.size.height)];
    label1.backgroundColor = [UIColor clearColor];
    label1.text = leftLabelName;
    label1.textColor = RGBHEX_(#424242);
    label1.font = SysFont_(14);
    [backView addSubview:label1];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(leftWidth + 10, 0, frame.size.width - leftWidth - 10, frame.size.height);
    [button setTitle:btnText forState:UIControlStateNormal];
    [button setTitleColor:RGBHEX_(#424242) forState:UIControlStateNormal];
    button.titleLabel.font = SysFont_(14);
    button.tag = tag;
    button.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 20);
    [backView addSubview:button];
    [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    
    UIImageView *imagv = [[UIImageView alloc] initWithFrame:CGRectMake(frame.size.width - 20, (frame.size.height - 7)/2, 14, 7)];
    imagv.image = [UIImage imageNamed:@"drop"];
    [backView addSubview:imagv];
    
    return button;
}

@end
