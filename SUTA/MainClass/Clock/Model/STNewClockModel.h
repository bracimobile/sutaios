//
//  STNewClockModel.h
//  SUTA
//
//  Created by 张海军 on 2017/5/18.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface STNewClockModel : NSObject
/// 唯一标识
@property (nonatomic, copy) NSString *identifier;
/** 时间 */
@property (nonatomic, copy) NSString *timeStr;
/** 详情 */
@property (nonatomic, copy) NSString *detaiStr;
/** 开关是否打开 */
@property (nonatomic, assign) BOOL isOpen;
/** 是否是编辑状态 */
@property (nonatomic, assign) BOOL isEdit;

/// 通知
@property (nonatomic, strong) UILocalNotification * notification;

/// 通知时间
@property (nonatomic, strong) NSString *fireDateStr;

/// 选中的星期 集合  <1,2,3>
@property (nonatomic, strong) NSMutableArray *seleteWeekNumArray;
/// 选中的时间
@property (nonatomic, copy) NSString *sleteTime;
/// 重复的星期
@property (nonatomic, copy) NSString *repetitionWeek;
/// 标签
@property (nonatomic, copy) NSString *labelStr;
/// 铃声
@property (nonatomic, copy) NSString *ringStr;
/// 是否稍后提醒
@property (nonatomic, assign) BOOL isLaterRemind;

@end


@interface NewClockEditModel : NSObject
/// 标题
@property (nonatomic, copy) NSString *titleStr;
/// 副标题
@property (nonatomic, copy) NSString *detaiStr;
/// 是否开启提醒
@property (nonatomic, assign) BOOL isOpenRemind;
/// 右边显示
@property (nonatomic) UITableViewCellAccessoryType  editAccessoryType;

/// 进入下一个页面选中的星期
@property (nonatomic, copy) NSString *sleteWeekStr;

+ (instancetype)clockEditModelWithTitle:(NSString *)title detaiStr:(NSString *)detaiStr;

@end


@interface ClockWeekSeleteModel : NSObject

/// 标题
@property (nonatomic, copy) NSString *titleStr;
/// 星期对应的数组
@property (nonatomic, strong) NSNumber *weekNum;
/// 是否选中
@property (nonatomic, assign) BOOL isSelete;

@end
