//
//  ToastManager.h
//  LEYAOGOSDK
//
//  Created by Chums on 2016/12/27.
//  Copyright © 2016年 baoqianli. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ToastManager : NSObject

/**
 显示toast
 
 @param string 要显示的内容
 */
+ (void)showToastWithString:(NSString*)string;

@end
