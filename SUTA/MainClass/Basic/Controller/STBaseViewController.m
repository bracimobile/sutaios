//
//  STBaseViewController.m
//  SUTA
//
//  Created by 张海军 on 17/3/21.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import "STBaseViewController.h"
#import "UIFactory.h"

@interface STBaseViewController ()

@end

@implementation STBaseViewController
#pragma mark - life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initNavigation];
    self.view.backgroundColor = [UIColor whiteColor];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 初始化
/**
 初始化导航栏按钮
 */
- (void)initNavigation{
    
    //背景
    self.NavBackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 64)];
    _NavBackView.backgroundColor = [UIColor blackColor]; //HJColor(24, 24, 24, 1);//[UIColor whiteColor];
    _NavBackView.clipsToBounds = NO;
    [self.view addSubview:_NavBackView];
    
    //标题
    self.NaviTitle = [[UILabel alloc] init];
    self.NaviTitle.frame = CGRectMake(45, 8, SCREEN_WIDTH-90, 40);
    //_NaviTitle.backgroundColor = [UIColor whiteColor];;
    _NaviTitle.textColor = RGBHEX_(c0a076);
    _NaviTitle.textAlignment = NSTextAlignmentCenter;
    _NaviTitle.font = [UIFont boldSystemFontOfSize:30];//SysFont_(17);
    [_NavBackView addSubview:_NaviTitle];
    
    _NavLineView = [UIFactory createViewWithFrame:CGRectMake(0,START_Y-0.5, SCREEN_WIDTH, 0.5) backgroundColor:RGBHEX_(#e3e3e3)];
    [_NavBackView addSubview:_NavLineView];
    
    // 默认左键
    self.NavLeftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _NavLeftBtn.frame = CGRectMake(0, 15, 90, 44);
    if (self.navigationController.childViewControllers.count > 1) {
        [_NavLeftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateNormal];
        [_NavLeftBtn setImage:[UIImage imageNamed:@"back"] forState:UIControlStateHighlighted];
    }
    _NavLeftBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 50);
    [_NavLeftBtn addTarget:self action:@selector(navigationClickLeft) forControlEvents:UIControlEventTouchUpInside];
    _NavLeftBtn.titleLabel.font = SysFont_(16);
    [_NavLeftBtn setTitleColor:RGBHEX_(c0a076) forState:UIControlStateNormal];
    [_NavBackView addSubview:_NavLeftBtn];
    
    // 默认右键
    self.NavRightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _NavRightBtn.frame = CGRectMake(SCREEN_WIDTH - 90,15, 90, 44);
    _NavRightBtn.userInteractionEnabled = NO;
    [_NavRightBtn addTarget:self action:@selector(navigationClickRight) forControlEvents:UIControlEventTouchUpInside];
    _NavRightBtn.titleLabel.font = SysFont_(16);
    [_NavRightBtn setTitleColor:RGBHEX_(c0a076) forState:UIControlStateNormal];
    [_NavBackView addSubview:_NavRightBtn];
    
}

-(void)replaceLeftButtonImage:(NSString *)imageName Title:(NSString *)title{
    UIImage *image = nil;
    if (imageName.length > 0) {
        image = [UIImage imageNamed:imageName];
    }
    [_NavLeftBtn setImage:image forState:UIControlStateNormal];
    [_NavLeftBtn setTitle:title forState:UIControlStateNormal];
    CGFloat imageWid = image ? image.size.width : 0;
    CGFloat widLab = 0;
    if (title.length) {
        widLab = [STPublicMethod sizeString:title WithFont:_NavLeftBtn.titleLabel.font constrainedToSize:CGSizeMake(HUGE_VALF, HUGE_VALF)].width;
    }
    _NavLeftBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 80 - imageWid - widLab);
    _NavLeftBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 90 - (imageWid > 0 ? imageWid + 10 : 0) - widLab-10);
}

- (void)replaceRightButtonImage:(NSString *)imageName Title:(NSString *)title{
    [_NavRightBtn setUserInteractionEnabled:YES];
    UIImage *image;
    if (imageName.length > 0) {
        image = [UIImage imageNamed:imageName];
    }
    [_NavRightBtn setImage:image forState:UIControlStateNormal];
    [_NavRightBtn setTitle:title forState:UIControlStateNormal];
    
    CGFloat imageWid = image ? image.size.width : 0;
    CGFloat widLab = 0;
    if (title.length) {
        widLab = [STPublicMethod sizeString:title WithFont:_NavRightBtn.titleLabel.font constrainedToSize:CGSizeMake(HUGE_VALF, HUGE_VALF)].width;
    }
    _NavRightBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 80 - (widLab > 0 ? widLab+15 : 7) - imageWid, 0, 0);
    _NavRightBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 90 - imageWid - widLab-10, 0, 7);
}

- (void)navigationClickLeft{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)navigationClickRight{
    
}

@end
