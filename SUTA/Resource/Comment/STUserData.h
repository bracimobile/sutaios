//
//  STUserData.h
//  SUTA
//
//  Created by 张海军 on 2017/3/22.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface STUserData : NSObject

/**
 从本地初始化 所有公共数据
 */
+ (void)getCommonDataInitFormLocation;

/**
 退出登录 / 账号异地登录  清空用户本地数据
 */
+ (void)loginOutRemoveCommonDataFormLocation;

@end
