//
//  STNewClockModel.m
//  SUTA
//
//  Created by 张海军 on 2017/5/18.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import "STNewClockModel.h"

@implementation STNewClockModel

- (NSString *)timeStr
{
    return self.sleteTime;
}

- (NSString *)detaiStr
{
    NSString *title = self.labelStr;
    if (self.repetitionWeek.length) {
        NSString *repetStr = self.repetitionWeek;
        NSArray *array = [self.repetitionWeek componentsSeparatedByString:@" "];
        if (array.count == 7) {
            repetStr = @"Every day";
        }
        title = [NSString stringWithFormat:@"%@,%@",self.labelStr,repetStr];
    }
    return title;
}

//- (NSMutableArray *)weekNumArray
//{
//    if (!_weekNumArray) {
//        _weekNumArray = [NSMutableArray array];
//    }
//    return _weekNumArray;
//}

@end


@implementation NewClockEditModel

+ (instancetype)clockEditModelWithTitle:(NSString *)title detaiStr:(NSString *)detaiStr
{
    NewClockEditModel *model = [[self alloc] init];
    model.titleStr = title ;
    model.detaiStr = detaiStr;
    return model;
}

@end


@implementation ClockWeekSeleteModel

- (NSNumber *)weekNum
{
    if ([self.titleStr isEqualToString:@"Sun"]) {
        return @7;
    }else if ([self.titleStr isEqualToString:@"Sat"]){
        return @6;
    }else if ([self.titleStr isEqualToString:@"Fri"]){
        return @5;
    }else if ([self.titleStr isEqualToString:@"Thu"]){
        return @4;
    }else if ([self.titleStr isEqualToString:@"Wed"]){
        return @3;
    }else if ([self.titleStr isEqualToString:@"Tue"]){
        return @2;
    }else if ([self.titleStr isEqualToString:@"Mon"]){
        return @1;
    }else{
        return nil;
    }
}

@end
