//
//  STHomeSetView.m
//  SUTA
//
//  Created by 张海军 on 2017/3/22.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import "STHomeSetView.h"

@interface STHomeSetView () <UIPickerViewDelegate, UIPickerViewDataSource>
/// logoImge
@property (nonatomic, strong) UIImageView *logoImageView;
/// 型号选择器
@property (nonatomic, strong) UIPickerView *typeSeletePickView;
/// 设置按钮
@property (nonatomic, strong) UIButton *setSureButton;

/// 数据数组
@property (nonatomic, strong) NSArray *dataArray;
@end

@implementation STHomeSetView
#pragma mark - life cycle
- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        [self setupInit];
        [self loadData];
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat imageW = SCREEN_WIDTH * 0.48;
    CGFloat imageX = (SCREEN_WIDTH - imageW) * 0.5;
    self.logoImageView.frame = CGRectMake(imageX, 32, imageW, SCREEN_HEIGHT * 0.20);
    
    CGFloat pickY = SCREEN_HEIGHT * 0.02 + CGRectGetMaxY(self.logoImageView.frame);
    CGFloat pickH = SCREEN_HEIGHT * 0.22;
    self.typeSeletePickView.frame = CGRectMake(0, pickY, self.hj_width, pickH);
    
    CGFloat buttonW = SCREEN_HEIGHT * 0.24;
    CGFloat buttonH = SCREEN_HEIGHT * 0.07;
    CGFloat buttonX = (self.hj_width - buttonW) * 0.5;
    CGFloat buttonY = CGRectGetMaxY(self.typeSeletePickView.frame) + SCREEN_HEIGHT * 0.05;
    self.setSureButton.frame = CGRectMake(buttonX, buttonY, buttonW, buttonH);
}

#pragma mark - 初始化
- (void)setupInit
{
    // logoImageView
    self.logoImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo"]];
    self.logoImageView.contentMode = UIViewContentModeScaleAspectFit;
    //self.logoImageView.backgroundColor = RandColor;
    [self addSubview:self.logoImageView];
    
    // typeSeletePickView
    self.typeSeletePickView = [[UIPickerView alloc] init];
    self.typeSeletePickView.dataSource = self;
    self.typeSeletePickView.delegate = self;
    //self.typeSeletePickView.backgroundColor = [UIColor redColor];
    [self addSubview:self.typeSeletePickView];
    
    // sureButton
    self.setSureButton = [[UIButton alloc] init];
    [self.setSureButton setTitle:@"Set" forState:UIControlStateNormal];
    [self.setSureButton setTitleColor:RGBHEX_(c0a076) forState:UIControlStateNormal];
    [self.setSureButton addTarget:self action:@selector(setSureButtonDidClick:) forControlEvents:UIControlEventTouchUpInside];
    self.setSureButton.layer.borderColor = RGBHEX_(c0a076).CGColor;
    self.setSureButton.layer.borderWidth = 1;
    [self.setSureButton hj_viewCornerRadiusValue:8.0];
    [self addSubview:self.setSureButton];
    
}

- (void)loadData
{
    self.dataArray = @[FIVEZERO_I,NINEZERO_I];
    [self.typeSeletePickView reloadAllComponents];
    
    NSString *currTypeStr = [[NSUserDefaults standardUserDefaults] valueForKey:@"CURRENT_SELETE_SUTATYPE"];
    if ([currTypeStr isEqualToString:SUTA_500I]) {
        [self.typeSeletePickView selectRow:0 inComponent:0 animated:YES];
    }else if ([currTypeStr isEqualToString:SUTA_900I]){
        [self.typeSeletePickView selectRow:1 inComponent:0 animated:YES];
    }
}

#pragma mark - data pick view data source
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.dataArray.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *title = self.dataArray[row];
    return title;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel* pickerLabel = (UILabel*)view;
    if (!pickerLabel){
        pickerLabel = [[UILabel alloc] init];
        pickerLabel.adjustsFontSizeToFitWidth = YES;
        pickerLabel.textAlignment = NSTextAlignmentCenter;
        pickerLabel.textColor = RGBHEX_(c0a076);
        [pickerLabel setFont:[UIFont boldSystemFontOfSize:20.0]];
    }
    pickerLabel.text=[self pickerView:pickerView titleForRow:row forComponent:component];
    return pickerLabel;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return SCREEN_HEIGHT * 0.07;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (row == 0) {
        CURRENT_SELETE_SUTATYPE = SUTA_500I;
    }else if(row == 1){
        CURRENT_SELETE_SUTATYPE = SUTA_900I;
    }
    [[NSUserDefaults standardUserDefaults] setValue:CURRENT_SELETE_SUTATYPE forKey:@"CURRENT_SELETE_SUTATYPE"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - event
- (void)setSureButtonDidClick:(UIButton *)button
{
    CURRENT_SELETE_SUTATYPE = [[NSUserDefaults standardUserDefaults] valueForKey:@"CURRENT_SELETE_SUTATYPE"];
    if (self.setButtonDidClick) {
        self.setButtonDidClick();
    }
}

@end
