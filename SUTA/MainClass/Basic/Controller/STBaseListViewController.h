//
//  STBaseListViewController.h
//  SUTA
//
//  Created by 张海军 on 2017/3/23.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import "STBaseViewController.h"
#import <UIScrollView+EmptyDataSet.h>
#import "CMRefreshHeader.h"
#import "CMRefreshFooter.h"

@interface STBaseListViewController : STBaseViewController
<DZNEmptyDataSetSource,
DZNEmptyDataSetDelegate>

typedef NS_ENUM(NSInteger, BaseListRefreshState_type)
{
    BaseListRefreshState_typeUp,         // 上拉
    BaseListRefreshState_typeDown,       // 下拉
    BaseListRefreshState_typeUpAndDown   // 上下拉
};


/// tableView
@property (nonatomic, strong) UITableView *tableView;
/// tableView 的样式  默认plan
@property (nonatomic, assign) BOOL isGroup;
/// 当前页
@property (nonatomic, assign) NSInteger currentPage;
/// 数据总个数
@property (nonatomic, assign) NSInteger totalNum;


/// 是否是加载失败
@property (nonatomic, assign) BOOL isLoadFaile;
/// 当前空白页状态是否是空状态
@property (nonatomic, assign) BOOL isEmptyStatus;
/// 加载失败的图片
@property (nonatomic, strong) UIImage *loadFaileImage;
/// 加载失败or没有数据时显示的文字
@property (nonatomic, copy) NSString *loadFaileStr;
/// 加载失败or没有数据时按钮显示的文字
@property (nonatomic, copy) NSString *loadFailButtonStr;
/**
 加载失败or没有数据时显示的带文字图片 注 : loadFailButtonTextImage 的优先级大于 loadFailButtonStr 不能并存
 通过下面的方法来创建带文字的图片
 [UIImage ImageTitleSize: imageColor: text: textFont: textColor:];
 */
@property (nonatomic, strong) UIImage *loadFailButtonTextImage;


/**
 listView 没有数据时需显示的东西
 
 @param imageN     图片 需设置 每个页面都不一样
 @param title      传入 nil 时为 空空如也~
 @param buttonText 有传入就显示  没传入不显示
 */
- (void)setEmptyImage:(NSString *)imageN text:(NSString *)title buttonText:(NSString *)buttonText;

/**
 listView 加载失败时需显示的东西
 
 @param imageF     有默认图片 / 可以传入nil
 @param title      默认为 加载失败  / 可以传入 nil
 @param buttonText 默认为 重新加载  / 可以传入 nil
 */
- (void)setFaileImage:(NSString *)imageF text:(NSString *)title buttonText:(NSString *)buttonText;

/**
 加载失败or没有数据时 刷新空白页
 */
- (void)loadFaileRefreshEmpty;

/**
 上下拉刷新
 
 @param refreshType 可选 上 / 下 拉
 */
- (void)refreshDataWithType:(BaseListRefreshState_type)refreshType;

/// 上下拉刷新的请求 state : YES 下拉 / NO 上拉
- (void)loadDataWithState:(BOOL)state;

/// 结束刷新
- (void)endRefreshing;

// 设置分割线全部填充
- (void)hj_setTableViewSeparator;


@end
