//
//  STUserData.m
//  SUTA
//
//  Created by 张海军 on 2017/3/22.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import "STUserData.h"

@implementation STUserData

+ (void)getCommonDataInitFormLocation
{
    if ([STPublicMethod isIPad]) {
        [self initPad];
    }else{
        [self initPhone];
    }
    
    //CURRENT_SELETE_SUTATYPE = [[NSUserDefaults standardUserDefaults] valueForKey:@"CURRENT_SELETE_SUTATYPE"];
}

+ (void)loginOutRemoveCommonDataFormLocation
{
    
}



#pragma mark - 赋值
+ (void) initPhone
{
    
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    
    screenWidth =[UIScreen mainScreen].bounds.size.width;
    screenHeight =[UIScreen mainScreen].bounds.size.height;
    
    centerScreenHeight = [UIScreen mainScreen].bounds.size.height-64-49;
    centerScreenWidth = [UIScreen mainScreen].bounds.size.width;
    if (height == 480) {
        //如果是iphone4s , 就缩小一下长度
        centerScreenWidth =centerScreenWidth - 20;
    }
    
    Blackheight = centerScreenHeight*0.8;
    Blackwidth = centerScreenWidth*0.848;
    
    leftMargin = [UIScreen mainScreen].bounds.size.width * 0.05;
    if (height == 480) {
        leftMargin += 10;
        Blackheight += 10;
    }
    rightMargin = [UIScreen mainScreen].bounds.size.width * 0.1;
    //背景图片坐标与宽高
    imgBgX =  centerScreenWidth * 0.106;
    imgBgY =  centerScreenHeight * 0.06;
    imgBgWidth = centerScreenWidth * 0.74;
    imgBgHeight = centerScreenHeight * 0.765;
    if (height == 480) {
        imgBgX += 10;
        imgBgHeight += 10;
        centerScreenHeight = imgBgHeight / 0.765;
    }
//    NSLog(@"screenHeight ----%f",centerScreenHeight);
    
    //右边的LIR 背景
    LIRBgX = centerScreenWidth * 0.875;
    LIRBgY = centerScreenHeight * 0.2;
    LIRBgWidth = centerScreenWidth * 0.1;
    LIRBgHeight = centerScreenHeight* 0.44;
    
    if (height == 480) {
        LIRBgX += 10;
    }
    
    
    //右边的LIR 文字
    LIRX = centerScreenWidth * 0.91;
    LIRY = centerScreenHeight * 0.25;
    LIRWidth = centerScreenWidth * 0.04;
    LIRHeight = centerScreenHeight* 0.34;
    
    if (height == 480) {
        LIRX += 10;
    }
    
    //按钮位置
    //10 灯
    LightTenX = centerScreenWidth*0.445;
    LightTenY  = centerScreenHeight*0.02;
    LightTenWidth  = centerScreenWidth*0.05;
    LightTenHeight = centerScreenHeight * 0.009;
    if (height == 480) {
        LightTenX += 10;
    }
    
    LightTenLabelX = centerScreenWidth*0.51;
    LightTenLabelY  =centerScreenHeight*0.012;
    LightTenLabelWidth = centerScreenWidth *0.05;
    LightTenLabelHeight = centerScreenWidth*0.032;
    if (height == 480) {
        LightTenLabelX += 10;
    }
    
    //20 灯
    LightTwentyX = centerScreenWidth*0.445;
    LightTwentyY =centerScreenHeight*0.05;
    LightTwentyWidth =centerScreenWidth*0.05;
    LightTwentyHeight =centerScreenHeight * 0.009;
    if (height == 480) {
        LightTwentyX += 10;
    }
    
    LightTwentyLabelX =centerScreenWidth*0.51;
    LightTwentyLabelY =centerScreenHeight*0.042;
    LightTwentyLabelWidth =centerScreenWidth *0.05;
    LightTwentyLabelHeight =centerScreenWidth*0.032;
    if (height == 480) {
        LightTwentyLabelX += 10;
    }
    
    //30灯
    LightThirtyX = centerScreenWidth*0.445;
    LightThirtyY =centerScreenHeight*0.08;
    LightThirtyWidth =centerScreenWidth*0.05;
    LightThirtyHeight =centerScreenHeight * 0.009;
    if (height == 480) {
        LightThirtyX += 10;
    }
    
    LightThirtyLabelX =centerScreenWidth*0.51;
    LightThirtyLabelY =centerScreenHeight*0.072;
    LightThirtyLabelWidth =centerScreenWidth *0.05;
    LightThirtyLabelHeight =centerScreenWidth*0.032;
    if (height == 480) {
        LightThirtyLabelX += 10;
    }
    
    
    headShakeX = centerScreenWidth*0.11;
    headShakeY = centerScreenHeight*0.06;
    headShakeWidth = centerScreenWidth *0.25;
    headShakeHeight = centerScreenHeight*0.171;
    if (height == 480) {
        headShakeX += 10;
    }
    
    switchModeX = centerScreenWidth*0.361;
    switchModeY = centerScreenHeight*0.12;
    switchModeWidth = centerScreenWidth *0.23;
    swithcModeHeight = centerScreenHeight*0.11;
    if (height == 480) {
        switchModeX += 10;
    }
    
    footShakeX = centerScreenWidth*0.6;
    footShakeY = centerScreenHeight*0.067;
    footShakeWidth = centerScreenWidth *0.25;
    footShakeHeight = centerScreenHeight*0.171;
    if (height == 480) {
        footShakeX += 10;
    }
    
    btnlX = centerScreenWidth*0.11;
    btnlY = centerScreenHeight*0.2;
    btnlWidth = centerScreenWidth *0.24;
    btnlHeight = centerScreenHeight *0.176;
    if (height == 480) {
        btnlX += 10;
    }
    
    btnllX = centerScreenWidth*0.361;
    btnllY = centerScreenHeight*0.23;
    btnllWidth = centerScreenWidth *0.23;
    btnllHeight = centerScreenHeight*0.11;
    if (height == 480) {
        btnllX += 10;
    }
    
    btnlllX = centerScreenWidth*0.605;
    btnlllY = centerScreenHeight*0.20;
    btnlllWidth = centerScreenWidth *0.23;
    btnlllHeight = centerScreenHeight*0.175;
    if (height == 480) {
        btnlllX += 10;
    }
    
    backUpX = centerScreenWidth*0.11;
    backUpY = centerScreenHeight*0.345;
    backUpWidth = centerScreenWidth *0.24;
    backUpHeight = centerScreenHeight*0.11;
    if (height == 480) {
        backUpX += 10;
    }
    
    backDownX = centerScreenWidth*0.11;
    backDownY = centerScreenHeight*0.54;
    backDownWidth = centerScreenWidth *0.24;
    backDownHeight = centerScreenHeight*0.11;
    if (height == 480) {
        backDownX += 10;
    }
    
    zerogX = centerScreenWidth*0.35;
    zerogY = centerScreenHeight*0.38;
    zerogWidth = centerScreenWidth *0.247;
    zerogHeight = centerScreenHeight*0.13;
    if (height == 480) {
        zerogX += 10;
    }
    
    memoryX = centerScreenWidth*0.355;
    memoryY = centerScreenHeight*0.51;
    memoryWidth = centerScreenWidth *0.245;
    memoryHeight = centerScreenHeight*0.11;
    if (height == 480) {
        memoryX += 10;
    }
    
    footUpX = centerScreenWidth*0.6;
    footUpY = centerScreenHeight*0.345;
    footUpWidth = centerScreenWidth *0.24;
    footUpHeight = centerScreenHeight*0.11;
    if (height == 480) {
        footUpX += 10;
    }
    
    footDownX = centerScreenWidth*0.6;
    footDownY = centerScreenHeight*0.54;
    footDownWidth = centerScreenWidth *0.24;
    footDownHeight = centerScreenHeight*0.11;
    if (height == 480) {
        footDownX += 10;
    }
    
    btnMinusX = centerScreenWidth*0.355;
    btnMinusY = centerScreenHeight*0.68;
    btnMinusWidth = centerScreenWidth *0.245;
    btnMinusHeight = centerScreenHeight*0.13;
    if (height == 480) {
        btnMinusX += 10;
    }
    
    headUpX = centerScreenWidth*0.11;
    headUpY = centerScreenHeight*0.62;
    headUpWidth = centerScreenWidth *0.24;
    headUpHeight = centerScreenHeight*0.13;
    if (height == 480) {
        headUpX += 10;
    }
    
    headDownX = centerScreenWidth*0.11;
    headDownY = centerScreenHeight*0.74;
    headDownWidth = centerScreenWidth *0.24;
    headDownHeight = centerScreenHeight*0.14;
    if (height == 480) {
        headDownX += 10;
    }
    
    waistUpX = centerScreenWidth*0.605;
    waistUpY = centerScreenHeight*0.62;
    waistUpWidth = centerScreenWidth *0.24;
    waistUpHeight = centerScreenHeight*0.13;
    if (height == 480) {
        waistUpX += 10;
    }
    
    waistDownX = centerScreenWidth*0.605;
    waistDownY = centerScreenHeight*0.74;
    waistDownWidth = centerScreenWidth *0.24;
    waistDownHeight = centerScreenHeight*0.14;
    if (height == 480) {
        waistDownX += 10;
    }
    
    
    btnLX = centerScreenWidth * 0.88;
    btnLY = centerScreenHeight * 0.20;
    btnLWidth = centerScreenWidth*0.097;
    btnLHeight = centerScreenHeight*0.141;
    if (height == 480) {
        btnLX += 10;
    }
    
    btnCenterLX = centerScreenWidth * 0.88;
    btnCenterLY = centerScreenHeight * 0.35;
    btnCenterLWidth = centerScreenWidth*0.097;
    btnCenterLHeight = centerScreenHeight*0.141;
    if (height == 480) {
        btnCenterLX += 10;
    }
    
    btnRX = centerScreenWidth * 0.88;
    btnRY = centerScreenHeight * 0.5;
    btnRWidth = centerScreenWidth*0.097;
    btnRHeight = centerScreenHeight*0.141;
    if (height == 480) {
        btnRX += 10;
    }
    
}


+ (void) initPad
{
    
    centerScreenHeight = [UIScreen mainScreen].bounds.size.height-64-49;
    centerScreenWidth = [UIScreen mainScreen].bounds.size.width;
    
    Blackheight = centerScreenHeight*0.8;
    Blackwidth = centerScreenWidth*0.848;
    
    leftMargin = [UIScreen mainScreen].bounds.size.width * 0.05;
    rightMargin = [UIScreen mainScreen].bounds.size.width * 0.1;
    //背景图片坐标与宽高
    imgBgX =  centerScreenWidth * 0.106;
    imgBgY =  centerScreenHeight * 0.06;
    imgBgWidth = centerScreenWidth * 0.74;
    imgBgHeight = centerScreenHeight * 0.765;
    
    
    //右边的LIR 背景
    LIRBgX = centerScreenWidth * 0.875;
    LIRBgY = centerScreenHeight * 0.2;
    LIRBgWidth = centerScreenWidth * 0.1;
    LIRBgHeight = centerScreenHeight* 0.44;
    
    
    //右边的LIR 文字
    LIRX = centerScreenWidth * 0.91;
    LIRY = centerScreenHeight * 0.25;
    LIRWidth = centerScreenWidth * 0.04;
    LIRHeight = centerScreenHeight* 0.34;
    
    
    //按钮位置
    //10 灯
    LightTenX = centerScreenWidth*0.445;
    LightTenY  = centerScreenHeight*0.02;
    LightTenWidth  = centerScreenWidth*0.05;
    LightTenHeight = centerScreenHeight * 0.009;
    
    LightTenLabelX = centerScreenWidth*0.51;
    LightTenLabelY  =centerScreenHeight*0.012;
    LightTenLabelWidth = centerScreenWidth *0.05;
    LightTenLabelHeight = centerScreenWidth*0.032;
    
    
    //20 灯
    LightTwentyX = centerScreenWidth*0.445;
    LightTwentyY =centerScreenHeight*0.05;
    LightTwentyWidth =centerScreenWidth*0.05;
    LightTwentyHeight =centerScreenHeight * 0.009;
    
    LightTwentyLabelX =centerScreenWidth*0.51;
    LightTwentyLabelY =centerScreenHeight*0.042;
    LightTwentyLabelWidth =centerScreenWidth *0.05;
    LightTwentyLabelHeight =centerScreenWidth*0.032;
    
    //30灯
    LightThirtyX = centerScreenWidth*0.445;
    LightThirtyY =centerScreenHeight*0.08;
    LightThirtyWidth =centerScreenWidth*0.05;
    LightThirtyHeight =centerScreenHeight * 0.009;
    
    LightThirtyLabelX =centerScreenWidth*0.51;
    LightThirtyLabelY =centerScreenHeight*0.072;
    LightThirtyLabelWidth =centerScreenWidth *0.05;
    LightThirtyLabelHeight =centerScreenWidth*0.032;
    
    
    
    headShakeX = centerScreenWidth*0.11;
    headShakeY = centerScreenHeight*0.06;
    headShakeWidth = centerScreenWidth *0.25;
    headShakeHeight = centerScreenHeight*0.171;
    
    switchModeX = centerScreenWidth*0.361;
    switchModeY = centerScreenHeight*0.12;
    switchModeWidth = centerScreenWidth *0.23;
    swithcModeHeight = centerScreenHeight*0.11;
    
    footShakeX = centerScreenWidth*0.6;
    footShakeY = centerScreenHeight*0.067;
    footShakeWidth = centerScreenWidth *0.25;
    footShakeHeight = centerScreenHeight*0.171;
    
    
    btnlX = centerScreenWidth*0.11;
    btnlY = centerScreenHeight*0.2;
    btnlWidth = centerScreenWidth *0.24;
    btnlHeight = centerScreenHeight *0.176;
    
    btnllX = centerScreenWidth*0.361;
    btnllY = centerScreenHeight*0.23;
    btnllWidth = centerScreenWidth *0.23;
    btnllHeight = centerScreenHeight*0.11;
    
    btnlllX = centerScreenWidth*0.605;
    btnlllY = centerScreenHeight*0.20;
    btnlllWidth = centerScreenWidth *0.23;
    btnlllHeight = centerScreenHeight*0.175;
    
    backUpX = centerScreenWidth*0.11;
    backUpY = centerScreenHeight*0.345;
    backUpWidth = centerScreenWidth *0.24;
    backUpHeight = centerScreenHeight*0.11;
    
    backDownX = centerScreenWidth*0.11;
    backDownY = centerScreenHeight*0.54;
    backDownWidth = centerScreenWidth *0.24;
    backDownHeight = centerScreenHeight*0.11;
    
    zerogX = centerScreenWidth*0.35;
    zerogY = centerScreenHeight*0.38;
    zerogWidth = centerScreenWidth *0.247;
    zerogHeight = centerScreenHeight*0.13;
    
    memoryX = centerScreenWidth*0.355;
    memoryY = centerScreenHeight*0.51;
    memoryWidth = centerScreenWidth *0.245;
    memoryHeight = centerScreenHeight*0.11;
    
    
    footUpX = centerScreenWidth*0.6;
    footUpY = centerScreenHeight*0.345;
    footUpWidth = centerScreenWidth *0.24;
    footUpHeight = centerScreenHeight*0.11;
    
    footDownX = centerScreenWidth*0.6;
    footDownY = centerScreenHeight*0.54;
    footDownWidth = centerScreenWidth *0.24;
    footDownHeight = centerScreenHeight*0.11;
    
    
    btnMinusX = centerScreenWidth*0.355;
    btnMinusY = centerScreenHeight*0.68;
    btnMinusWidth = centerScreenWidth *0.245;
    btnMinusHeight = centerScreenHeight*0.13;
    
    
    headUpX = centerScreenWidth*0.11;
    headUpY = centerScreenHeight*0.62;
    headUpWidth = centerScreenWidth *0.24;
    headUpHeight = centerScreenHeight*0.13;
    
    headDownX = centerScreenWidth*0.11;
    headDownY = centerScreenHeight*0.74;
    headDownWidth = centerScreenWidth *0.24;
    headDownHeight = centerScreenHeight*0.14;
    
    
    waistUpX = centerScreenWidth*0.605;
    waistUpY = centerScreenHeight*0.62;
    waistUpWidth = centerScreenWidth *0.24;
    waistUpHeight = centerScreenHeight*0.13;
    
    waistDownX = centerScreenWidth*0.605;
    waistDownY = centerScreenHeight*0.74;
    waistDownWidth = centerScreenWidth *0.24;
    waistDownHeight = centerScreenHeight*0.14;
    
    
    
    btnLX = centerScreenWidth * 0.88;
    btnLY = centerScreenHeight * 0.20;
    btnLWidth = centerScreenWidth*0.097;
    btnLHeight = centerScreenHeight*0.141;
    
    btnCenterLX = centerScreenWidth * 0.88;
    btnCenterLY = centerScreenHeight * 0.35;
    btnCenterLWidth = centerScreenWidth*0.097;
    btnCenterLHeight = centerScreenHeight*0.141;
    
    btnRX = centerScreenWidth * 0.88;
    btnRY = centerScreenHeight * 0.5;
    btnRWidth = centerScreenWidth*0.097;
    btnRHeight = centerScreenHeight*0.141;
    
    
}

@end
