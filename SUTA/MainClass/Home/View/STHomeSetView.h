//
//  STHomeSetView.h
//  SUTA
//
//  Created by 张海军 on 2017/3/22.
//  Copyright © 2017年 ZLB. All rights reserved.
//  设置 SUTA 型号

#import <UIKit/UIKit.h>

@interface STHomeSetView : UIView

/// 点击设置的回调
@property (nonatomic, copy) void(^setButtonDidClick)();

@end
