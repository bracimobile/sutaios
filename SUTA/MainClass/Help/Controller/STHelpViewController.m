//
//  STHelpViewController.m
//  SUTA
//
//  Created by 张海军 on 17/3/21.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import "STHelpViewController.h"
#import "STHomeTopView.h"
#import "STBaseModel.h"
#import "STContactView.h"
#import "STFAQController.h"

CGFloat HELPCELLH = 0.0;

@interface STHelpViewController () <UITableViewDelegate,UITableViewDataSource>
/// topView
@property (nonatomic, strong) STHomeTopView *homeTopView;
/// infoView
@property (nonatomic, strong) STContactView *infoView;
/// tableView
@property (nonatomic, strong) UITableView *tableView;
/// logo
@property (nonatomic, strong) UIImageView *logoImageView;
/// 模型数组
@property (nonatomic, strong) NSMutableArray *modelArray;

@end

@implementation STHelpViewController
#pragma mark - life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupInit];
    [self loadData];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    self.homeTopView.frame = CGRectMake(0, 0, SCREEN_WIDTH, START_Y);
    CGFloat tableH = self.modelArray.count * HELPCELLH;
    self.tableView.frame = CGRectMake(0, START_Y, SCREEN_WIDTH, tableH);
    CGFloat imageW = SCREEN_WIDTH * 0.48;
    CGFloat imageX = (SCREEN_WIDTH - imageW) * 0.5;
    CGFloat margin = SCREEN_HEIGHT * 0.024;
    self.logoImageView.frame = CGRectMake(imageX, CGRectGetMaxY(self.tableView.frame) + margin, imageW, SCREEN_HEIGHT * 0.20);
    CGFloat infox = (self.view.hj_width - 260) * 0.5;
    self.infoView.frame = CGRectMake(infox, CGRectGetMaxY(self.logoImageView.frame) + 2 * margin, 260, 100);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 初始化
- (void)setupInit
{
    HELPCELLH = SCREEN_HEIGHT * 0.065;
    
    // 移除 NAv
    [self.NavBackView removeFromSuperview];
    [self.NavLeftBtn removeFromSuperview];
    [self.NavRightBtn removeFromSuperview];
    
    // 添加顶部
    self.homeTopView = [STHomeTopView homeTopView];
    self.homeTopView.upLabel.text = @"Help";
    self.homeTopView.modelLabel.hidden = YES;
    [self.view addSubview:self.homeTopView];
    
    // tableview
    self.tableView = [[UITableView alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.scrollEnabled = NO;
    self.tableView.rowHeight = HELPCELLH;
    self.tableView.separatorInset = UIEdgeInsetsZero;
    if ([self.tableView respondsToSelector:@selector(layoutMargins)]) {
        self.tableView.layoutMargins = UIEdgeInsetsZero;
    }
    [self.view addSubview:self.tableView];
    
    // logo
    self.logoImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo"]];
    self.logoImageView.contentMode = UIViewContentModeScaleAspectFit;
    //self.logoImageView.backgroundColor = [UIColor redColor];
    [self.view addSubview:self.logoImageView];
    
    // info
    self.infoView = [STContactView contactView];
    [self.view addSubview:self.infoView];
}

- (void)loadData
{
    WS(weakSelf);
    STBaseModel *RemoteM = [[STBaseModel alloc] init];
    RemoteM.titleName = @"Remote Guide";
    RemoteM.cellClickBlock = ^{
        weakSelf.tabBarController.selectedIndex = 0;
    };
    [self.modelArray addObject:RemoteM];
    
    STBaseModel *FAQM = [[STBaseModel alloc] init];
    FAQM.titleName = @"FAQs";
    FAQM.cellClickBlock = ^{
        STFAQController *FAQVC = [[STFAQController alloc] init];
        [weakSelf.navigationController pushViewController:FAQVC animated:YES];
    };
    [self.modelArray addObject:FAQM];
    
    STBaseModel *TSM = [[STBaseModel alloc] init];
    TSM.titleName = @"Trouble Shooting";
    TSM.cellClickBlock = ^{
        [ToastManager showToastWithString:@"Development, please look forward to"];
    };
    [self.modelArray addObject:TSM];
    
    STBaseModel *SupportM = [[STBaseModel alloc] init];
    SupportM.cellClickBlock = ^{
        [ToastManager showToastWithString:@"Development, please look forward to"];
    };
    SupportM.titleName = @"Support";
    [self.modelArray addObject:SupportM];
}

#pragma mark - table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.modelArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *ID = @"helpcell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.separatorInset = UIEdgeInsetsZero;
        if ([cell respondsToSelector:@selector(layoutMargins)]) {
            cell.layoutMargins = UIEdgeInsetsZero;
        }
    }
    STBaseModel *model = self.modelArray[indexPath.row];
    cell.textLabel.text = model.titleName;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    STBaseModel *model = self.modelArray[indexPath.row];
    if (model.cellClickBlock) {
        model.cellClickBlock();
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark - lazy
- (NSMutableArray *)modelArray
{
    if (!_modelArray) {
        _modelArray = [NSMutableArray array];
    }
    return _modelArray;
}

@end
