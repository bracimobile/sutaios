//
//  DBManager.m
//  DatabasePackge
//
//  Created by houzhifei on 16/2/25.
//  Copyright © 2016年 tentinet. All rights reserved.
//

#import "DBManager.h"
#import <objc/runtime.h>

@interface DBManager()

@property (nonatomic, strong) FMDatabaseQueue *dbQueue;

@end

@implementation DBManager

//公用数据名字
static NSString *const commenDbName = @"commenDB";

- (NSString *)dbPathWithDbName:(NSString *)dbName
{
    NSString *docsdir = [NSSearchPathForDirectoriesInDomains( NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSFileManager *filemanage = [NSFileManager defaultManager];
    docsdir = [docsdir stringByAppendingPathComponent:@"Database"];
    BOOL isDir;
    BOOL exit =[filemanage fileExistsAtPath:docsdir isDirectory:&isDir];
    if (!exit || !isDir) {
        [filemanage createDirectoryAtPath:docsdir withIntermediateDirectories:YES attributes:nil error:nil];
    }
    dbName = [NSString stringWithFormat:@"%@.sqlite", dbName];
    NSString *dbpath = [docsdir stringByAppendingPathComponent:dbName];
    return dbpath;
}

- (instancetype)initWithDatabaseName:(NSString *)dbName {
    if (self = [super init]) {
        _dbQueue = [[FMDatabaseQueue alloc] initWithPath:[self dbPathWithDbName:dbName]];
        [_dbQueue inDatabase:^(FMDatabase *db) {
            [db open];
            //为数据库设置缓存，提高查询效率
            [db setShouldCacheStatements:YES];
            [db close];
        }];
        
        [self checkVersionWords];
    }
    return self;
}

+ (instancetype)sharedCommentDataManager {
    static DBManager *commenDataManager = nil;
    @synchronized(self) {
        if (commenDataManager == nil) {
            commenDataManager = [[DBManager alloc] initWithDatabaseName:commenDbName];
        }
    }
    return commenDataManager;
}

+ (instancetype)sharedUserDataManager {
    static DBManager *userDataManager = nil;
    static NSString *lastUser = nil;
    @synchronized(self) {
# pragma mark - 用当前用户id 为数据库名字
        NSString *userId = @"SUTA"; //[NSString stringWithFormat:@"%@",USER_USERID];
        if (userDataManager == nil || (userId != nil && ![userId isEqualToString:lastUser])) {
            lastUser = userId;
            userDataManager = [[DBManager alloc] initWithDatabaseName:lastUser];
        }
    }
    
    return userDataManager;
}

#pragma mark - 核对版本字段，缺少则添加，多则不删，更新数据库
- (void)checkVersionWords {
    [self.dbQueue inDatabase:^(FMDatabase *db) {
        //判断数据库是否已经打开，如果没有打开，提示失败
        if (![db open]) {
            DBLog(@"数据库打开失败");
            return;
        }
        
        //查询所有表名
        FMResultSet *result = [db executeQuery:@"select name from sqlite_master where type = 'table'"];
        while ([result next]) {
            NSString *tableName = [result stringForColumn:@"name"];
            
            //根据表名查询所有字段
            FMResultSet *nameResult =
            [db executeQuery:[NSString stringWithFormat:@"select *from %@", tableName]];
            if ([nameResult next]) {
                
                //数据库中的表的字段
                NSArray *dbArray = [[nameResult resultDictionary] allKeys];
                
                //模型中的属性和名称
                NSDictionary *modelDict = [self getPropertys:NSClassFromString(tableName)];
                NSMutableArray *proNames = modelDict[@"name"];
                
                //如果字段不存在，则添加
                for (NSString *popertyName in proNames) {
                    if (![dbArray containsObject:popertyName]) {
                        NSUInteger index = [proNames indexOfObject:popertyName];
                        NSString *dbType = [[modelDict objectForKey:@"type"] objectAtIndex:index];
                        NSString *dbInsert = [NSString stringWithFormat:@"alter table %@ add column '%@' %@", tableName, popertyName, dbType];
                        FMResultSet *insert = [db executeQuery:dbInsert];
                        if (insert) {
                            [insert next]; //不执行next就不成功。执行next，返回NO。
                            DBLog(@"%@表添加%@成功", tableName, popertyName);
                        }
                    }
                }
            }
        }
        //关闭db
        [db close];
    }];
}

/**************************分割线*****************************/
#pragma mark - 创建表
- (BOOL)createTable:(Class)className WithDB:(FMDatabase *)myDb
{
    BOOL isSuccess = YES;
    NSString *tableName = NSStringFromClass(className);
    //判断数据库中是否已经存在这个表，如果不存在则创建该表
    if (![myDb tableExists:tableName]) {
        // 创建表语句的第一部分
        NSString *creatTableStrOne = [NSString stringWithFormat:@"create table %@(id integer primary key", tableName];
        
        //模型中的属性和名称
        NSDictionary *modelDict = [self getPropertys:NSClassFromString(tableName)];
        NSMutableArray *proNames = modelDict[@"name"];
        
        // 创建表语句的第二部分
        NSMutableString *creatTableStrTwo = [NSMutableString string];
        for (NSString *popertyName in proNames) {
            NSUInteger index = [proNames indexOfObject:popertyName];
            NSString *dbType = [[modelDict objectForKey:@"type"] objectAtIndex:index];
            
            [creatTableStrTwo appendFormat:@",%@ %@", popertyName, dbType];
        }
        // 创建表的整个语句
        NSString *creatTableStr =
        [NSString stringWithFormat:@"%@%@)", creatTableStrOne, creatTableStrTwo];
        
        if ([myDb executeUpdate:creatTableStr]) {
            isSuccess = YES;
            DBLog(@"创建%@表成功", tableName);
        } else {
            isSuccess = NO;
            DBLog(@"创建%@表失败", tableName);
        }
    }
    
    return isSuccess;
}


#pragma mark - 数据库插入或更新实体
/**
 *  数据库插入或更新实体
 *
 *  @param model 实体
 */
- (BOOL)insertAndUpdateModelToDatabase:(id)model
{
    //模型是否为空
    if (!model) return NO;
    
    __block BOOL isSuccess = YES;
    [_dbQueue inDatabase:^(FMDatabase *db) {
        if (![db open]) {
            DBLog(@"数据库打开失败");
            isSuccess = NO;
        } else {
            // 判断是否存在对应的userModel表
            if(![db tableExists:NSStringFromClass([model class])] && ![self createTable:[model class] WithDB:db]) {
                isSuccess = NO;
            } else {
                NSArray *properties = [self getPropertys:[model class]][@"name"];
                isSuccess = [self insertOrUpdateWithModel:model withDB:db withProperties:properties];
            }
        }
        
        [db close];
    }];
    return isSuccess;
}

/**
 *  插入或更新多条记录
 *
 *  @param array 模型数组列表
 */
- (void)insertOrUpdateMultipleData:(NSArray *)array complete:(void (^)(BOOL))complete{
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        __block NSArray *datas = array;
        __block BOOL isSuccess = YES;
        
        [_dbQueue inDatabase:^(FMDatabase *db) {
            if (![db open]) {
                DBLog(@"数据库打开失败");
                isSuccess = NO;
            } else {
                id modelTarget = [datas firstObject];
                NSString *tableName = NSStringFromClass([modelTarget class]);
                
                if (![db tableExists:tableName] && ![self createTable:[modelTarget class] WithDB:db]) {
                    DBLog(@"创建表失败");
                    isSuccess = NO;
                } else {
                    NSArray *properties = [self getPropertys:[modelTarget class]][@"name"];
                    
                    [db beginTransaction];
                    for (id model in datas) {
                        [self insertOrUpdateWithModel:model withDB:db withProperties:properties];
                    }
                    
                    if (![db commit]) {
                        [db rollback];
                        isSuccess = NO;
                    }
                }
            }
            
            [db close];
            
            //主线程通知事件
            dispatch_async(dispatch_get_main_queue(), ^{
                if (complete) {
                    complete(isSuccess);
                }
            });
        }];
    });
}

- (BOOL)insertOrUpdateWithModel:(id)model withDB:(FMDatabase *)myDb withProperties:(NSArray *)properties {
    
    BOOL isSuccess = YES;
    NSString *tableName = NSStringFromClass([model class]);
    
//    // 判断是否存在对应的userModel表
//    if(![myDb tableExists:tableName] && ![self createTable:NSClassFromString(tableName) WithDB:myDb]) {
//        return NO;
//    }
    
    //模型的第一个属性值用于区分唯一，例如userid
    NSString *selectStr = [NSString stringWithFormat:@"select *from %@ where %@ = ?", tableName,properties[0]];
    // 获取对应属性的对应值
    FMResultSet * resultSet = [myDb executeQuery:selectStr,[model valueForKey:properties[0]]];
    if ([resultSet next]) {
        // 如果有[resultSet next]的话，本次做更新操作
        
        // 更新语句第一部分
        NSString *updateStrOne = [NSString stringWithFormat:@"update %@ set ",tableName];
        // 更新语句第二部分
        NSMutableString *updateStrTwo = [NSMutableString string];
        for (int i = 0; i < properties.count; i++) {
            
            [updateStrTwo appendFormat:@"%@ = ?",[properties objectAtIndex:i]];
            
            if (i != properties.count -1) {
                [updateStrTwo appendFormat:@","];
            }
        }
        // 更新语句第二部分
        NSString *updateStrThree = [NSString stringWithFormat:@"where %@ = '%@'",[properties objectAtIndex:0], [model valueForKey:[properties objectAtIndex:0]]];
        
        // 更新总语句
        NSString *updateStr = [NSString stringWithFormat:@"%@%@%@",updateStrOne,updateStrTwo,updateStrThree];
        
        // 属性值数组
        NSMutableArray *propertyValue = [NSMutableArray array];
        for (NSString *property in properties) {
            if (![model valueForKey:property]) {
                [model setValue:@"" forKey:property];
            }
            id object = [model valueForKey:property];
            [propertyValue addObject:object ? object : @""];
        }
        
        // 调用更新
        if([myDb executeUpdate:updateStr withArgumentsInArray:propertyValue]) {
            isSuccess = YES;
            DBLog(@"调用更新成功");
        } else {
            isSuccess = NO;
            DBLog(@"调用更新失败");
        }
    } else {
//        double start =  CFAbsoluteTimeGetCurrent();
        // 本次做插入操作
        
        // 插入语句第一部分
        NSString *insertStrOne = [NSString stringWithFormat:@"insert into %@ (", tableName];
        // 插入语句第二部分
        NSMutableString *insertStrTwo =[NSMutableString string];
        for (int i =0; i < properties.count; i++) {
            [insertStrTwo appendFormat:@"%@",[properties objectAtIndex:i]];
            if (i != properties.count -1) {
                [insertStrTwo appendFormat:@","];
            }
        }
        // 插入语句第三部分
        NSString *insertStrThree =[NSString stringWithFormat:@") values ("];
        // 插入语句第四部分
        NSMutableString *insertStrFour =[NSMutableString string];
        for (int i =0; i < properties.count; i++) {
            [insertStrFour appendFormat:@"?"];
            if (i != properties.count -1) {
                [insertStrFour appendFormat:@","];
            }
        }
        // 插入整个语句
        NSString *insertStr = [NSString stringWithFormat:@"%@%@%@%@)",insertStrOne,insertStrTwo,insertStrThree,insertStrFour];
        
        
//        DBLog(@"insertStr = %@",insertStr);
        // 属性值数组
        NSMutableArray *propertyValue = [NSMutableArray array];
        for (NSString *property in properties) {
            // 每一次遍历属性数组，通过valueForKey获取属性的值
            if (![model valueForKey:property]) {
                [model setValue:@"" forKey:property];
            }
            
            id object = [model valueForKey:property];
            [propertyValue addObject:object ? object : @""];
        }
        // 调用插入
        if([myDb executeUpdate:insertStr withArgumentsInArray:propertyValue]) {
            isSuccess = YES;
            DBLog(@"插入数据成功");
        } else {
            isSuccess = NO;
            DBLog(@"插入数据失败");
        }
        
//        static int i = 0 ;
//        DBLog(@"%zd,inset耗时%f",++i,CFAbsoluteTimeGetCurrent() - start);
    }
    return isSuccess;
}

#pragma mark - 数据库删除对应实体
- (BOOL)deleteModelInDatabase:(id)model {
    __block BOOL isSuccess = YES;
    __block NSString *tableName = NSStringFromClass([model class]);
    __block NSArray *properties = [self getPropertys:[model class]][@"name"];
    
    [_dbQueue inDatabase:^(FMDatabase *db) {
        if (![db open]) {
            DBLog(@"数据库打开失败");
            isSuccess = NO;
            return;
        }
        
        // 判断是否有该表
        if (![db tableExists:tableName]) {
            isSuccess = NO;
            DBLog(@"数据库中没有%@表", tableName);
            return;
        }
        
        // 以前的数据库语句 delete from tableName where user_id = ?;
        NSString *deleteStr = [NSString stringWithFormat:@"delete from %@ where %@ = ?", tableName,
        [properties objectAtIndex:0]];
        
        if ([db executeUpdate:deleteStr, [model valueForKey:[properties objectAtIndex:0]]]) {
            isSuccess = YES;
            DBLog(@"删除数据成功");
        } else {
            isSuccess = NO;
            DBLog(@"删除数据失败");
        }
        
        [db close];
    }];
    return isSuccess;
}

- (BOOL)deleteAllDatasInTableName:(NSString *)tableName {
    __block BOOL isSuccess = YES;
    __weak typeof(self) weakSelf = self;
    [_dbQueue inDatabase:^(FMDatabase *db) {
        NSString *deletString = [NSString stringWithFormat:@"delete from %@", tableName];
        isSuccess = [weakSelf deleteWithSqlString:deletString withDB:db withTableName:tableName];
    }];
    
    return isSuccess;
}

- (BOOL)deleteFromTableName:(NSString *)tableName WithRule:(NSString *)rule {
    __block BOOL isSuccess = YES;
    __weak typeof(self) weakSelf = self;
    [_dbQueue inDatabase:^(FMDatabase *db) {
        isSuccess = [weakSelf deleteWithSqlString:rule withDB:db withTableName:tableName];
    }];
    
    return isSuccess;
}

- (BOOL)deleteWithSqlString:(NSString *)sqlString withDB:(FMDatabase *)db withTableName:(NSString *)tableName {
    BOOL isSuccess = YES;
    
    if (![db open]) {
        DBLog(@"数据库打开失败");
        return NO;
    }
    
    // 判断是否有该表
    if (![db tableExists:tableName]) {
        DBLog(@"数据库中没有%@表", tableName);
        return NO;
    }
    
    if ([db executeUpdate:sqlString]) {
        isSuccess = YES;
        DBLog(@"删除数据成功");
    } else {
        isSuccess = NO;
        DBLog(@"删除数据失败");
    }
    
    [db close];
    
    return isSuccess;
}

#pragma mark - 查询
/**
 *  只查询表中数据条数
 */
- (NSInteger)inquireDataCountInDatabase:(NSString *)tableName {
    
    __block NSInteger count = 0;
    [_dbQueue inDatabase:^(FMDatabase *db) {
        if (![db open]) {
            DBLog(@"数据库打开失败");
            return;
        }
        
        // 判断是否有该表,并创建
        if (![db tableExists:tableName] && ![self createTable:NSClassFromString(tableName) WithDB:db]) {
            DBLog(@"创建%@表失败", tableName);
            return;
        }
        
        count = [db intForQuery:[NSString stringWithFormat:@"select count(*) from %@",tableName]];
        [db close];
    }];
    
    return count;
}

- (NSInteger)inquireIMDataCountInDatabase:(NSString *)tableName IMID:(NSString *)imID{
    __block NSInteger count = 0;
    [_dbQueue inDatabase:^(FMDatabase *db) {
        if (![db open]) {
            DBLog(@"数据库打开失败");
            return;
        }
        
        // 判断是否有该表,并创建
        if (![db tableExists:tableName] && ![self createTable:NSClassFromString(tableName) WithDB:db]) {
            DBLog(@"创建%@表失败", tableName);
            return;
        }
        
        count = [db intForQuery:[NSString stringWithFormat:@"select count(*) from %@ WHERE g_groupIM = \'%@\'",tableName,imID]];
        [db close];
    }];
    
    return count;
}

/**
 *  数据库查询所有实体
 */
- (NSMutableArray *)takeOutInDataBase:(Class)curClass {
    return [self takeOutDataWithModelClass:curClass WithRule:nil];
}

- (NSMutableArray *)takeOutInDataBase:(Class)curClass withRule:(NSString *)rule {
    return [self takeOutDataWithModelClass:curClass WithRule:rule];
}

- (NSMutableArray *)takeOutDataWithModelClass:(Class)curClass WithRule:(NSString *)rule {
    __block NSMutableArray *modelArray = [NSMutableArray array];
    __block NSArray *properties = [self getPropertys:curClass][@"name"];
    
    [_dbQueue inDatabase:^(FMDatabase *db) {
        if (![db open]) {
            DBLog(@"数据库打开失败");
            return;
        }
        
        // 判断是否有该表,并创建
        if (![db tableExists:NSStringFromClass(curClass)] && ![self createTable:curClass WithDB:db]) {
            DBLog(@"创建%@表失败", NSStringFromClass(curClass));
            return;
        }
        
        // 查询整个表的语句
        NSString * selectStr = nil;
        if (rule.length) {
            selectStr = [NSString stringWithFormat:@"select * from %@ %@",NSStringFromClass(curClass), rule];
        } else {
            selectStr = [NSString stringWithFormat:@"select * from %@",NSStringFromClass(curClass)];
        }
        
        FMResultSet *resultSet = [db executeQuery:selectStr];
        while([resultSet next]) {
            // 使用表名作为类名创建对应的类的对象
            id model = [[curClass alloc] init];
            for (int i = 0; i < properties.count; i++) {
                // 值是从我们的数据表的Column字段取出来
                [model setValue:[resultSet objectForColumnName:[properties objectAtIndex:i]] forKey:[properties objectAtIndex:i]];
            }
            [modelArray addObject:model];
        }
        
        [db close];
    }];
    
    return modelArray;
}

- (BOOL)editDatabaseBySQL:(NSString *)sql {
    __block BOOL isSuccess = YES;
    [_dbQueue inDatabase:^(FMDatabase *db) {
        if (![db open]) {
            DBLog(@"数据库打开失败");
            isSuccess = NO;
            return;
        }
        
        isSuccess = [db executeUpdate:sql];
        
        [db close];
    }];
    return isSuccess;
}

- (FMResultSet *)queryWithsqlStr:(NSString *)sqlStr {
    __block FMResultSet *resultSet = nil;
    
    [_dbQueue inDatabase:^(FMDatabase *db) {
        if (![db open]) {
            DBLog(@"数据库打开失败");
            return;
        }
        resultSet = [db executeQuery:sqlStr];
        [db close];
    }];
    
    return resultSet;
}

#pragma mark - base method
- (NSDictionary *)getPropertys:(Class)className
{
    NSMutableArray *proNames = [NSMutableArray array];
    NSMutableArray *proTypes = [NSMutableArray array];
    unsigned int outCount, i;
    objc_property_t *properties = class_copyPropertyList(className, &outCount);
    for (i = 0; i < outCount; i++) {
        objc_property_t property = properties[i];
        //获取属性名
        NSString *propertyName = [NSString stringWithCString:property_getName(property) encoding:NSUTF8StringEncoding];
        [proNames addObject:propertyName];
        //获取属性类型等参数
        NSString *propertyType = [NSString stringWithCString: property_getAttributes(property) encoding:NSUTF8StringEncoding];
        /*
         c char         C unsigned char
         i int          I unsigned int
         l long         L unsigned long
         s short        S unsigned short
         d double       D unsigned double
         f float        F unsigned float
         q long long    Q unsigned long long
         B BOOL
         @ 对象类型 //指针 对象类型 如NSString 是@“NSString”
         
         
         64位下long 和long long 都是Tq
         SQLite 默认支持五种数据类型TEXT、INTEGER、REAL、BLOB、NULL
         */
        if ([propertyType hasPrefix:@"T@"]) {
            if ([propertyType rangeOfString:@"T@\"NSData\""].location != NSNotFound) {
                [proTypes addObject:SQLBLOB];
            } else {
                [proTypes addObject:SQLTEXT];
            }
        } else if ([propertyType hasPrefix:@"Ti"] || [propertyType hasPrefix:@"TI"] || [propertyType hasPrefix:@"Ts"] || [propertyType hasPrefix:@"TS"] || [propertyType hasPrefix:@"TB"]||[propertyType hasPrefix:@"Tq"] || [propertyType hasPrefix:@"TQ"] || [propertyType hasPrefix:@"Tl"] || [propertyType hasPrefix:@"TL"]) {
            [proTypes addObject:SQLINTEGER];
        } else if ([propertyType hasPrefix:@"Td"] || [propertyType hasPrefix:@"TD"] || [propertyType hasPrefix:@"Tf"] || [propertyType hasPrefix:@"TF"]){
            [proTypes addObject:SQLREAL];
        } else {
            [proTypes addObject:SQLNULL];
        }
    }
    free(properties);
    
    return [NSDictionary dictionaryWithObjectsAndKeys:proNames,@"name",proTypes,@"type",nil];
}


@end
