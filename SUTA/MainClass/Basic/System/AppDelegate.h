//
//  AppDelegate.h
//  SUTA
//
//  Created by 张海军 on 17/3/21.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic,assign) BOOL isEngineWorking;
@property (nonatomic,assign) BOOL kdetectionMode;


@end

