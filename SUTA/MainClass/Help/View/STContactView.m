//
//  STContactView.m
//  SUTA
//
//  Created by 张海军 on 2017/3/22.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import "STContactView.h"

@interface STContactView ()
@property (weak, nonatomic) IBOutlet UILabel *upLabel;
@property (weak, nonatomic) IBOutlet UILabel *downLabel;
@property (weak, nonatomic) IBOutlet UIView *marginView;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;

@end

@implementation STContactView

+ (instancetype)contactView
{
    STContactView *view = [[[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:nil options:nil] lastObject];
    return view;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.layer.borderColor = [UIColor blackColor].CGColor;
    self.layer.borderWidth = 1;
    NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    self.versionLabel.text = [NSString stringWithFormat:@"version : %@",appVersion];
}


@end
