//
//  AppDelegate.m
//  SUTA
//
//  Created by 张海军 on 17/3/21.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import "AppDelegate.h"
#import "STTabBarController.h"
#import <AVFoundation/AVFoundation.h>
#import "bridgeClass.h"
#ifdef NSFoundationVersionNumber_iOS_9_x_Max
#import <UserNotifications/UserNotifications.h>
#endif
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

@interface AppDelegate ()<UNUserNotificationCenterDelegate, UIAlertViewDelegate>
///
@property (nonatomic, strong) AVAudioPlayer *audioPlayer;
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    
    STTabBarController *tabVC = [[STTabBarController alloc] init];
    
    self.window.rootViewController = tabVC;
    
    [self.window makeKeyAndVisible];
    //[application setStatusBarStyle:UIStatusBarStyleLightContent];
    // 初始化公共数据
    [STUserData getCommonDataInitFormLocation];
    [bridgeClass SET_APP_IS_BACKGROUND:false];
    UIApplication.sharedApplication.idleTimerDisabled = true;
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 10.0) {
        
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert completionHandler:^(BOOL granted, NSError * _Nullable error) {
            
        }];
    }else{
        /*
         UIUserNotificationTypeBadge
         UIUserNotificationTypeSound
         UIUserNotificationTypeAlert
         */
        UIUserNotificationSettings *setting = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:setting];
    }
    
//    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
//    NSArray *localNotifications = [UIApplication sharedApplication].scheduledLocalNotifications;
//    
//    for (UILocalNotification *notification in localNotifications) {
//        NSLog(@"fireDate = %@, userInfo = %@",notification.fireDate,notification.userInfo);
//    }
    
    NSString *isFirst = [[NSUserDefaults standardUserDefaults] valueForKey:@"isFirst_stua"];
    if (isFirst.length <=0 ) {
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
        [[NSUserDefaults standardUserDefaults] setValue:@"来过" forKey:@"isFirst_stua"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    [Fabric with:@[[Crashlytics class]]];
    return YES;
}


- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notificatio{
    
    NSDictionary *dic = notificatio.userInfo;
    /*
     notifi.alertBody = self.clockModel.labelStr;//提示信息弹出提示框
     
     notifi.alertAction = self.clockModel.sleteTime;  //提示框按钮
     */
  //  NSLog(@"didReceiveLocalNotification +++ dic = %@",dic);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"闹铃" message:nil  delegate:self cancelButtonTitle:@"关闭" otherButtonTitles:nil, nil];
    [alert show];
    
    NSURL *pathUrl = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"callRing" ofType:@".caf"]];
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:pathUrl error:nil];
    //self.audioPlayer.delegate = self;
    self.audioPlayer.volume = 1.0f;
    // 准备播放
    [self.audioPlayer prepareToPlay];
    [self.audioPlayer play];
}


- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"闹铃" message:nil  delegate:self cancelButtonTitle:@"关闭" otherButtonTitles:nil, nil];
    [alert show];
    
    
    NSURL *pathUrl = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"callRing" ofType:@".caf"]];
    self.audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:pathUrl error:nil];
    self.audioPlayer.volume = 1.0f;
    // 准备播放
    [self.audioPlayer prepareToPlay];
    [self.audioPlayer play];
    
    NSDictionary * userInfo = notification.request.content.userInfo;;
   
   // NSLog(@"willPresentNotification +++ dic = %@",userInfo);
}


- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void (^)())completionHandler
{
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"本地通知"  delegate:nil cancelButtonTitle:@"关闭" otherButtonTitles:nil, nil];
//    [alert show];
    
    completionHandler();
    NSDictionary * userInfo = response.notification.request.content.userInfo;;
    
//    NSLog(@"didReceiveNotificationResponse +++ dic = %@",userInfo);
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    [bridgeClass SET_APP_IS_BACKGROUND:true];
    NSLog(@"Sound detection is : %d",[bridgeClass get_g_bDetecting] == true? 1: 0);
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    [application setApplicationIconBadgeNumber:0];
    [bridgeClass SET_APP_IS_BACKGROUND:false];

}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self.audioPlayer stop];
}

@end
