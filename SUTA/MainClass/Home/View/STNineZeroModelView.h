//
//  STNineZeroModelView.h
//  SUTA
//
//  Created by 张海军 on 2017/3/23.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STZeroModelBaseView.h"

@interface STNineZeroModelView : STZeroModelBaseView

@property(strong,nonatomic) UIView  *blackBg;
@property(strong,nonatomic) UIImageView *bgImgView;
@property(assign) CGFloat imgHeight;

//底部黑色部分
@property(strong,nonatomic) UIImageView *bgImgBottomView;
@property(assign) CGFloat imgBottomHeight;


// 10 20 30 指示灯
@property(strong,nonatomic) UIView  *lightTen;
@property(strong,nonatomic) UIView  *lightTwenty;
@property(strong,nonatomic) UIView  *lightThirty;

@property(strong,nonatomic) UILabel  *lightTenLabel;
@property(strong,nonatomic) UILabel  *lightTwentyLabel;
@property(strong,nonatomic) UILabel  *lightThirtyLabel;


// 右边音量键背景图
@property (strong,nonatomic) UIImageView *ivLIRBg;

@property (strong,nonatomic) UIImageView *ivLIR;


@property (strong,nonatomic) UIButton *btnHeadShake;

@property (strong,nonatomic) UIButton *btnLight;

@property (strong,nonatomic) UIButton *btnFootShake;


@property (strong,nonatomic) UIButton *btnl;


@property (strong,nonatomic) UIButton *btnll;

@property (strong,nonatomic) UIButton *btnlll;


@property (strong,nonatomic) UIButton *btnBackUp;



@property (strong,nonatomic) UIButton *btnFlat;


@property (strong,nonatomic) UIButton *btnBackDown;


@property (strong,nonatomic) UIButton *btnZeroG;


@property (strong,nonatomic) UIButton *btnMemory;



@property (strong,nonatomic) UIButton *btnFootUp;


@property (strong,nonatomic) UIButton *btnFootDown;


@property (strong,nonatomic) UIButton *btnMinus;



@property (strong,nonatomic) UIButton *btnLeft;


@property (strong,nonatomic) UIButton *btnCenterL;


@property (strong,nonatomic) UIButton *btnRight;


@property (strong,nonatomic) UIButton *btnHeadUp;


@property (strong,nonatomic) UIButton *btnHeadDown;



@property (strong,nonatomic) UIButton *btnWaistUp;


@property (strong,nonatomic) UIButton *btnWaistDown;

/// 点击 tag == 2 按钮的次数  在 3秒内的
@property (nonatomic, assign) NSInteger seleteLongButtonCount;


/// 隐藏时需要做的操作
- (void)hiddeOperation;

//设置指示灯的颜色  1  2   0==3  4全部熄灭
- (void)setLightColorForIndex:(NSInteger) index;

// 连接设备  返回 第几个灯连接
- (void)perherCallBackLightNum:(NSInteger)index;

- (void)btnPressedDown:(UIButton *)button;

- (void)btnPressedCancel:(UIButton *)button;

- (void)sendMessage:(Byte[])dataMsg;

@end
