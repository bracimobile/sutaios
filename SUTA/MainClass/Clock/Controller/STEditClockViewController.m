//
//  STEditClockViewController.m
//  SUTA
//
//  Created by 张海军 on 2017/5/19.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import "STEditClockViewController.h"
#import "STClockRemindCell.h"
#import "STNewClockModel.h"
#import "UIFactory.h"
#import "STWeekSeleteViewController.h"
#import "STClockLabelViewController.h"

static CGFloat PICKHEIGHT = 216;

@interface STEditClockViewController ()<UITableViewDelegate, UITableViewDataSource>
/// timePickView
@property (nonatomic, strong) UIDatePicker *timePickView;
/// uiview
@property (nonatomic, strong) UIView *headView;
/// tableView
@property (nonatomic, strong) UITableView *tableView;
/// 模型数据
@property (nonatomic, strong) NSMutableArray *modalArray;
/// 时间格式化
@property (nonatomic, strong) NSDateFormatter *dateFormatter;
@end

@implementation STEditClockViewController
#pragma mark - life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupInit];
    
    if (self.isAddClock) {
        [self loadData];
    }else{
        [self loadDataFormEdit];
    }
    
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    self.headView.frame = CGRectMake(0, START_Y, self.view.hj_width, PICKHEIGHT);
    self.timePickView.frame = self.headView.bounds;
    self.tableView.frame = CGRectMake(0, CGRectGetMaxY(self.headView.frame), self.view.hj_width, 132);
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self replaceLeftButtonImage:nil Title:@"Cancel"];
    [self replaceRightButtonImage:nil Title:@"Save"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - 初始化
- (void)setupInit
{
    self.view.backgroundColor = [UIColor blackColor];
    
    self.headView = [[UIView alloc] init];
    self.headView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:self.headView];
    self.NaviTitle.text = self.isAddClock ? @"Add Alarm" : @"Edit Alarm";
    //self.NaviTitle.textColor = [UIColor whiteColor];
    
    self.timePickView = [[UIDatePicker alloc] init];
    self.timePickView.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];
    self.timePickView.datePickerMode = UIDatePickerModeTime;
    [self.headView addSubview:self.timePickView];
    
    UIView *lineView = [UIFactory createViewWithFrame:CGRectMake(0,PICKHEIGHT-0.5, SCREEN_WIDTH, 0.5) backgroundColor:RGBHEX_(#e3e3e3)];
    [self.headView addSubview:lineView];
    
    [self setPickViewTextColor];
    
    // tableView
    self.tableView = [[UITableView alloc] init];
    self.tableView.separatorColor = RGBHEX_(#e3e3e3);
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.scrollEnabled = NO;
    self.tableView.separatorInset = UIEdgeInsetsZero;
    if ([self.tableView respondsToSelector:@selector(layoutMargins)]) {
        self.tableView.layoutMargins = UIEdgeInsetsZero;
    }
    [self.view addSubview:self.tableView];
    
    if (!self.clockModel) {
        self.clockModel = [[STNewClockModel alloc] init];
    }
}

- (void)setPickViewTextColor
{
    [self.timePickView setValue:[UIColor whiteColor] forKey:@"textColor"];
    //通过NSSelectorFromString获取setHighlightsToday方法
    SEL selector = NSSelectorFromString(@"setHighlightsToday:");
    //创建NSInvocation
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[UIDatePicker instanceMethodSignatureForSelector:selector]];
    BOOL no = NO;
    [invocation setSelector:selector];
    //setArgument中第一个参数的类picker，第二个参数是SEL，
    [invocation setArgument:&no atIndex:2];
    //让invocation执行setHighlightsToday方法
    [invocation invokeWithTarget:self.timePickView];
}

- (void)loadData
{
    self.modalArray = [NSMutableArray arrayWithCapacity:3];
    NSArray *titleArray = @[@"Repeat", @"Label", @"Ring"];
    NSArray *detaiTitleArray = @[@"Never", @"Alarm", @"Default"];
    self.clockModel.repetitionWeek = detaiTitleArray[0];
    self.clockModel.labelStr = detaiTitleArray[1];
    self.clockModel.ringStr = detaiTitleArray[2];
    
    NSInteger count = titleArray.count;
    for (NSInteger i = 0; i < count; i++) {
        NewClockEditModel *model = [[NewClockEditModel alloc] init];
        model.titleStr = titleArray[i];
        model.detaiStr = detaiTitleArray[i];
        model.editAccessoryType = UITableViewCellAccessoryNone;
        [self.modalArray addObject:model];
    }
}

- (void)loadDataFormEdit
{
    if (self.clockModel.timeStr.length) {
        self.dateFormatter.dateFormat = @"HH:mm";
        NSDate *seleteDate = [self.dateFormatter dateFromString:self.clockModel.timeStr];
        self.timePickView.date = seleteDate;
    }
    
    self.modalArray = [NSMutableArray arrayWithCapacity:3];
    NSArray *array = [self.clockModel.repetitionWeek componentsSeparatedByString:@" "];
    NSString *weekStr = self.clockModel.repetitionWeek;
    if (array.count == 7) {
        weekStr = @"Every day";
    }
    
    NewClockEditModel *weekModel = [NewClockEditModel clockEditModelWithTitle:@"Repeat" detaiStr:weekStr.length ? weekStr : @"Never"];
    [self.modalArray addObject:weekModel];
    NewClockEditModel *labelModel = [NewClockEditModel clockEditModelWithTitle:@"Label" detaiStr:self.clockModel.labelStr.length ? self.clockModel.labelStr : @"Alarm"];
    [self.modalArray addObject:labelModel];
    NewClockEditModel *ringModel = [NewClockEditModel clockEditModelWithTitle:@"Ring" detaiStr:self.clockModel.ringStr.length ? self.clockModel.ringStr : @"Default"];
    [self.modalArray addObject:ringModel];

    
    
    //MYLog(@"weekNumArray = %zd",self.clockModel.seleteWeekNumArray.count);
}

#pragma mark - event
- (void)navigationClickLeft
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

- (void)navigationClickRight
{
    NSString *oldIdentifier = self.clockModel.identifier;
    STNewClockModel *oldModel = nil;
    
    self.clockModel.sleteTime = [self.dateFormatter stringFromDate:self.timePickView.date];
    self.clockModel.identifier = [NSString stringWithFormat:@"%@-%@-%@",self.clockModel.labelStr,self.clockModel.repetitionWeek,self.clockModel.sleteTime];
    self.dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    self.clockModel.fireDateStr = [self.dateFormatter stringFromDate:self.timePickView.date];
    NSArray *loactionData = [LYGUserDBManager takeOutInDataBase:[STNewClockModel class]];
    BOOL isSave = YES;
    for (STNewClockModel *model in loactionData) {
        if ([model.identifier isEqualToString:self.clockModel.identifier]) {
            isSave = NO;
        }
        
        if ([model.identifier isEqualToString:oldIdentifier]) {
            oldModel = model;
        }
    }
    
    if (self.dismissFinshBlock && isSave) {
        // 移除旧通知
        NSArray *localNotifications = [UIApplication sharedApplication].scheduledLocalNotifications;
        UILocalNotification * notification = nil;
        
        for (UILocalNotification * notifi in localNotifications) {
            NSDictionary *userInfo = notifi.userInfo;
            NSString *userinfoStr = userInfo[oldIdentifier];
            if ([userinfoStr isEqualToString:oldIdentifier]) {
                notification = notifi;
                //MYLog(@"notifi = %@ userinfoStr = %@",notifi.fireDate,userinfoStr);
                [[UIApplication sharedApplication] cancelLocalNotification:notification];
            }
        }
        
        // 移除旧模型
        [LYGUserDBManager deleteModelInDatabase:oldModel];
        
        // 添加新的
        [self setAlarmDetialWithModel:self.clockModel];
        self.dismissFinshBlock(self.clockModel);
        // 数据库
        [LYGUserDBManager insertAndUpdateModelToDatabase:self.clockModel];
    }
    
//    if (!self.isAddClock) {
//        // 数据库
//        [LYGUserDBManager insertAndUpdateModelToDatabase:self.clockModel];
//    }
    
    [self navigationClickLeft];

}

#pragma mark - tableView data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.modalArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *ID = @"cell";
    NewClockEditModel *model = self.modalArray[indexPath.row];
    UITableViewCell *CELL = nil;
    if (indexPath.row < 3) {
        CELL = [tableView dequeueReusableCellWithIdentifier:ID];
        if (!CELL) {
            CELL = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:ID];
            CELL.selectionStyle = UITableViewCellSelectionStyleNone;
            CELL.separatorInset = UIEdgeInsetsZero;
            if ([CELL respondsToSelector:@selector(layoutMargins)]) {
                CELL.layoutMargins = UIEdgeInsetsZero;
            }
            CELL.backgroundColor = [UIColor blackColor];
            CELL.textLabel.textColor = [UIColor whiteColor];
        }
        
        if (indexPath.row == 2) {
            CELL.accessoryType = UITableViewCellAccessoryNone;
        }else{
            CELL.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        
        CELL.textLabel.text = model.titleStr;
        CELL.detailTextLabel.text = model.detaiStr;
        
    }else{
        STClockRemindCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([STClockRemindCell class])];
        if (!cell) {
            cell = [[STClockRemindCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:NSStringFromClass([STClockRemindCell class])];
        }
        cell.dataModel = model;
        CELL = cell;
    }
    return CELL;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NewClockEditModel *model = self.modalArray[indexPath.row];
    if (indexPath.row == 0) { // 重复时间选择
        STWeekSeleteViewController *weekVC = [[STWeekSeleteViewController alloc] init];
        weekVC.checkBlackBlock = ^(NSMutableArray *weekNumArray, NSString *seleteWeekStr){
            model.detaiStr = seleteWeekStr;
            self.clockModel.repetitionWeek = model.sleteWeekStr;
            //NSLog(@"repetitionWeek = %@",self.clockModel.repetitionWeek);
            self.clockModel.seleteWeekNumArray = weekNumArray;
            [tableView reloadData];
            model.sleteWeekStr = nil;
        };
        weekVC.editModel = model;
        [self.navigationController pushViewController:weekVC animated:YES];
    }else if (indexPath.row == 1){
        STClockLabelViewController *labelVC = [[STClockLabelViewController alloc] init];
        labelVC.checkBlackBlock = ^(){
            self.clockModel.labelStr = model.detaiStr;
            [tableView reloadData];
        };
        labelVC.editModel = model;
        [self.navigationController pushViewController:labelVC animated:YES];
    }
}

#pragma mark - lazy
- (NSDateFormatter *)dateFormatter
{
    if (!_dateFormatter) {
        _dateFormatter = [[NSDateFormatter alloc] init];
        _dateFormatter.dateFormat = @"HH:mm";
    }
    return _dateFormatter;
}

#pragma mark ---
-(void)setAlarmDetialWithModel:(STNewClockModel *)clockModel{
 
     //设定时间
     
     //取得当前用户的逻辑日历阳历
     
     NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
     
     //通过已定义的日历对象，获取某个时间点的NSDateComponents表示，并设置需要表示哪些信息（NSYearCalendarUnit, NSMonthCalendarUnit, NSDayCalendarUnit等）
     
     NSUInteger unitFlags =  NSCalendarUnitWeekOfMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    
     //NSDateFormatter *dat=[[NSDateFormatter alloc]init];
     self.dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
     NSDate *alarmDate = [self.dateFormatter dateFromString:clockModel.fireDateStr];
    
     NSDateComponents *dateComponent=[calendar components:unitFlags fromDate:alarmDate];//选中时间
    
     NSDate *nowDat=[NSDate date];
    
     NSDateComponents *dateComNow=[calendar components:unitFlags fromDate:nowDat];
    
    
     [self.dateFormatter setDateFormat:@"HH:mm"];
     
     NSString *setTime = [self.dateFormatter stringFromDate:alarmDate];
     
     NSInteger hour = [dateComponent hour];
     
     NSInteger minute = [dateComponent minute];
     
     NSInteger alarmTim = hour*60*60+minute*60;
     
     NSInteger hournow = [dateComNow hour];
     
     NSInteger minNow = [dateComNow minute];
     
     NSInteger nowTim = hournow*60*60+minNow*60;
     
     NSInteger alarmT = 0;
     
     if (nowTim){
        alarmT = alarmTim - nowTim - 1;
     }else{
        alarmT = 24*60*60-(nowTim-alarmTim)+1;
     }
     
     NSInteger couT = 0;
     
     // 当前星期几
     NSDateComponents* comps =[calendar components:(NSWeekCalendarUnit | NSWeekdayCalendarUnit |NSWeekdayOrdinalCalendarUnit) fromDate:[NSDate date]];
     //MYLog(@"weekday = %zd",comps.weekday);
     
     
     NSInteger currentWeek = comps.weekday;
     
     if (currentWeek == 1) { // 星期天
         currentWeek = 7;
     }else{
         currentWeek -= 1;
     }
    
    NSMutableArray *array = [NSMutableArray array];
    
//    if ([clockModel.seleteWeekNumArray isKindOfClass:[NSString class]]) {
    
        NSArray *wkArray = [clockModel.repetitionWeek componentsSeparatedByString:@" "];
        
        if (wkArray.count > 0) {
            
            for (NSString *str in wkArray) {
                NSNumber *wkNum = nil;
                if ([str isEqualToString:@"Sun"]) {
                    wkNum = @7;
                }else if ([str isEqualToString:@"Sat"]){
                    wkNum = @6;
                }else if ([str isEqualToString:@"Fri"]){
                    wkNum = @5;
                }else if ([str isEqualToString:@"Thu"]){
                    wkNum = @4;
                }else if ([str isEqualToString:@"Wed"]){
                    wkNum = @3;
                }else if ([str isEqualToString:@"Tue"]){
                    wkNum = @2;
                }else if ([str isEqualToString:@"Mon"]){
                    wkNum = @1;
                }
                if (wkNum != nil) {
                    [array addObject:wkNum];
                }
                
            }
            
//        }else{
//            array = clockModel.seleteWeekNumArray;
//        }
        
        
    }
    
    if ([array isKindOfClass:[NSArray class]]) {
        if (array.count>0){
            for (NSNumber *weekNum in array){
                
                couT = [weekNum integerValue];
                
                if (currentWeek > couT) {
                    couT = 7 - currentWeek + couT;
                    MYLog(@"currentWeek = %zd , couT = %zd",currentWeek,couT);
                }else if (currentWeek == couT){
//                    NSComparisonResult result = [nowDat compare:alarmDate];
//                    if ( result == NSOrderedAscending) {
//                        couT = 7 - currentWeek + couT;
//                    }else{
//                        couT = couT - currentWeek;
//                    }
                    
                    if (alarmT < 0) {
                        couT = 7;
                    }else{
                        couT = 0;
                    }
                    
                }else{
                    couT = couT - currentWeek;
                }
                
                [self setNotiWith:couT withAlermm:alarmT andSetTime:setTime];//alarmT秒数
                
            }
        }else{
            if (alarmT < 0) {
                couT = 1;
            }else{
                couT = 0;
            }
            
            [self setNotiWith:couT withAlermm:alarmT andSetTime:setTime];//alarmT秒数
        }
        
    }else{
//        NSComparisonResult result = [nowDat compare:alarmDate];
//        couT = currentWeek;
//        if ( result == NSOrderedAscending || result == NSOrderedSame) {
//            couT = 0;//couT - currentWeek;
//        }else{
//            couT = 1;//7 - currentWeek + couT;
//        }
        if (alarmT < 0) {
            couT = 1;
        }else{
            couT = 0;
        }
        [self setNotiWith:couT withAlermm:alarmT andSetTime:setTime];
    }
    
    
     
 
 }
 
 -(void)setNotiWith:(NSInteger)count withAlermm:(NSInteger)alarmTime andSetTime:(NSString*)setTime
 
 {
 
     UILocalNotification * notifi=[[UILocalNotification alloc] init];
     
     if (notifi!=nil){
     
         notifi.fireDate=[NSDate dateWithTimeIntervalSinceNow:count*60*60*24+alarmTime];//多长时间后通知
         MYLog(@"fireDate = %@ count = %zd",notifi.fireDate,count);
         //修改原因，闹铃提醒是一周反复，不是一天反复
         
         notifi.repeatInterval= kCFCalendarUnitWeek; //一Mon次
         
         notifi.timeZone=[NSTimeZone defaultTimeZone];
         
         notifi.applicationIconBadgeNumber = 1; //应用的红色数字
         
         notifi.soundName= UILocalNotificationDefaultSoundName;//声音
         notifi.soundName = @"callRing.caf";
         
         notifi.alertBody = self.clockModel.labelStr;//提示信息弹出提示框
         
         notifi.alertAction = self.clockModel.sleteTime;  //提示框按钮
     
         notifi.userInfo = [NSDictionary dictionaryWithObject:self.clockModel.identifier forKey:self.clockModel.identifier];
         
         [[UIApplication sharedApplication] scheduleLocalNotification:notifi];
     
         
         self.clockModel.notification = notifi;
     }
 
 }
 
 #pragma mark - 获取的重复时间
 /*
 -(void)repeatTime:(NSNotification*)noti{
 
     week_add=[noti object];
     
     _repeatTime_add=[ToolshowWeekDay:week_add];
     
     
     
     [tableView_addreloadData];
     
     int weekd=[Tool getweekdate];//获取当前是周几
     
     //分割字符串，获取所选是周几
     
     NSArray *timeA=[week_addcomponentsSeparatedByString:@"|"];
     
     int tima;
     
     int timA;
     
     for (NSString *tim in timeA)
     
     {
     
         tima=[tim intValue]+1; //星期几
         
         if (tima >weekd) //
         
         {
         
         timA=tima-weekd;
         
         }else if (tima
         
         {
         
         timA = (7-weekd)+tima;
         
         }
         
         else
         
         {
         
         timA = 0;
         
         }
         
         NSString *seleTim=[NSStringstringWithFormat:@"%d",timA]
         
         ​        [_couTim addObject:seleTim];
         
         }
 
 }
  */


@end
