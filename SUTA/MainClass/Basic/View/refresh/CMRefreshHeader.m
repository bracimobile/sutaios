//
//  CMRefreshHeader.m
//  CuteManger
//
//  Created by 张海军 on 16/4/24.
//  Copyright © 2016年 navyzhj. All rights reserved.
//

#import "CMRefreshHeader.h"
#import "LYGRefreshHeadView.h"

@interface CMRefreshHeader ()
/** headView */
//@property (nonatomic, strong) LYGRefreshHeadView *headView;

@end

@implementation CMRefreshHeader

//- (void)prepare
//{
//    [super prepare];
//    self.stateLabel.hidden = YES;
//    self.lastUpdatedTimeLabel.hidden = YES;
//    self.arrowView.hidden = YES;
//    self.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
//    self.headView = [[LYGRefreshHeadView alloc] init];
//    self.headView.backgroundColor = [UIColor clearColor]; //RGBHEX_(ececec);
//    [self addSubview:_headView];
//    self.backgroundColor = [UIColor clearColor]; //RGBHEX_(ececec);
//}
//
//- (void)placeSubviews
//{
//    [super placeSubviews];
//    self.hj_height = REFRESHHEADVIEWH + 10;
//    CGFloat height = REFRESHHEADVIEWH;
//    _headView.frame = CGRectMake(0, 10, self.mj_w, height);
//    
//    for (UIView *view in self.subviews) {
//        
//        if ([view isKindOfClass:[UIActivityIndicatorView class]]) {
//            [view removeFromSuperview];
//            break;
//        }
//    }
//    self.arrowView.hidden = YES;
//    self.backgroundColor = [UIColor clearColor]; //RGBHEX_(ececec);
//}
//
//
//- (void)scrollViewContentOffsetDidChange:(NSDictionary *)change
//{
//    [super scrollViewContentOffsetDidChange:change];
//    
//    NSInteger offsetY = (NSInteger)self.scrollView.contentOffset.y;
//    if (self.state == MJRefreshStateRefreshing ) {
//        [self.headView refreshViewLoadingWithOffsetY:offsetY];
//    }else if (self.state == MJRefreshStatePulling) {
//        [self.headView refreshViewPullingWithOffsetY:offsetY];
//    }else if (self.state == MJRefreshStateWillRefresh){
//        [self.headView refreshViewLoadingWithOffsetY:offsetY];
//    }else if(self.state == MJRefreshStateIdle) {
//        [self.headView refreshViewStateIdle];
//    }
//  
//}




@end
