//
//  STClockModel.m
//  SUTA
//
//  Created by 张海军 on 2017/3/22.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import "STClockModel.h"

@implementation STClockModel

+ (instancetype)clockModelWithTitle:(NSString *)title detaiT:(NSString *)detaiStr
{
    STClockModel *model = [[self alloc] init];
    model.titleName = title;
    model.detaiStr = detaiStr;
    return model;
}

@end
