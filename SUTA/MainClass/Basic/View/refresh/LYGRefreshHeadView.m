//
//  LYGRefreshHeadView.m
//  LeYaoGO
//
//  Created by 张海军 on 16/11/8.
//  Copyright © 2016年 ZLB. All rights reserved.
//

#import "LYGRefreshHeadView.h"

static NSString *loadingKey = @"loadingKey";

@interface LYGRefreshHeadView ()
/// 图片
@property (nonatomic, strong) UIImageView *animationImageView;
/// 阴影view
@property (nonatomic, strong) CAShapeLayer *shapeLayer;
@end

@implementation LYGRefreshHeadView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.animationImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"animation_refesh"]];
        [self addSubview:self.animationImageView];
        
        //[self creationOvalShadow];
        
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat imageW = self.animationImageView.hj_width;
    CGFloat imageH = self.animationImageView.hj_height;
    CGFloat imageX = (self.hj_width - imageW) * 0.5;
    CGFloat imageY = self.hj_height - imageH - 8.8;
    self.animationImageView.frame = CGRectMake(imageX, imageY, imageW, imageH);
    _shapeLayer.frame = CGRectMake((self.hj_width - 20) * 0.5 + 0.6, self.hj_height - 11, 20, 12);
}

- (void)creationOvalShadow
{
    UIBezierPath *_shapePath=[UIBezierPath bezierPathWithOvalInRect:CGRectMake(0, 0, 20, 9)];
    _shapeLayer = [CAShapeLayer layer];
    
    //_shapeLayer.position = CGPointMake(SCREEN_WIDTH * 0.5, REFRESHHEADVIEWH - 5.5); //self.center;
    _shapeLayer.path = _shapePath.CGPath;
    _shapeLayer.fillColor = RGBHEX_(d1d1d1).CGColor;
    [self.layer addSublayer:_shapeLayer];
    
}

- (void)refreshViewLoadingWithOffsetY:(NSInteger)offsetY
{
    if ([self.animationImageView.layer animationForKey:loadingKey]) {
        return;
    }
    
    if (!self.shapeLayer) {
        [self creationOvalShadow];
    }
    
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animation];
    animation.keyPath = @"position.y";
    animation.values = @[@1.2,@33,@2.2]; //@[@10,@40,@11];
    animation.duration = 0.8;
    animation.autoreverses = YES;
    animation.repeatCount = MAXFLOAT;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.removedOnCompletion = NO;
    
    CAKeyframeAnimation *shapAnimation = [CAKeyframeAnimation animation];
    shapAnimation.keyPath = @"transform.scale";
    shapAnimation.values = @[@0.2,@1.0];
    shapAnimation.duration = 0.42;
    shapAnimation.autoreverses = YES;
    shapAnimation.repeatCount = MAXFLOAT;
    shapAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    shapAnimation.removedOnCompletion = NO;
    
    [self.animationImageView.layer addAnimation:animation forKey:loadingKey];
    [self.shapeLayer addAnimation:shapAnimation forKey:nil];
}


- (void)refreshViewPullingWithOffsetY:(NSInteger)offsetY
{
//    NSLog(@"offsetY = %zd",offsetY);
    [self.animationImageView.layer removeAllAnimations];
    [self.shapeLayer removeFromSuperlayer];
    self.shapeLayer = nil;
    [UIView animateWithDuration:1.0 animations:^{
        self.animationImageView.y = 1.2; //10;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)refreshViewStateIdle
{
    [self.animationImageView.layer removeAllAnimations];
    [self.shapeLayer removeFromSuperlayer];
    self.shapeLayer = nil;
    //[UIView animateWithDuration:1.0 animations:^{
//        self.animationImageView.y = - (REFRESHHEADVIEWH + 30);
    //}];
    
}

@end
