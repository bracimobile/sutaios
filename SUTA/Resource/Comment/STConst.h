//
//  STConst.h
//  SUTA
//
//  Created by 张海军 on 2017/3/22.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import <Foundation/Foundation.h>

/// 当前选中的型号
UIKIT_EXTERN  NSString * CURRENT_SELETE_SUTATYPE;

/// 当前是否长按大于6s
UIKIT_EXTERN  BOOL longTapMoreSix;
