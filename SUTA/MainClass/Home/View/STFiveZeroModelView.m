//
//  STFiveZeroModelView.m
//  SUTA
//
//  Created by 张海军 on 2017/3/23.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import "STFiveZeroModelView.h"

@interface STFiveZeroModelView ()
/// 床的位置  默认为左边
@property (nonatomic, strong) NSString *defBedSide;
/// 定时器
@property (nonatomic, weak) NSTimer *longTapTimer;
/// 长按时间
@property (nonatomic, assign) NSInteger longTimes;
/// 所有按钮集合
@property (nonatomic, strong) NSMutableArray *buttonArray;
/** 当前选中的指示灯 */
@property (nonatomic, strong) UIView *currentSeleteLight;

/// 针对右边选中的按钮
@property (nonatomic, strong) UIButton *rightSeleteButton;

/// 是否是点击了 2按钮 且在3秒内取消了
@property (nonatomic, assign) BOOL isCancelWithThr;
/// 是否是点击了 2按钮 且在3 - 6秒内取消了
@property (nonatomic, assign) BOOL isCancelWithThrToSix;
/// 是否是点击了 2按钮 且在6s后
@property (nonatomic, assign) BOOL isCancelWithSixMore;
/// 是否是点击了 2 号 亮灯按钮
@property (nonatomic, assign) BOOL isSelete2button;
@end

@implementation STFiveZeroModelView

- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.seleteLongButtonCount = 1;
        [self setupInit];
    }
    return self;
}

//布局子View
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self setBackgroundColor:[UIColor clearColor]];
    //黑色背景
    
    self.blackBg.frame= CGRectMake(leftMargin, 0, Blackwidth,Blackheight);
    
    //10 light
    self.lightTen.frame = CGRectMake(LightTenX, LightTenY, LightTenWidth, LightTenHeight);
    self.lightTenLabel.frame = CGRectMake(LightTenLabelX, LightTenLabelY, LightTenLabelWidth, LightTenLabelHeight);
    
    
    //20 light
    self.lightTwenty.frame = CGRectMake(LightTwentyX, LightTwentyY, LightTwentyWidth, LightTwentyHeight);
    self.lightTwentyLabel.frame = CGRectMake(LightTwentyLabelX, LightTwentyLabelY, LightTwentyLabelWidth, LightTwentyLabelHeight);
    //30 light
    self.lightThirty.frame = CGRectMake(LightThirtyX, LightThirtyY, LightThirtyWidth, LightThirtyHeight);
    self.lightThirtyLabel.frame = CGRectMake(LightThirtyLabelX, LightThirtyLabelY, LightThirtyLabelWidth, LightThirtyLabelHeight);
    
    self.btnHeadShake.frame =CGRectMake(headShakeX, headShakeY, headShakeWidth, headShakeHeight);
    self.btnLight.frame = CGRectMake(switchModeX, switchModeY, switchModeWidth, swithcModeHeight);
    self.btnFootShake.frame =CGRectMake(footShakeX, footShakeY, footShakeWidth, footShakeHeight);
    self.btnl.frame = CGRectMake(btnlX, btnlY, btnlWidth, btnlHeight);
    self.btnll.frame = CGRectMake(btnllX, btnllY, btnllWidth, btnllHeight);
    self.btnlll.frame =CGRectMake(btnlllX, btnlllY, btnlllWidth, btnlllHeight);
    self.btnBackUp.frame = CGRectMake(backUpX, backUpY, backUpWidth, backUpHeight);
    self.btnBackDown.frame = CGRectMake(backDownX, backDownY, backDownWidth, backDownHeight);
    self.btnZeroG.frame = CGRectMake(zerogX, zerogY, zerogWidth, zerogHeight);
    self.btnMemory.frame = CGRectMake(memoryX, memoryY, memoryWidth, memoryHeight);
    self.btnFootUp.frame = CGRectMake(footUpX, footUpY, footUpWidth, footUpHeight);
    self.btnFootDown.frame = CGRectMake(footDownX, footDownY, footDownWidth, footDownHeight);
    self.btnMinus.frame = CGRectMake(btnMinusX, btnMinusY, btnMinusWidth, btnMinusHeight);
//    self.btnHeadUp.frame =CGRectMake(headUpX, headUpY, headUpWidth, headUpHeight);
//    self.btnHeadDown.frame =CGRectMake(headDownX, headDownY, headDownWidth, headDownHeight);
//    self.btnWaistUp.frame =CGRectMake(waistUpX, waistUpY, waistUpWidth, waistUpHeight);
//    self.btnWaistDown.frame =CGRectMake(waistDownX, waistDownY, waistDownWidth, waistDownHeight);
    self.btnLeft.frame =CGRectMake(btnLX, btnLY, btnLWidth, btnLHeight);
    self.btnCenterL.frame =CGRectMake(btnCenterLX, btnCenterLY
                                      , btnCenterLWidth, btnCenterLHeight);
    self.btnRight.frame =CGRectMake(btnRX, btnRY, btnRWidth, btnRHeight);
    
    
    //背景图片
    self.bgImgView.frame= CGRectMake(imgBgX, imgBgY, imgBgWidth,imgBgHeight);
    
    //黑色图片底部
    CGFloat bgImgBottomheight = centerScreenHeight*0.14;
    self.bgImgBottomView.frame = CGRectMake(leftMargin, Blackheight, Blackwidth,bgImgBottomheight);
    
    
    self.ivLIRBg.frame =CGRectMake(LIRBgX, LIRBgY, LIRBgWidth,LIRBgHeight);
    
    self.ivLIR.frame = CGRectMake(LIRX, LIRY, LIRWidth, LIRHeight);
    
    
}



#pragma mark - 初始化
- (void)setupInit
{
    self.buttonArray = [NSMutableArray arrayWithCapacity:20];
    // 设置床的位置 默认左边
    self.defBedSide = LEFT_AND_RIGHT_BED;
    
    //黑色背景
    self.blackBg =[[UIView alloc] init];
    self.blackBg.backgroundColor = [UIColor blackColor];
    [self addSubview:self.blackBg];
    
    //底部黑色的
    self.bgImgBottomView = [[UIImageView alloc] initWithImage:[UIImage ImageIpadOrIphoneName:@"frame03"]];
    [self addSubview:self.bgImgBottomView];
    
    // 右边的LIR 背景 音量键
    self.ivLIRBg = [[UIImageView alloc] initWithImage:[UIImage ImageIpadOrIphoneName:@"bg"]];
    [self addSubview:self.ivLIRBg];
    
    //三个灯, 10 20 30
    self.lightTen = [self flashlightView];
    self.lightTenLabel = [self flashlightLableWithTitle:@"10"];
    
    self.lightTwenty = [self flashlightView];
    self.lightTwentyLabel = [self flashlightLableWithTitle:@"20"];
    
    self.lightThirty = [self flashlightView];
    self.lightThirtyLabel = [self flashlightLableWithTitle:@"30"];
    
    
    // 加载每一个点击的button
    //headShake
    self.btnHeadShake = [self creatButtonWithHighlighteImageName:@"head_shake" SeleteImage:nil tag:1];
    //message headshake 旁边的按钮
    self.btnLight = [self creatButtonWithHighlighteImageName:@"massage" SeleteImage:nil tag:2];
    // foot shake
    self.btnFootShake = [self creatButtonWithHighlighteImageName:@"foot_shake" SeleteImage:nil tag:3];;
    // btn l
    self.btnl = [self creatButtonWithHighlighteImageName:@"M1" SeleteImage:nil tag:4];
    // btn ll
    self.btnll = [self creatButtonWithHighlighteImageName:@"massage" SeleteImage:nil tag:5];
    //btn lll
    self.btnlll = [self creatButtonWithHighlighteImageName:@"M2" SeleteImage:nil tag:6];
    //btn back up
    self.btnBackUp = [self creatButtonWithHighlighteImageName:@"back_up" SeleteImage:nil tag:7];
    //btn back down
    self.btnBackDown = [self creatButtonWithHighlighteImageName:@"back_down" SeleteImage:nil tag:8];
    //zerog
    self.btnZeroG = [self creatButtonWithHighlighteImageName:@"zerog" SeleteImage:nil tag:9];
    //memory
    self.btnMemory = [self creatButtonWithHighlighteImageName:@"zerog" SeleteImage:nil tag:10];
    //foot up
    self.btnFootUp = [self creatButtonWithHighlighteImageName:@"foot_up" SeleteImage:nil tag:11];
    //foot down
    self.btnFootDown = [self creatButtonWithHighlighteImageName:@"foot_down" SeleteImage:nil tag:12];
    // btnMinus  btn －号
    self.btnMinus = [self creatButtonWithHighlighteImageName:@"light" SeleteImage:nil tag:13];
    //head up btn
//    self.btnHeadUp = [self creatButtonWithHighlighteImageName:@"head_up" SeleteImage:nil tag:14];
//    //head down btn
//    self.btnHeadDown = [self creatButtonWithHighlighteImageName:@"head_down" SeleteImage:nil tag:15];
//    //waist up Btn
//    self.btnWaistUp = [self creatButtonWithHighlighteImageName:@"waist_up" SeleteImage:nil tag:16];
//    //waist down Btn
//    self.btnWaistDown = [self creatButtonWithHighlighteImageName:@"waist_down" SeleteImage:nil tag:17];
    
    // left btn L
    self.btnLeft = [self creatButtonWithHighlighteImageName:@"L" SeleteImage:@"L" tag:18];
    // centerL btn l
    self.btnCenterL = [self creatButtonWithHighlighteImageName:@"I" SeleteImage:@"I" tag:19];
    // right btn R
    self.btnRight = [self creatButtonWithHighlighteImageName:@"R" SeleteImage:@"R" tag:20];
    
    
    //背景图片
    self.bgImgView = [[UIImageView alloc] initWithImage:[UIImage ImageIpadOrIphoneName:@"frame02"]];
    [self addSubview:self.bgImgView];
    
    // 右边的LIR 文字
    self.ivLIR = [[UIImageView alloc] initWithImage:[UIImage ImageIpadOrIphoneName:@"LIR"]];
    [self addSubview:self.ivLIR];
    
}


// 闪光灯
- (UIView *)flashlightView
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = HJColor(178, 143, 99, 0.4);
    [self addSubview:view];
    return view;
}

// 闪光灯旁边的 文字
- (UILabel *)flashlightLableWithTitle:(NSString *)title
{
    UILabel *label = [[UILabel alloc] init];
    label.textColor = HJColor(178, 143, 99, 1);
    label.text = title;
    label.font = [UIFont systemFontOfSize:12];
    [self addSubview:label];
    return label;
}


// 创建按钮
- (UIButton *)creatButtonWithHighlighteImageName:(NSString *)bgName SeleteImage:(NSString *)selName tag:(NSInteger)tag;
{
    UIButton *button = [[UIButton alloc] init];
    if (bgName.length) {
        UIImage * headShakeImg = [UIImage ImageIpadOrIphoneName:bgName];
        [button setBackgroundImage:headShakeImg forState:UIControlStateHighlighted | UIControlStateNormal];
    }
    
    if (selName.length) {
        UIImage *seleImage = [UIImage ImageIpadOrIphoneName:selName];
        [button setBackgroundImage:seleImage forState:UIControlStateSelected];
    }
    
    button.tag = tag;
    
   
    //增加点击按下的事件
    [button addTarget:self action:@selector(btnPressedDown:) forControlEvents:UIControlEventTouchDown];
    //增加点击抬起的事件
    //[button addTarget:self action:@selector(btnPressedUp:) forControlEvents:UIControlEventTouchUpInside];
    // 点击取消事件
    [button addTarget:self action:@selector(btnPressedCancel:) forControlEvents:UIControlEventTouchUpInside | UIControlEventTouchUpOutside];
    [self addSubview:button];
    [self.buttonArray addObject:button];
    
    return button;
}


#pragma mark - 定时器事件
- (void)addLongTapTime
{
    self.longTimes += 1;
    
    if (self.longTimes < 3) {
        
    }else if(self.longTimes >= 3 && self.longTimes < 6){
        
        self.isCancelWithThrToSix = YES;
        if (self.lightAnimBlock) {
            self.lightAnimBlock(self.currentSeleteLight);
            self.isAnimationing = YES;
        }
        
    }else if (self.longTimes >= 6){
        //[self setLightColorForIndex:self.seleteLongButtonCount];
        self.isCancelWithSixMore = YES;
        NSData *data =[SutaGlobalData combineCommand:COMMAND
                                       middleCommand:_defBedSide
                                         lastCommand:DATA_RECOVER_MEMORY_A];
        
        [[STBlueManage blueManageShare] writeCurrentPeripheralWihtData:data];
        
        [self setLightColorForIndex:self.seleteLongButtonCount];
        
    }
 //   NSLog(@"longTimes = %zd",self.longTimes);
}

#pragma mark - event
- (void)btnPressedDown:(UIButton *)button
{
    self.isSelete2button = NO;
    for (UIButton *btn in self.buttonArray) {
        if (button != btn && btn.tag != 18 && btn.tag != 19 && btn.tag != 20) {
            btn.userInteractionEnabled = NO;
        }
    }
    
    if (button.tag == 18 || button.tag == 19 || button.tag == 20) {
        button.selected = YES;
        self.rightSeleteButton.selected = NO;
        self.rightSeleteButton = button;
    }
    
    MYLog(@"tag = %zd",button.tag);
    NSInteger type = button.tag;
    NSData *data = nil;
    NSString *bineCommandStr = nil;
    NSString *lastCommandStr = nil;
    
    if (type == 1) {
        bineCommandStr = COMMAND;
        lastCommandStr = DATA_HEAD_SHAKE;
    }else if (type == 2){
 //       NSLog(@"点击开始");
        self.isSelete2button = YES;
        bineCommandStr = COMMAND;
        lastCommandStr = DATA_TIME_ALL_OFF;
        self.isCancelWithThrToSix = NO;
        [self setLightColorForIndex:self.seleteLongButtonCount];
        self.longTimes = 1;
        self.longTapTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(addLongTapTime) userInfo:nil repeats:YES];
    }else if (type == 3){
        bineCommandStr = COMMAND;
        lastCommandStr = DATA_FOOT_SHAKE;
    }else if (type == 5){
        bineCommandStr = COMMAND;
        lastCommandStr = DATA_SHAKE_MODEL_ll;
    }else if (type == 7){
        bineCommandStr = COMMAND;
        lastCommandStr = DATA_BACK_UP;
    }else if (type == 8){
        bineCommandStr = COMMAND;
        lastCommandStr = DATA_BACK_DOWN;
    }else if (type == 10){
       
        if(self.longTimes < 3 && !self.isCancelWithThrToSix){
            bineCommandStr = COMMAND;
            lastCommandStr = DATA_POSITION_MEMORY;
            
        }else if(self.isCancelWithThrToSix){
            self.isCancelWithThrToSix = NO;
            self.isCancelWithSixMore = NO;
            bineCommandStr = COMMAND;
            lastCommandStr = DATA_SAVE_MEMORY;
            
            // 松开 全部恢复默认状态
            [self setLightColorForIndex:4];
//            self.seleteLongButtonCount = 1;
            
        }
        
    }else if (type == 11){
        bineCommandStr = COMMAND;
        lastCommandStr = DATA_FOOT_UP;
    }else if (type == 12){
        bineCommandStr = COMMAND;
        lastCommandStr = DATA_FOOT_DOWN;
    }else if (type == 13){
        bineCommandStr = COMMAND;
        lastCommandStr = DATA_FLAT;
    }else if (type == 18){
        self.defBedSide = LEFT_BED;
    }else if (type == 19){
        self.defBedSide = LEFT_AND_RIGHT_BED;
    }else if (type == 20){
        self.defBedSide = RIGHT_BED;
    }else if (type == 4) { // 是否是长按
        bineCommandStr = COMMAND;
        lastCommandStr = DATA_SHAKE_MODEL_l;
    
    }else if (type == 6){
        bineCommandStr = COMMAND;
        lastCommandStr = DATA_SHAKE_MODEL_lll;
        
    }else if (type == 9){
        
        if(self.longTimes < 3 && !self.isCancelWithThrToSix){
            bineCommandStr = COMMAND;
            lastCommandStr = DATA_ZEROG;
            
        }else if(self.isCancelWithThrToSix){
            self.isCancelWithThrToSix = NO;
            self.isCancelWithSixMore = NO;
            bineCommandStr = COMMAND;
            lastCommandStr = DATA_SAVE_ZEROG;
            
            // 松开 全部恢复默认状态
            [self setLightColorForIndex:4];
//            self.seleteLongButtonCount = 1;
        }
    }
    
    if (bineCommandStr.length && lastCommandStr.length) {
        data =[SutaGlobalData combineCommand:bineCommandStr
                               middleCommand:_defBedSide
                                 lastCommand:lastCommandStr];
    }
    
    if (data) {
        [[STBlueManage blueManageShare] writeCurrentPeripheralWihtData:data];
    }
    
    
}

- (void)btnPressedCancel:(UIButton *)button
{
    for (UIButton *btn in self.buttonArray) {
        if ( btn.tag != 18 && btn.tag != 19 && btn.tag != 20) {
            btn.userInteractionEnabled = YES;
        }
    }
    NSInteger type = button.tag;
    

    if (type == 2) {
        if(self.longTimes < 3){
    
//            self.seleteLongButtonCount += 1;
//            if (self.seleteLongButtonCount == 3) {
//                self.seleteLongButtonCount = 0;
//            }
//            
//            // 松开 全部恢复默认状态
//            [self setLightColorForIndex:4];
            
            self.isCancelWithThr = YES;
            
        }else if(self.longTimes >= 3 && self.longTimes < 6){
            self.isSelete2button = NO;
            
        }else if(self.isCancelWithSixMore){
//            self.isSelete2button = NO;
            // 松开 全部恢复默认状态
            [self setLightColorForIndex:4];
            self.seleteLongButtonCount = 1;
        }
        
        
    }else if (type == 6){ // M2 全部恢复不选中状态
        
    }
    
    if (self.longTapTimer) {
        [self.longTapTimer invalidate];
        self.longTapTimer = nil;
        self.longTimes = 1;
    }
    
    if (!longTapMoreSix) {
        [self sendMessage:upliftCommand];
    }
    
    //[self sendMessage:upliftCommand];
    
}

- (void)btnPressedUp:(UIButton *)button
{
    MYLog(@"tag = %zd",button.tag);
    
    [self sendMessage:upliftCommand];
}

- (void)sendMessage:(Byte[])dataMsg
{
 //   NSLog(@"abc sendMessage---- ");
    NSData *adata = [[NSData alloc] initWithBytes:dataMsg length:5];
    [[STBlueManage blueManageShare] writeCurrentPeripheralWihtData:adata];
}

//设置指示灯的颜色
//设置指示灯的颜色
- (void)setLightColorForIndex:(NSInteger) index
{
    if (index == 4 || self.isSelete2button) {
        if (self.currentSeleteLight) {
            [self.currentSeleteLight.layer removeAllAnimations];
            self.isAnimationing = NO;
        }
    }
    
    
    // 只有选择 2 号按钮
    if (self.isSelete2button) {
        
        if(index == 1){
            //第一个灯亮
            self.lightTen.alpha = 1;
            self.lightTwenty.alpha = 0.4;
            self.lightThirty.alpha = 0.4;
            self.currentSeleteLight = self.lightTen;
        }else if(index == 2){
            //第二个灯亮
            self.lightTen.alpha = 0.4;
            self.lightTwenty.alpha = 1;
            self.lightThirty.alpha = 0.4;
            self.currentSeleteLight = self.lightTwenty;
        }else if(index == 0){
            //第三个灯亮
            self.lightTen.alpha = 0.4;
            self.lightTwenty.alpha = 0.4;
            self.lightThirty.alpha = 1;
            self.currentSeleteLight = self.lightThirty;
        }
    }
    
    
    
    if (index == 4){ // 全部恢复默认状态
        self.lightTen.alpha = 0.4;
        self.lightTwenty.alpha = 0.4;
        self.lightThirty.alpha = 0.4;
    }
    
    if (self.isCancelWithThr) {
        self.isCancelWithThr = NO;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.15 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            self.lightTen.alpha = 0.4;
            self.lightTwenty.alpha = 0.4;
            self.lightThirty.alpha = 0.4;
        });
    }
}


/*
- (void)setLightColorForIndex:(NSInteger) index
{
    if (self.currentSeleteLight) {
        [self.currentSeleteLight.layer removeAllAnimations];
        self.isAnimationing = NO;
    }
    
    if(index == 1){
        //第一个灯亮
        self.lightTen.alpha = 1;
        self.lightTwenty.alpha = 0.4;
        self.lightThirty.alpha = 0.4;
        self.currentSeleteLight = self.lightTen;
    }else if(index == 2){
        //第二个灯亮
        self.lightTen.alpha = 0.4;
        self.lightTwenty.alpha = 1;
        self.lightThirty.alpha = 0.4;
        self.currentSeleteLight = self.lightTwenty;
    }else if(index == 0){
        //第三个灯亮
        self.lightTen.alpha = 0.4;
        self.lightTwenty.alpha = 0.4;
        self.lightThirty.alpha = 1;
        self.currentSeleteLight = self.lightThirty;
    }else if (index == 4){ // 全部恢复默认状态
        self.lightTen.alpha = 0.4;
        self.lightTwenty.alpha = 0.4;
        self.lightThirty.alpha = 0.4;
    }
    
    
}
*/
// 闪灯
- (void)lightAnimated
{
    UIView *view = nil;
    if (self.seleteLongButtonCount == 0) {
        view = self.lightThirty;
    }else if (self.seleteLongButtonCount == 1){
        view = self.lightTen;
    }else if (self.seleteLongButtonCount == 2){
        view = self.lightTwenty;
    }
    
    [UIView animateWithDuration:1.0 animations:^{
        [UIView setAnimationRepeatCount:MAXFLOAT];
        [UIView setAnimationRepeatAutoreverses:YES];
        //动画变化
        self.lightTen.alpha = 1;
        //[UIColor colorWithRed:178/255.0 green:143/255.0 blue:99/255.0 alpha:1];
        
    }];
}

- (void)setHidden:(BOOL)hidden
{
    [super setHidden:hidden];
    
    if (hidden) {
        if (self.longTapTimer) {
            [self.longTapTimer invalidate];
            self.longTapTimer = nil;
        }
        
        // 松开 全部恢复默认状态
        [self setLightColorForIndex:4];
        self.seleteLongButtonCount = 1;
    }
}


@end
