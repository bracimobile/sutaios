//
//  STPublicMethod.m
//  SUTA
//
//  Created by 张海军 on 17/3/21.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import "STPublicMethod.h"
#import <CoreText/CoreText.h>

@implementation STPublicMethod


+ (CGSize)sizeString:(NSString *)string WithFont:(UIFont *)font constrainedToSize:(CGSize)size {
    NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:font ,kCTFontAttributeName, nil];
    return [string boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
}


/**
 是否是iPad

 @return YES / NO
 */
+ (BOOL)isIPad
{
    NSString *deviceType = [[UIDevice currentDevice] model];
    //NSLog(@"deviceType = %@",deviceType);
    if ([deviceType isEqual:@"iPad"]) {
        return YES;
    }else{
        return NO;
    }
    
}

@end
