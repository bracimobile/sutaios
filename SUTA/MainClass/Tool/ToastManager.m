//
//  ToastManager.m
//  LEYAOGOSDK
//
//  Created by Chums on 2016/12/27.
//  Copyright © 2016年 baoqianli. All rights reserved.
//

#import "ToastManager.h"
#import <UIView+Toast.h>
@interface ToastManager ()
@property (nonatomic, strong) NSTimer *outTimer;    // 显示蒙版计时器
@property (nonatomic, strong) UIView *overlayView;  // 蒙版
@property (nonatomic, assign) BOOL isHave;          // 判断当前是否有蒙版
@end

static dispatch_once_t once;
static ToastManager *manager = nil;
@implementation ToastManager
+ (ToastManager *)shareInstance {
    dispatch_once(&once, ^{
        manager = [[ToastManager alloc] init];
    });
    return manager;
}
+ (void)showToastWithString:(NSString*)string{
    [SVProgressHUD dismiss];
    [[self shareInstance] showWithString:string];
}
- (void)showWithString:(NSString*)string{
    if ([string isKindOfClass:[NSNull class]]) {
        MYLog(@"Toast String:%@",string);
        string = [NSString stringWithFormat:@"操作失败"];
    }
    if (self.isHave) {
        [self.overlayView hideToasts];
        [self toastDismiss];
    }
    NSEnumerator *frontToBackWindows = [UIApplication.sharedApplication.windows reverseObjectEnumerator];
    for (UIWindow *window in frontToBackWindows) {
        BOOL windowOnMainScreen = window.screen == UIScreen.mainScreen;
        BOOL windowIsVisible = !window.hidden && window.alpha > 0;
        BOOL windowLevelSupported = (window.windowLevel >= UIWindowLevelNormal && window.windowLevel <= UIWindowLevelNormal);
        if(windowOnMainScreen && windowIsVisible && windowLevelSupported) {
            [window addSubview:self.overlayView];
            break;
        }
    }

    NSTimeInterval showTimes = [self toastDisplayDurationForString:string];
    [_overlayView makeToast:string duration:showTimes position:CSToastPositionCenter];
    self.isHave = YES;
    self.outTimer = [NSTimer timerWithTimeInterval:showTimes+0.3 target:self selector:@selector(toastDismiss) userInfo:nil repeats:NO];
    [[NSRunLoop mainRunLoop] addTimer:self.outTimer forMode:NSRunLoopCommonModes];
}
- (void)toastDismiss{
    [self.overlayView removeFromSuperview];
    self.overlayView = nil;
    self.isHave = NO;
}

#pragma mark - 懒加载
- (void)setOutTimer:(NSTimer *)outTimer{
    if(_outTimer) {
        [_outTimer invalidate], _outTimer = nil;
    }
    if(outTimer) {
        _outTimer = outTimer;
    }
}
- (UIView*)overlayView {
    if(!_overlayView) {
        _overlayView = [[UIView alloc] init];
        _overlayView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _overlayView.backgroundColor = [UIColor clearColor];
    }
    _overlayView.frame = [UIScreen mainScreen].bounds;

    return _overlayView;
}

/**
 动态计算显示时间

 @param string 要显示的字符串
 @return 时间
 */
- (NSTimeInterval)toastDisplayDurationForString:(NSString*)string {
    return (float)string.length * 0.06 + 0.5 > 0.75 ? (float)string.length * 0.06 + 0.5:0.75;
}
@end
