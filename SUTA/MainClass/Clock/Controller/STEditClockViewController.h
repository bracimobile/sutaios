//
//  STEditClockViewController.h
//  SUTA
//
//  Created by 张海军 on 2017/5/19.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import "STBaseViewController.h"
#import "STNewClockModel.h"

@interface STEditClockViewController : STBaseViewController

/// 是否是新增 默认为编辑
@property (nonatomic, assign) BOOL isAddClock;

/// 闹铃模型
@property (nonatomic, strong) STNewClockModel *clockModel;

/// 回调
@property (nonatomic, copy) void(^dismissFinshBlock)(STNewClockModel *clockModel);

@end
