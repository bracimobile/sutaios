//
//  STFiveZeroModelView.h
//  SUTA
//
//  Created by 张海军 on 2017/3/23.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STZeroModelBaseView.h"

@interface STFiveZeroModelView : STZeroModelBaseView

@property(strong,nonatomic) UIView  *blackBg;
@property(strong,nonatomic) UIImageView *bgImgView;
@property(assign) CGFloat imgHeight;

//底部黑色部分
@property(strong,nonatomic) UIImageView *bgImgBottomView;

// 10 20 30 指示灯
@property(strong,nonatomic) UIView  *lightTen;
@property(strong,nonatomic) UIView  *lightTwenty;
@property(strong,nonatomic) UIView  *lightThirty;

@property(strong,nonatomic) UILabel  *lightTenLabel;
@property(strong,nonatomic) UILabel  *lightTwentyLabel;
@property(strong,nonatomic) UILabel  *lightThirtyLabel;


@property (strong,nonatomic) UIImageView *ivLIRBg;


@property (strong,nonatomic) UIImageView *ivLIR;



@property (strong,nonatomic) UIButton *btnHeadShake;


@property (strong,nonatomic) UIButton *btnLight;


@property (strong,nonatomic) UIButton *btnFootShake;



@property (strong,nonatomic) UIButton *btnl;



@property (strong,nonatomic) UIButton *btnll;


@property (strong,nonatomic) UIButton *btnlll;



@property (strong,nonatomic) UIButton *btnBackUp;



@property (strong,nonatomic) UIButton *btnFlat;


@property (strong,nonatomic) UIButton *btnBackDown;


@property (strong,nonatomic) UIButton *btnZeroG;



@property (strong,nonatomic) UIButton *btnMemory;



@property (strong,nonatomic) UIButton *btnFootUp;


@property (strong,nonatomic) UIButton *btnFootDown;


@property (strong,nonatomic) UIButton *btnMinus;



@property (strong,nonatomic) UIButton *btnLeft;


@property (strong,nonatomic) UIButton *btnCenterL;


@property (strong,nonatomic) UIButton *btnRight;

/// 点击 tag == 2 按钮的次数  在 3秒内的
@property (nonatomic, assign) NSInteger seleteLongButtonCount;

//设置指示灯的颜色  1  2   0==3  4全部熄灭
- (void)setLightColorForIndex:(NSInteger) index;

- (void)sendMessage:(Byte[])dataMsg;

@end
