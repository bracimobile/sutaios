//
//  STBlueManage.m
//  SUTA
//
//  Created by 张海军 on 2017/3/23.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import "STBlueManage.h"


@interface STBlueManage () <CBCentralManagerDelegate,CBPeripheralDelegate>
/** 中心管理者 */
@property (nonatomic, strong) CBCentralManager *centralManager;

@property (nonatomic, strong) CBCharacteristic *writeCharacteristic;

/** 当前连接到的外围设备 */
@property (nonatomic, strong) CBPeripheral *currentConnentPeripheral;

/// 设备断开连接后的重连
@property (nonatomic, weak) NSTimer *reconnectionTimer;

/// 记录进入前台后是否需要继续重连
@property (nonatomic, assign) BOOL isConnentR;

/// 是否是主动断开连接
@property (nonatomic, assign) BOOL isAccordDisconnent;

/// 当期需要连接的型号
@property (nonatomic, copy) NSString *connentTypeName;

/// 是否是主动去断开前一个连接
@property (nonatomic, assign) BOOL isDisconnentPrior;

/// 是否是第一次
@property (nonatomic, assign) BOOL isFirest;

/// 是否需要去回调收到的data
@property (nonatomic, assign) BOOL isCallBackData;

@end

@implementation STBlueManage
+ (instancetype)blueManageShare
{
    static STBlueManage *manage = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manage = [[STBlueManage alloc] init];
        [manage setupInit];
    });
    return manage;
}

- (void)dealloc
{
    [self disconnect];
}

- (void)setupInit
{
    self.centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
    
    // 进入前台
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startTime) name:UIApplicationWillEnterForegroundNotification object:nil];
    
    // 进入后台
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(destroyTime) name:UIApplicationDidEnterBackgroundNotification object:nil];
}

- (void)startScanWitTypeName:(NSString *)typeName
{
    self.connentTypeName = typeName;
    if (self.centralManager.state == CBCentralManagerStatePoweredOff) {
        [ToastManager showToastWithString:@"Turn on Bluetooth, please"];
        [self destroyTime];
        return;
    }
    //[SVProgressHUD showWithStatus:@"搜索中..."];
    // 搜索外围设备
    [self.centralManager scanForPeripheralsWithServices:nil options:nil];
}

//连接外围设备
- (void)connect:(CBPeripheral *)peripheral
{
    if (self.currentPeripheral.state == CBPeripheralStateConnected && self.currentConnentPeripheral) {
        self.isDisconnentPrior = YES;
        [self.centralManager cancelPeripheralConnection:self.currentConnentPeripheral];
        self.currentConnentPeripheral = nil;
    }
    
    self.currentPeripheral = peripheral;
    [self.centralManager connectPeripheral:peripheral options:nil];
}

//断开连接
- (void)disconnect{
    //self.isAccordDisconnent = YES;
    if (self.currentPeripheral) {
        [self.centralManager cancelPeripheralConnection:self.currentPeripheral];
    }
}

- (void)stopToDetect{
    if (self.currentPeripheral) {
        [self.centralManager stopScan];
        [self disconnect];
    }
}

// 向当前外围设备写入数据
- (void)writeCurrentPeripheralWihtData:(NSData *)writeData
{
    if (self.currentConnentPeripheral) {
        [self.currentConnentPeripheral writeValue:writeData
                         forCharacteristic:self.writeCharacteristic
                                      type:CBCharacteristicWriteWithResponse];
    }
}

#pragma mark - aleart
- (void)showSetBuleAleart
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"请打开蓝牙来允许”SUTA“连接到配件" message:nil delegate:self cancelButtonTitle:@"好" otherButtonTitles:@"设置", nil];
    [alert show];
}

#pragma mark - CBCentralManagerDelegate
// 蓝牙状态
- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    self.blueState = central.state;
    switch (central.state) {
        case CBCentralManagerStateUnknown:
        {
            //NSLog(@"未知状态");
            break;
        }
        case CBCentralManagerStateResetting:
        {
            //NSLog(@"重设状态");
            break;
        }
        case CBCentralManagerStateUnsupported:
        {
            //NSLog(@"不支持状态");
            [ToastManager showToastWithString:@"Bluetooth is not supported for this device"];
            break;
        }
        case CBCentralManagerStateUnauthorized:
        {
            //NSLog(@"未授权状态状态");
            //[ToastManager showToastWithString:@"该设备对蓝牙未授权"];
            break;
        }
        case CBCentralManagerStatePoweredOff:
        {
            NSLog(@"关闭状态");
            if (self.didDisconnectPeripheralBlock) {
                self.didDisconnectPeripheralBlock(self.currentConnentPeripheral);
            }
            [ToastManager showToastWithString:@"The device Bluetooth is in shutdown"];
            break;
        }
        case CBCentralManagerStatePoweredOn:
        {
            if (CURRENT_SELETE_SUTATYPE.length) {
                [self startScanWitTypeName:CURRENT_SELETE_SUTATYPE];
                if(self.currentConnentPeripheral){
                    [self.centralManager connectPeripheral:self.currentConnentPeripheral options:nil];
                }
            }
            break;
        }
            
        default:
            break;
    }
}

// 发现外围设备
- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary<NSString *, id> *)advertisementData RSSI:(NSNumber *)RSSI
{
    NSString *name = peripheral.name;
 //   NSLog(@"name = %@, rssi = %zd, UUID = %@",peripheral.name,RSSI.integerValue,[peripheral.identifier UUIDString]);
    if ([name isEqualToString:_connentTypeName] && _connentTypeName.length) {
        CURRENT_SELETE_SUTATYPE = _connentTypeName;
        STBlueModel *model = [[STBlueModel alloc] init];
        model.peripheral = peripheral;
        model.peripheralName = peripheral.name;
        model.peripheralRssi = [RSSI integerValue];
        model.peripheralUUID = [peripheral.identifier UUIDString];
        if (self.scanConfromEquipmentBlock) {
            self.scanConfromEquipmentBlock(model);
        }
    }
}

// 中心设备 连接到外围设备
- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    NSString *oldUUID = [self.currentPeripheral.identifier UUIDString];
    NSString *newUUID = [peripheral.identifier UUIDString];
    MYLog(@"oldUUID = %@, newUUID = %@",oldUUID,newUUID);
    if ([oldUUID isEqualToString:newUUID]) {

        [self.reconnectionTimer invalidate];
        self.reconnectionTimer = nil;
        
        // 记录当前连接外围设备
        self.currentConnentPeripheral = peripheral;
        self.currentConnentPeripheral.delegate = self;
        [self.currentPeripheral discoverServices:nil];
        if (self.connetPeripheralSuccessBlock) {
            self.connetPeripheralSuccessBlock(self);
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:CONNETPERIPHERALSUCCESS object:nil];
//        NSString *title = peripheral.name.length ? [NSString stringWithFormat:@"%@ 设备连接成功",peripheral.name] : @"设备连接成功";
//        [ToastManager showToastWithString:title];
//        [self.centralManager stopScan];
        [SVProgressHUD dismiss];
    }
}

//连接到Peripherals-失败
-(void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    NSString *oldUUID = [self.currentPeripheral.identifier UUIDString];
    NSString *newUUID = [peripheral.identifier UUIDString];
    if ([oldUUID isEqualToString:newUUID]) {
        
        if (self.connectPeripheraldidFailBlock) {
            self.connectPeripheraldidFailBlock(peripheral);
        }
        [SVProgressHUD dismiss];
//        NSString *title = peripheral.name.length ? [NSString stringWithFormat:@"%@ 设备连接失败",peripheral.name] : @"设备连接失败";
//        [ToastManager showToastWithString:title];
    }
}

//Peripherals断开连接
- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error{
    
    // 如果断开的设备是当前连接的设备
    if (peripheral == self.currentConnentPeripheral) {
        [SVProgressHUD dismiss];
        self.isCallBackData = NO;
        if (self.didDisconnectPeripheralBlock) {
            self.didDisconnectPeripheralBlock(peripheral);
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:CONNETPERIPHERALDISCONNECT object:nil];
        self.isAccordDisconnent = NO;
        self.reconnectionTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(reconnectionPeripheral) userInfo:nil repeats:YES];
    }
    
}

#pragma mark - CBPeripheralDelegate
//扫描到Services
-(void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error{
    
    if (error){
        return;
    }
    
    for (CBService *service in peripheral.services) {
        if ([service.UUID isEqual:[CBUUID UUIDWithString:UUID_COMM_SERVICE]]) {
            [peripheral discoverCharacteristics:nil forService:service];
        }
    }
    
}

//扫描到Characteristics
-(void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error{
    
    if (error){
        return;
    }
    
    for (CBCharacteristic *characteristic in service.characteristics)
    {
        
        if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:UUID_WRITE_CHARACTERISTIC]]) {
            NSLog(@"找到这个写的特征了--");
            //[peripheral readValueForCharacteristic:characteristic];
            // read the characteristic’s value，回调didUpdateValueForCharacteristic
            [peripheral readValueForCharacteristic:characteristic];
            self.writeCharacteristic = characteristic;
            
//            if (!self.isCallBackData) {
//                NSData *data =[SutaGlobalData combineCommand:COMMAND
//                                               middleCommand:LEFT_AND_RIGHT_BED
//                                                 lastCommand:DATA_TIME_ALL_OFF];
//                [peripheral writeValue:data forCharacteristic:characteristic type:CBCharacteristicWriteWithResponse];
//                self.isFirest = YES;
//            }
        }
        
        if ([characteristic.UUID isEqual:[CBUUID UUIDWithString:UUID_NOTIFY_CHARACTERISTIC]]) {
            NSLog(@"找到这个notify的特征了--");
            // read the characteristic’s value，回调didUpdateValueForCharacteristic
            //[peripheral readValueForCharacteristic:characteristic];
            //Subscribing to a Characteristic’s Value 订阅
            [peripheral setNotifyValue:YES forCharacteristic:characteristic];
        }
        
    }
    
}

//订阅的特征值有新的数据时回调
- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic
             error:(NSError *)error {
    if (error) {
        NSLog(@"Error changing notification state: %@",
              [error localizedDescription]);
    }
    
    [peripheral readValueForCharacteristic:characteristic];
    
}


#pragma mark - 蓝牙打通后,蓝牙设备回来的值 didUpdateValueForCharacteristic
// 获取到特征的值时回调
-(void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{

    NSData *data = characteristic.value;

    NSString *dataString = [self ConvertToNSString:data];
    
    NSString *inge = [NSString stringWithFormat:@"%@",dataString];
    
    NSInteger stringL = inge.length - 1;
    
    NSString *lastVaule = nil;
    
    if (inge.length > 0 && stringL > 0) {
        lastVaule = [inge substringFromIndex:stringL];
    }
    
  //  NSLog(@"inge = %@, stringL = %zd lastVaule = %@ data = %@",inge,stringL,lastVaule,data);
    // && !self.isCallBackData
    if (self.connectPeripherCallBack && data != nil ) {
        
        self.isCallBackData = YES;
        self.connectPeripherCallBack(lastVaule.integerValue);
        
        if (longTapMoreSix && self.sixSBlock) {
            self.sixSBlock();
        }
    }

}


// 向当前外围设备写入数据后的回调
- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(nullable NSError *)error
{
    if (!error) {
        NSLog(@"写入数据成功 characteristic = %@",characteristic);
    }
}

#pragma mark - 重连机制
- (void)reconnectionPeripheral
{
    if (self.blueState == CBManagerStatePoweredOff) {
        [self destroyTime];
    }
    [self startScanWitTypeName:CURRENT_SELETE_SUTATYPE];
    [self.centralManager connectPeripheral:self.currentConnentPeripheral options:nil];
}

// 销毁定时器
- (void)destroyTime
{
    if (self.reconnectionTimer) {
        self.isConnentR = YES;
        [self.reconnectionTimer invalidate];
        self.reconnectionTimer = nil;
    }
    
}

// 开始定时器
- (void)startTime
{
    if (self.isConnentR) {
        self.isConnentR = NO;
        self.reconnectionTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(reconnectionPeripheral) userInfo:nil repeats:YES];
    }
}

-(NSString *)ConvertToNSString:(NSData *)data
{
    
    
    NSMutableString *strTemp = [NSMutableString stringWithCapacity:[data length]*2];
    
    
    const unsigned char *szBuffer = [data bytes];
    
    
    for (NSInteger i=0; i < [data length]; ++i) {
        
        [strTemp appendFormat:@"%02lx",(unsigned long)szBuffer[i]];
        
    }
    
    return strTemp;
    
}

@end
