//
//  STBlueModel.h
//  SUTA
//
//  Created by 张海军 on 2017/3/23.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import <Foundation/Foundation.h>
@class CBPeripheral;

@interface STBlueModel : NSObject
/// peripheral 周围设备
@property (nonatomic, strong) CBPeripheral *peripheral;
/// 当前设备名称
@property (nonatomic, copy) NSString *peripheralName;
/// 当前设备UUID
@property (nonatomic, copy) NSString *peripheralUUID;
/// 当前设置rssi值
@property (nonatomic, assign) NSInteger peripheralRssi;
@end
