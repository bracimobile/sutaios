//
//  STZeroModelBaseView.h
//  SUTA
//
//  Created by 张海军 on 2017/5/22.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface STZeroModelBaseView : UIView

/** 闪关灯 */
@property (nonatomic, copy) void(^lightAnimBlock)(UIView *view);
/// 是否正在动画
@property (nonatomic, assign) BOOL isAnimationing;
/// 当前动画view
@property (nonatomic, weak) UIView *currentAnimView;

@end
