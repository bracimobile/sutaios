//
//  STClockModel.h
//  SUTA
//
//  Created by 张海军 on 2017/3/22.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import "STBaseModel.h"

@interface STClockModel : STBaseModel

/// detaiStr
@property (nonatomic, copy) NSString *detaiStr;

+ (instancetype)clockModelWithTitle:(NSString *)title detaiT:(NSString *)detaiStr;

@end
