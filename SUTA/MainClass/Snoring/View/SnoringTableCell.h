//
//  SnoringTableCell.h
//  SUTA
//
//  Created by Pavan Jangid on 29/01/18.
//  Copyright © 2018 ZLB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SnoringTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UISwitch *btnSwitch;

@end
