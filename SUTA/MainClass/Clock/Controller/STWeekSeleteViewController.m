//
//  STWeekSeleteViewController.m
//  SUTA
//
//  Created by 张海军 on 2017/5/19.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import "STWeekSeleteViewController.h"
#import "STNewClockModel.h"

@interface STWeekSeleteViewController () <UITableViewDelegate, UITableViewDataSource>
/// 模型数据
@property (nonatomic, strong) NSMutableArray *modelArray;
@end

@implementation STWeekSeleteViewController
#pragma mark - life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupInit];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    self.tableView.frame = CGRectMake(0, START_Y, self.view.hj_width, self.view.hj_height - START_Y);
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    NSInteger seleteNum = 0;
    NSMutableArray *weekNumArray = [NSMutableArray arrayWithCapacity:self.modelArray.count];
    self.editModel.sleteWeekStr = @"";
    NSString *seleteWeekStr = @"";
    for (ClockWeekSeleteModel *model in self.modelArray) {
        if (model.isSelete) {
            if (!self.editModel.sleteWeekStr.length) {
                self.editModel.sleteWeekStr = model.titleStr;
                seleteWeekStr = model.titleStr;
            }else{
                self.editModel.sleteWeekStr = [NSString stringWithFormat:@"%@ %@",self.editModel.sleteWeekStr,model.titleStr];
                
                seleteWeekStr = [NSString stringWithFormat:@"%@ %@",seleteWeekStr,model.titleStr];;
            }
            
            if (model.weekNum) {
                [weekNumArray addObject:model.weekNum];
            }
            
            seleteNum += 1;
        }
    }
    
    
    
    if (seleteNum == 7) {
        seleteWeekStr = @"Every day";
    }
    
//    if (self.editModel.sleteWeekStr.length) {
        if (self.checkBlackBlock) {
            if (seleteWeekStr.length <= 0) {
                seleteWeekStr = @"Never";
            }
            self.checkBlackBlock(weekNumArray,seleteWeekStr);
        }
//    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - 初始化
- (void)setupInit
{
    self.NaviTitle.text = @"Repeat";
    self.view.backgroundColor = [UIColor blackColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.scrollEnabled = NO;
    self.tableView.backgroundColor = [UIColor blackColor];
    [self.view addSubview:self.tableView];
    
    NSArray *titleArray = @[@"Sun", @"Mon", @"Tue", @"Wed", @"Thu", @"Fri", @"Sat"];
    self.modelArray = [NSMutableArray arrayWithCapacity:titleArray.count];
    NSArray *seleteWeek = [self.editModel.detaiStr componentsSeparatedByString:@" "];
    for (NSString *str in titleArray) {
        ClockWeekSeleteModel *model = [[ClockWeekSeleteModel alloc] init];
        model.titleStr = str;
        for (NSString *seleteStr in seleteWeek) {
            if ([seleteStr isEqualToString:str]) {
                model.isSelete = YES;
                break;
            }
        }
        
        if ([self.editModel.detaiStr isEqualToString:@"Every day"]) {
            model.isSelete = YES;
        }
        
        [self.modelArray addObject:model];
    }
    
}

#pragma mark - table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.modelArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *ID = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = RGB_(17, 17, 17);
        cell.textLabel.textColor = [UIColor whiteColor];
    }
    ClockWeekSeleteModel *model = self.modelArray[indexPath.row];
    cell.textLabel.text = model.titleStr; ///[NSString stringWithFormat:@"每%@",model.titleStr];
    if (model.isSelete) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ClockWeekSeleteModel *model = self.modelArray[indexPath.row];
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (model.isSelete) {
        cell.accessoryType = UITableViewCellAccessoryNone;
        model.isSelete = NO;
    }else{
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        model.isSelete = YES;
    }
}

@end
