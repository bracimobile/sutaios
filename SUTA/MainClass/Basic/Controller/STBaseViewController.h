//
//  STBaseViewController.h
//  SUTA
//
//  Created by 张海军 on 17/3/21.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface STBaseViewController : UIViewController
@property (nonatomic, strong) UIView *NavBackView;         //导航栏背景
@property (nonatomic, strong) UILabel *NaviTitle;          //中间文字
@property (nonatomic, strong) UIView *NavLineView;        // 导航栏阴影线
@property (nonatomic, strong) UIButton *NavLeftBtn;      //左键返回
@property (nonatomic, strong) UIButton *NavRightBtn;     //右键

-(void)replaceLeftButtonImage:(NSString *)imageName Title:(NSString *)title;

- (void)replaceRightButtonImage:(NSString *)imageName Title:(NSString *)title;

- (void)navigationClickLeft;

- (void)navigationClickRight;

@end
