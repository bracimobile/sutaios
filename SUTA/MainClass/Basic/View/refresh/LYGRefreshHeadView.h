//
//  LYGRefreshHeadView.h
//  LeYaoGO
//
//  Created by 张海军 on 16/11/8.
//  Copyright © 2016年 ZLB. All rights reserved.
//

#import <UIKit/UIKit.h>
static CGFloat REFRESHHEADVIEWH = 62.0;

@interface LYGRefreshHeadView : UIView

// 正在刷新的状态
- (void)refreshViewLoadingWithOffsetY:(NSInteger)offsetY;

// 松开既可以刷新的状态
- (void)refreshViewPullingWithOffsetY:(NSInteger)offsetY;

// 闲置状态
- (void)refreshViewStateIdle;

@end
