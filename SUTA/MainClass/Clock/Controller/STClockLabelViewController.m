//
//  STClockLabelViewController.m
//  SUTA
//
//  Created by 张海军 on 2017/5/19.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import "STClockLabelViewController.h"
#import "STNewClockModel.h"

@interface STClockLabelViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *TextFild;

@end

@implementation STClockLabelViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor blackColor];
    self.TextFild.delegate = self;
    self.NaviTitle.text = @"Label";
    self.TextFild.text = self.editModel.detaiStr;
    [self.TextFild becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.editModel.detaiStr = self.TextFild.text;
    if (self.checkBlackBlock) {
        self.checkBlackBlock();
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.navigationController popViewControllerAnimated:YES];
    return YES;
}

@end
