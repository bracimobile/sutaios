//
//  SnoringModel.h
//  SUTA
//
//  Created by Pavan Jangid on 28/01/18.
//  Copyright © 2018 ZLB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SnoringModel : NSObject
@property (nonatomic, copy) NSString *titleName;
@property (nonatomic, copy) NSString *cellType;
@end
