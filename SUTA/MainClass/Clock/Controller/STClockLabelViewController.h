//
//  STClockLabelViewController.h
//  SUTA
//
//  Created by 张海军 on 2017/5/19.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import "STBaseViewController.h"
@class NewClockEditModel;

@interface STClockLabelViewController : STBaseViewController

/// 编辑模型
@property (nonatomic, strong) NewClockEditModel *editModel;

/// 返回的回调
@property (nonatomic, copy) void(^checkBlackBlock)();

@end
