//
//  STNewClockViewController.m
//  SUTA
//
//  Created by 张海军 on 2017/5/18.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import "STNewClockViewController.h"
#import "STHomeTopView.h"
#import "STClockCell.h"
#import "STNewClockModel.h"
#import "STEditClockViewController.h"
#import "STNavigationController.h"

@interface STNewClockViewController ()<UITableViewDelegate, UITableViewDataSource>
/// topView
@property (nonatomic, strong) STHomeTopView *homeTopView;
/// 模型数据
@property (nonatomic, strong) NSMutableArray *modelArray;
/// 是否是编辑状态
@property (nonatomic, assign) BOOL isEditState;
@end

@implementation STNewClockViewController
#pragma mark - life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupInit];
    
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
//    self.homeTopView.frame = CGRectMake(0, 0, SCREEN_WIDTH, START_Y);
    self.tableView.frame = CGRectMake(0, START_Y, SCREEN_WIDTH, self.view.hj_height - START_Y);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - 
- (void)setupInit
{
    self.view.backgroundColor = [UIColor whiteColor];
    self.NaviTitle.text = @"Clock";
    [self replaceLeftButtonImage:nil Title:@"Edit"];
    [self replaceRightButtonImage:nil Title:@"Add"];
    
    // tableview
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.rowHeight = 90;
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([STClockCell class]) bundle:[NSBundle mainBundle]] forCellReuseIdentifier:NSStringFromClass([STClockCell class])];
    [self.view addSubview:self.tableView];
    
    if (!self.modelArray) {
        self.modelArray = [NSMutableArray array];
        NSArray *loactionData = [LYGUserDBManager takeOutInDataBase:[STNewClockModel class]];
        if (loactionData.count) {
            [self.modelArray addObjectsFromArray:loactionData];
        }else {
            self.NavLeftBtn.hidden = YES;
        }
    }
    
    [self setEmptyImage:nil text:@"No alarm set" buttonText:nil];
}


- (void)navigationClickLeft
{
//    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    if ([self.NavLeftBtn.titleLabel.text isEqualToString:@"Finish"]) {
        //[self.tableView setEditing:NO animated:NO];
        self.NavRightBtn.hidden = NO;
        self.isEditState = NO;
        for (STNewClockModel *model in self.modelArray) {
            model.isEdit = NO;
        }
        [self.tableView reloadData];
        [self replaceLeftButtonImage:nil Title:@"Edit"];
        return;
    }
    //[self.tableView setEditing:YES animated:YES];
    self.NavRightBtn.hidden = YES;
    self.isEditState = YES;
    for (STNewClockModel *model in self.modelArray) {
        model.isEdit = YES;
    }
    [self.tableView reloadData];
    [self replaceLeftButtonImage:nil Title:@"Finish"];
    
}

- (void)navigationClickRight
{
//    NSArray *localNotifications = [UIApplication sharedApplication].scheduledLocalNotifications;
//    
//    for (UILocalNotification *notification in localNotifications) {
//        NSLog(@"fireDate = %@, userInfo = %@",notification.fireDate,notification.userInfo);
//    }
    
    STEditClockViewController *editVC = [[STEditClockViewController alloc] init];
    editVC.isAddClock = YES;
    editVC.dismissFinshBlock = ^(STNewClockModel *clockModel){
        [self.modelArray addObject:clockModel];
        self.NavLeftBtn.hidden = NO;
        [self.tableView reloadData];
    };
    STNavigationController *NAV = [[STNavigationController alloc] initWithRootViewController:editVC];
    [self presentViewController:NAV animated:YES completion:nil];
}



#pragma mark - table view delegate / data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.modelArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    STClockCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([STClockCell class])];
    cell.indexPath = indexPath;
    STNewClockModel *dataModel = self.modelArray[indexPath.row];
    dataModel.isEdit = self.isEditState;
    cell.dataModel = dataModel;
    return cell;
}

- (NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    STNewClockModel *dataModel = self.modelArray[indexPath.row];
    UITableViewRowAction *delegateAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"删除" handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
        
        NSArray *localNotifications = [UIApplication sharedApplication].scheduledLocalNotifications;
        UILocalNotification * notification = nil;
        
        for (UILocalNotification * notifi in localNotifications) {
            NSDictionary *userInfo = notifi.userInfo;
            NSString *userinfoStr = userInfo[dataModel.identifier];
            if ([userinfoStr isEqualToString:dataModel.identifier]) {
                notification = notifi;
   //             NSLog(@"notifi = %@ userinfoStr = %@",notifi.fireDate,userinfoStr);
                [[UIApplication sharedApplication] cancelLocalNotification:notification];
            }
        }
        
        
        [LYGUserDBManager deleteModelInDatabase:dataModel];
        [self.modelArray removeObjectAtIndex:indexPath.row];
        
        if (self.modelArray.count == 0) {
            self.NavLeftBtn.hidden = YES;
        }
        
        [tableView reloadData];
    }];
    delegateAction.backgroundColor = [UIColor redColor];
    return @[delegateAction];
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (!self.isEditState) {
        return;
    }
    STNewClockModel *dataModel = self.modelArray[indexPath.row];
    STEditClockViewController *editVC = [[STEditClockViewController alloc] init];
    editVC.clockModel = dataModel;
    editVC.dismissFinshBlock = ^(STNewClockModel *clockModel){
        [self navigationClickLeft];
        [self.tableView reloadData];
    };
    STNavigationController *NAV = [[STNavigationController alloc] initWithRootViewController:editVC];
    [self presentViewController:NAV animated:YES completion:nil];
}


@end
