//
//  STClockRemindCell.h
//  SUTA
//
//  Created by 张海军 on 2017/5/19.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import <UIKit/UIKit.h>
@class NewClockEditModel;

@interface STClockRemindCell : UITableViewCell
/// 模型数据
@property (nonatomic, strong) NewClockEditModel *dataModel;

/// 打开开关的回调
@property (nonatomic, copy) void(^openSwitchBlock)(BOOL isOpen);

@end
