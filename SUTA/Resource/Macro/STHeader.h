//
//  STHeader.h
//  SUTA
//
//  Created by 张海军 on 17/3/21.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#ifndef STHeader_h
#define STHeader_h

// 打印
#ifdef DEBUG

#define MYLog(...) NSLog(__VA_ARGS__)

#else

#define MYLog(...)

#endif

/*************颜色**************/
#define RGB_(a,b,c) [UIColor colorWithRed:a/255.0f green:b/255.0f blue:c/255.0f alpha:1]
#define RGBHEX_(a) [CommenMethod RGBColor:@#a]
// 颜色
#define HJColor(r, g, b, a)  ([UIColor colorWithRed:(r) / 255.0 green:(g) / 255.0 blue:(b) / 255.0 alpha:(a)])
// 随机色
#define RandColor HJColor(arc4random_uniform(255), arc4random_uniform(255), arc4random_uniform(255),1)

#endif /* STHeader_h */

/*************系统版本**************/
#define iOS7 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
#define SystemVersion [[UIDevice currentDevice].systemVersion floatValue]
#define APPDELEGATE ((AppDelegate *)([[UIApplication sharedApplication] delegate]))
#define APPSHARE [UIApplication sharedApplication]

/*************屏幕尺寸**************/
#define SCREEN_HEIGHT [UIScreen mainScreen].bounds.size.height
#define STATUSHEIGHT  (iOS7?20:0)
#define SCREEN_WIDTH  [UIScreen mainScreen].bounds.size.width
#define START_Y (([[UIDevice currentDevice].systemVersion floatValue] >= 7.0) ? 64 : 44)
#define HEIGHT(view) view.frame.size.height
#define WIDTH(view)  view.frame.size.width

#define SysFont_(a) [UIFont systemFontOfSize:a]

// 弱引用
#define WS(weakSelf)  __weak __typeof(&*self)weakSelf = self;

#define KEYWINDOW  [UIApplication sharedApplication].keyWindow

// 用户数据库
#define LYGUserDBManager [DBManager sharedUserDataManager]

/*************常量宏**************/
#define NINEZERO_I  @"Series 900i"
#define FIVEZERO_I  @"Series 500i"

#define SUTA_900I   @"SUTA 900i"
#define SUTA_500I   @"SUTA 500i"

#define Mon     @"Mon"
#define Tue    @"Tue"
#define Wed  @"Wed"
#define Thu   @"Thu"
#define Fri     @"Fri"
#define Sat   @"Sat"
#define Sun    @"Sun"

/************** 通知 ******************/
#define CONNETPERIPHERALSUCCESS  @"CONNETPERIPHERALSUCCESS"  // 连接外围设备成功后的通知
#define CONNETPERIPHERALDISCONNECT  @"CONNETPERIPHERALDISCONNECT"  // 连接外围断开后的通知
#define BEGINSCARNDEVICE  @"BEGINSCARNDEVICE"  // 开始搜索设备

//#define SUTA_SOUND_DETECT_FIRST     4.0 // 1 First
//#define SUTA_SOUND_DETECT_SECOND    7.0 // 2 Seoncd
//#define SUTA_SOUND_DETECT_THIRD     15 // 2 Third time
//#define SUTA_SOUND_DETECT_FOURTH    19.0 // Fourth time

/*
 Please update the raising app timer for each stage as follow
 7%        - 3 seconds
 13.5%  - 4.2 seconds
 20%    -  8 seconds
 26.5%  - 12 seconds*/

#define SUTA_SOUND_DETECT_FIRST     4.0 // 1 First
#define SUTA_SOUND_DETECT_SECOND    5.2 // 2 Seoncd
#define SUTA_SOUND_DETECT_THIRD     9 // 2 Third time
#define SUTA_SOUND_DETECT_FOURTH    13.0 // Fourth time

#define SUTA_SOUND_DETECT_RESUME    60.0 * 3 // 60 seconds * 3 = 3mins
#define SUTA_SOUND_DETECT_STOP_HARDWARE    60.0 * 10 // 60 seconds * 10 = 10mins
