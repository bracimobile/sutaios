//
//  STClockCell.m
//  SUTA
//
//  Created by 张海军 on 2017/5/18.
//  Copyright © 2017年 ZLB. All rights reserved.
//

#import "STClockCell.h"
#import "STNewClockModel.h"

@interface STClockCell ()
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *detaiLabel;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImageView;
@property (weak, nonatomic) IBOutlet UISwitch *onOffSwitch;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *editWithConstraint;
@property (weak, nonatomic) IBOutlet UIButton *editButton;

@end

@implementation STClockCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.contentView.backgroundColor = RGB_(17, 17, 17);
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setDataModel:(STNewClockModel *)dataModel
{
    _dataModel = dataModel;
    
    self.timeLabel.text = dataModel.timeStr;
    self.detaiLabel.text = dataModel.detaiStr;
    
    self.onOffSwitch.hidden = dataModel.isEdit;
    self.onOffSwitch.on = !dataModel.isOpen;
    self.arrowImageView.hidden = !dataModel.isEdit;
    
}


- (IBAction)editButtonDidClick:(UIButton *)sender {
    if (self.deleteButtonDidClick) {
        self.deleteButtonDidClick(self.indexPath);
    }
}
- (IBAction)switchTouch:(UISwitch *)sender {

    self.dataModel.isOpen = !sender.on;
    
    if (sender.isOn) {
        
        [self setAlarmDetialWithModel:self.dataModel];
        
    }else{
        NSArray *localNotifications = [UIApplication sharedApplication].scheduledLocalNotifications;
        UILocalNotification * notification = nil;
        
        for (UILocalNotification * notifi in localNotifications) {
            NSDictionary *userInfo = notifi.userInfo;
            NSString *userinfoStr = userInfo[self.dataModel.identifier];
            if ([userinfoStr isEqualToString:self.dataModel.identifier]) {
                notification = notifi;
                MYLog(@"notifi = %@ userinfoStr = %@",notifi.fireDate,userinfoStr);
                [[UIApplication sharedApplication] cancelLocalNotification:notification];
            }
        }
    }
        
    // 数据库
    [LYGUserDBManager insertAndUpdateModelToDatabase:self.dataModel];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setAlarmDetialWithModel:(STNewClockModel *)clockModel{
    
    //设定时间
    NSDateFormatter *dat=[[NSDateFormatter alloc]init];
    //取得当前用户的逻辑日历阳历
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    //通过已定义的日历对象，获取某个时间点的NSDateComponents表示，并设置需要表示哪些信息（NSYearCalendarUnit, NSMonthCalendarUnit, NSDayCalendarUnit等）
    
    NSUInteger unitFlags =  NSCalendarUnitWeekOfMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    
    dat.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSDate *alarmDate = [dat dateFromString:clockModel.fireDateStr];
    
    
    NSDateComponents *dateComponent=[calendar components:unitFlags fromDate:alarmDate];//选中时间
    
    NSDate *nowDat=[NSDate date];
    
    NSDateComponents *dateComNow=[calendar components:unitFlags fromDate:nowDat];
    
    
    
    [dat setDateFormat:@"HH:mm"];
    
    NSString *setTime = [dat stringFromDate:alarmDate];
    
    NSInteger hour = [dateComponent hour];
    
    NSInteger minute = [dateComponent minute];
    
    NSInteger alarmTim = hour*60*60+minute*60;
    
    NSInteger hournow = [dateComNow hour];
    
    NSInteger minNow = [dateComNow minute];
    
    NSInteger nowTim = hournow*60*60+minNow*60;
    
    NSInteger alarmT = 0;
    
    if (nowTim){
        alarmT = alarmTim - nowTim - 1;
    }else{
        alarmT = 24*60*60-(nowTim-alarmTim)+1;
    }
    
    NSInteger couT = 0;
    
    // 当前星期几
    NSDateComponents* comps =[calendar components:(NSWeekCalendarUnit | NSWeekdayCalendarUnit |NSWeekdayOrdinalCalendarUnit) fromDate:[NSDate date]];
    //MYLog(@"weekday = %zd",comps.weekday);
    
    
    NSInteger currentWeek = comps.weekday;
    
    if (currentWeek == 1) { // 星期天
        currentWeek = 7;
    }else{
        currentWeek -= 1;
    }
    MYLog(@"currentWeek = %zd , couT = %zd",currentWeek,couT);
    NSMutableArray *array = [NSMutableArray array];
    
//    if ([clockModel.seleteWeekNumArray isKindOfClass:[NSString class]]) {
    
        NSArray *wkArray = [clockModel.repetitionWeek componentsSeparatedByString:@" "];
        
        if (wkArray.count > 0) {
            
            for (NSString *str in wkArray) {
                NSNumber *wkNum = nil;
                if ([str isEqualToString:@"Sun"]) {
                    wkNum = @7;
                }else if ([str isEqualToString:@"Sat"]){
                    wkNum = @6;
                }else if ([str isEqualToString:@"Fri"]){
                    wkNum = @5;
                }else if ([str isEqualToString:@"Thu"]){
                    wkNum = @4;
                }else if ([str isEqualToString:@"Wed"]){
                    wkNum = @3;
                }else if ([str isEqualToString:@"Tue"]){
                    wkNum = @2;
                }else if ([str isEqualToString:@"Mon"]){
                    wkNum = @1;
                }
                if (wkNum != nil) {
                    [array addObject:wkNum];
                }
                
            }
            
//        }else{
//            array = clockModel.seleteWeekNumArray;
//        }
        
        
    }
    
    if ([array isKindOfClass:[NSArray class]]) {
        if (array.count>0){
            for (NSNumber *weekNum in array){
                
                couT = [weekNum integerValue];
                
                if (currentWeek > couT) {
                    couT = 7 - currentWeek + couT;
                    MYLog(@"currentWeek = %zd , couT = %zd",currentWeek,couT);
                }else if (currentWeek == couT){
                    if (alarmT < 0) {
                        couT = 7;
                    }else{
                        couT = 0;
                    }
                    
                }else{
                    couT = couT - currentWeek;
                }
                
                [self setNotiWith:couT withAlermm:alarmT andSetTime:setTime];//alarmT秒数
                
            }
        }else{
//            if (hour > hournow) {
//                couT = 0 ;
//            }else if (hour == hournow){
//                if (minute > minNow) {
//                    couT = 0;
//                }else{
//                    couT = 1;
//                }
//            }else{
//                couT = 1;
//            }
            if (alarmT < 0) {
                couT = 1;
            }else{
                couT = 0;
            }
            [self setNotiWith:couT withAlermm:alarmT andSetTime:setTime];
            //[self setNotiWith:couT withAlermm:alarmT andSetTime:setTime];//alarmT秒数
        }
        
    }else{
//        if (hour > hournow) {
//            couT = 0 ;
//        }else if (hour == hournow){
//            if (minute > minNow) {
//                couT = 0;
//            }else{
//                couT = 1;
//            }
//        }else{
//            couT = 1;
//        }
        if (alarmT < 0) {
            couT = 1;
        }else{
            couT = 0;
        }
        [self setNotiWith:couT withAlermm:alarmT andSetTime:setTime];
    }
    
    
}

-(void)setNotiWith:(NSInteger)count withAlermm:(NSInteger)alarmTime andSetTime:(NSString*)setTime

{
    
    UILocalNotification * notifi=[[UILocalNotification alloc] init];
    
    if (notifi!=nil){
        
        notifi.fireDate=[NSDate dateWithTimeIntervalSinceNow:count*60*60*24+alarmTime];//多长时间后通知
        MYLog(@"fireDate = %@",notifi.fireDate);
        //修改原因，闹铃提醒是一周反复，不是一天反复
        
        notifi.repeatInterval= kCFCalendarUnitWeek; //一Mon次
        
        notifi.timeZone=[NSTimeZone defaultTimeZone];
        
        notifi.applicationIconBadgeNumber = 1; //应用的红色数字
        
        notifi.soundName= UILocalNotificationDefaultSoundName;//声音
        
        notifi.soundName = @"callRing.caf";
        notifi.alertBody = self.dataModel.labelStr;//提示信息弹出提示框
        
        notifi.alertAction = self.dataModel.sleteTime;  //提示框按钮
        
        notifi.userInfo = [NSDictionary dictionaryWithObject:self.dataModel.identifier forKey:self.dataModel.identifier];
        
        [[UIApplication sharedApplication] scheduleLocalNotification:notifi];
        
        
        self.dataModel.notification = notifi;
    }
    
}

@end
