//
//  CommenMethod.m
//  Etoou
//
//  Created by tentinet on 13-11-6.
//  Copyright (c) 2013年 Tentinet. All rights reserved.
//

#import "CommenMethod.h"
#import <sys/utsname.h>

@implementation CommenMethod

+(NSArray*) getCardType
{
    NSString * path=[[NSBundle mainBundle] pathForResource:@"CardType" ofType:@"plist"];
    NSArray * cardTypes =[NSArray arrayWithContentsOfFile:path];
    
    return cardTypes;
}


+(NSString *) getImageByName:(NSString*)imageName Type:(ImageSizeType) type
{
    NSString * result=nil;
    NSString *extension = [imageName pathExtension];
    if(extension)
    {
        result=[imageName stringByDeletingPathExtension];
        
    }
    
    switch (type) {
        case kIT150:
        {
            result=[NSString stringWithFormat:@"%@_150x150.%@",result,extension];
        }
            break;
        case kIT300:
        {
            result = [NSString stringWithFormat:@"%@_300x300.%@",result,extension];
        }
            break;
        case kIT450:
        {
            result=[NSString stringWithFormat:@"%@_450x450.%@",result,extension];
        }
            break;
        case kITNoraml:
        {
            result= [imageName copy];
        }
            break;
            
        default:
            break;
    }
    
    return  result;
}



+(NSString *) renameFile:(NSString*) filePath toType:(NSString *) type
{
    NSFileManager * mgr=[NSFileManager defaultManager];
    BOOL isDirectory = NO;
    
    if([mgr fileExistsAtPath:filePath isDirectory:&isDirectory])
    {
    
        NSString *  path=[filePath stringByDeletingLastPathComponent];
        NSString *  fileName=[[filePath lastPathComponent]stringByDeletingPathExtension];
        
        NSString * dePath=[[path stringByAppendingPathComponent:fileName]stringByAppendingPathExtension:type];
        
        NSError * error =nil;
        [mgr moveItemAtPath:filePath toPath:dePath error:&error];
        if(error)
        {
            NSLog(@"move file failed");
            return  nil;
        }
        
        
        return  dePath;
    }
    
    NSLog(@"file path not exist");
    
    return  nil;
}

+(UIImage*)  getImageWithName:(NSString*) fileName
{
    UIImage * image=nil;
    NSString * path = [[NSBundle mainBundle] pathForResource:fileName ofType:@"png"];
    if(path==nil)
        return  nil;
    if([[NSFileManager  defaultManager] fileExistsAtPath:path])
    {
        image =[[UIImage alloc] initWithContentsOfFile:path];
    }
    else
    {
        NSLog(@"image not exist at the path!");
    }
    
    return image;
}

+ (NSString *)trimString:(NSString *)str
{
    NSCharacterSet *charSet = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    NSString *trimString = [str stringByTrimmingCharactersInSet:charSet];
    return trimString;
}

+ (void)createDirectoryInSandBox:(NSString *)dircName
{
    NSString *path = [NSHomeDirectory() stringByAppendingFormat:@"/Documents/%@",dircName];
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    BOOL isDir = NO;
    BOOL isDirExist = [fileMgr fileExistsAtPath:path isDirectory:&isDir];
    if (!(isDirExist && isDir)) {
        BOOL bCreateDir = [fileMgr createDirectoryAtPath:path
                             withIntermediateDirectories:YES
                                              attributes:nil
                                                   error:nil];
        if (!bCreateDir) {
            NSLog(@"create directory failed");
        }
        else
        {
            NSLog(@"path:%@",path);
        }
    }
}

+ (NSString *)processNullString:(NSString *)string
{
    NSLog(@"string:%@",string);
    NSLog(@"string class:%@",[string class]);
    if ([string isKindOfClass:[NSString class]])
    {
        return string;
    }
    else
    {
        return nil;
    }
}

+ (BOOL)validatePassword:(NSString *)pwd
{
    NSString *str = @"([a-zA-Z0-9`~!@#$%^&*()\\-_=+{}\\[\\]|:;'\"<>,./?]){6,18}";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",str];
    BOOL result = [predicate evaluateWithObject:pwd];
    NSLog(@"result:%i",result);
    return result;
}

+ (void)debugMsg:(NSString *)message cls:(id)classObj file:(const char *)file function:(const char *)func line:(const unsigned int)line
{
    // log to console
    NSString *filePath = [NSString stringWithCString:file encoding:NSUTF8StringEncoding];

    //打印日志到控制台
    NSLog(@"Debug:%@",[NSString stringWithFormat:@"%@:%u %s %@",[filePath lastPathComponent], line, func, message]);
    
}

+ (NSString *)docPath
{
    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	return [paths objectAtIndex:0];
}


+ (NSString *)touch:(NSString *)path
{
	if ( NO == [[NSFileManager defaultManager] fileExistsAtPath:path] )
	{
		[[NSFileManager defaultManager] createDirectoryAtPath:path
								  withIntermediateDirectories:YES
												   attributes:nil
														error:NULL];
	}
	return path;
}

+ (NSInteger )securityInt:(id)param
{
    //防止[NSNull intValue]: unrecognized selector sent to instance 的crash问题
    
    
    
    if ([param isKindOfClass:[NSNull class]]) {
        return 0;
    }
    
    return [param integerValue];
}

+ (NSArray *)groupDataSource:(NSArray *)source numberOfColumn:(NSInteger)number
{
    NSMutableArray *groupedArr = [[NSMutableArray alloc] init];
    NSInteger count=ceil([source count] / number);
    for (NSInteger i = 0; i <= count; i++) {
        NSMutableArray *tempArr = [[NSMutableArray alloc] init];
        NSInteger location = i * number;
        NSInteger length = (i + 1) * number;
        
        //如果下标超出了原始数组的长度，那么lenth为剩下的长度
        length =  length > source.count ? source.count % number : length;
        
        //一行的数据记录
        NSArray *oneLineArr = [source objectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(location, length)]];
        [tempArr addObjectsFromArray:oneLineArr];
        [groupedArr addObject:tempArr];
    }
    return (NSArray *)groupedArr;
}

+ (BOOL)isPhoneNumber:(NSString *)string
{
//    NSString *str = @"^1\\d{10}$";
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",str];
//    BOOL result = [predicate evaluateWithObject:string];
//    return result;
    
    NSString *str = @"\\d*$";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",str];
    BOOL result = [predicate evaluateWithObject:string];
    return result;
}

+ (BOOL)NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *laxString = @".+@.+\\.[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

+ (NSString *)price:(NSString *)price
{
    return [NSString stringWithFormat:@"¥%@起",price];
}

+ (NSString *)sendboxTempFolder
{
    return [NSHomeDirectory() stringByAppendingPathComponent:@"tmp"];
}

+ (BOOL)saveImage:(UIImage *)image atPath:(NSString *)path fileName:(NSString *)fileName
{
    NSData *imageData = UIImageJPEGRepresentation(image, 1);
    BOOL result = [imageData writeToFile:[NSString stringWithFormat:@"%@/%@",path,fileName] atomically:NO];
    return result;
}


+ (NSString *)voiceLengthToString:(double)voiceLength
{
    NSLog(@"voiceLength:%f",voiceLength);
    NSString *length = [NSString stringWithFormat:@"%.0f",round(voiceLength * 1000)];
    NSLog(@"length:%@",length);
    return length;
}

+ (NSString *)formatVoiceLength:(NSString *)length
{
    if (length.intValue<1000) {
        length = @"1001";
    }
    NSInteger timeLength = length.intValue;
    
    
    NSString *lengthStr = nil;
    
    if (timeLength < 1) {
        lengthStr = @"0\"";
    }
    else
    {
        if (timeLength > 1000) {    //转换成秒数
            
            timeLength = timeLength / 1000;
            lengthStr = [NSString stringWithFormat:@"%li\"",(long)timeLength];
        }
        
        if (timeLength > 60) { //转换成分钟
            
            timeLength = timeLength / 60; //分钟
            NSInteger mm = timeLength % 60; //秒
            lengthStr = [NSString stringWithFormat:@"%li'%li\"",(long)timeLength,(long)mm];
        }
    }
    
    return lengthStr;
}

+ (NSInteger)extractDigit:(NSString *)string
{
    NSCharacterSet *nonDigits = [[NSCharacterSet decimalDigitCharacterSet] invertedSet];
    
    NSInteger value = [[string stringByTrimmingCharactersInSet:nonDigits] integerValue];
    
    return value;
}


+ (NSString *)deviceString
{
    // 需要#import "sys/utsname.h"
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *deviceString = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    
    if ([deviceString isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
    if ([deviceString isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    if ([deviceString isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    if ([deviceString isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    if ([deviceString isEqualToString:@"iPhone4,1"])    return @"iPhone 4S";
    if ([deviceString isEqualToString:@"iPhone5,1"])    return @"iPhone 5";
    if ([deviceString isEqualToString:@"iPhone5,2"])    return @"iPhone 5";
    if ([deviceString isEqualToString:@"iPhone5,3"])    return @"iPhone 5C";
    if ([deviceString isEqualToString:@"iPhone5,4"])    return @"iPhone 5C";
    if ([deviceString isEqualToString:@"iPhone6,1"])    return @"iPhone 5S";
    if ([deviceString isEqualToString:@"iPhone6,2"])    return @"iPhone 5S";
    if ([deviceString isEqualToString:@"iPhone7,1"])    return @"iPhone 6 Plus";
    if ([deviceString isEqualToString:@"iPhone7,2"])    return @"iPhone 6";
    if ([deviceString isEqualToString:@"iPhone8,2"])    return @"iPhone 6S Plus";
    if ([deviceString isEqualToString:@"iPhone8,1"])    return @"iPhone 6S";
    if ([deviceString isEqualToString:@"iPhone3,2"])    return @"Verizon iPhone 4";
    if ([deviceString isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
    if ([deviceString isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
    if ([deviceString isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
    if ([deviceString isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
    if ([deviceString isEqualToString:@"iPod5,1"])      return @"iPod Touch 5G";
    if ([deviceString isEqualToString:@"iPad1,1"])      return @"iPad";
    if ([deviceString isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
    if ([deviceString isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
    if ([deviceString isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
    if ([deviceString isEqualToString:@"iPad2,5"])      return @"iPad Mini";
    if ([deviceString isEqualToString:@"iPad4,1"])      return @"iPad Air";
    if ([deviceString isEqualToString:@"iPad4,2"])      return @"iPad Air";
    if ([deviceString isEqualToString:@"iPad4,4"])      return @"iPad Mini";
    if ([deviceString isEqualToString:@"iPad4,5"])      return @"iPad Mini";
    if ([deviceString isEqualToString:@"i386"])         return @"Simulator";
    if ([deviceString isEqualToString:@"x86_64"])       return @"Simulator";
    NSLog(@"NOTE: Unknown device type: %@", deviceString);
    return deviceString;
}

+ (NSString *)appVersion
{
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *app_Version = [infoDictionary objectForKey:(NSString *)kCFBundleVersionKey];  // 本地app版本
    
    return app_Version;
}

//转换#FFFFFF格式颜色
+ (UIColor *)RGBColor:(NSString *)string {
    if ([string rangeOfString:@"#"].location != NSNotFound) {
        string = [string stringByReplacingOccurrencesOfString:@"#" withString:@""];
    }
    
    // 十六进制字符串转成整形。
    long colorLong = strtoul([string cStringUsingEncoding:NSUTF8StringEncoding], 0, 16);
    // 通过位与方法获取三色值
    int R = (colorLong & 0xFF0000 )>>16;
    int G = (colorLong & 0x00FF00 )>>8;
    int B = colorLong & 0x0000FF;
    
    //string转color
    UIColor *wordColor = [UIColor colorWithRed:R/255.0 green:G/255.0 blue:B/255.0 alpha:1.0];
    return wordColor;
}

+ (NSData *)compressImage:(NSData *)originalData specifiedSize:(NSInteger)specifiedSize {
    if (originalData.length < specifiedSize) return originalData;
    
    UIImage *image = [UIImage imageWithData:originalData];
    
    CGSize itemSize = image.size;
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    CGFloat rate = MAX(screenSize.width /image.size.width, screenSize.height /image.size.height);
    if (rate < 1) {
        itemSize = CGSizeMake(image.size.width * rate, image.size.height * rate);
    }
    UIGraphicsBeginImageContextWithOptions(itemSize, NO,0.0);
    CGRect imageRect = CGRectMake(0.0, 0.0, itemSize.width, itemSize.height);
    [image drawInRect:imageRect];
    UIImage * resImage = UIGraphicsGetImageFromCurrentImageContext();
//    NSLog(@"out image size:%@", NSStringFromCGSize(resImage.size));
    UIGraphicsEndImageContext();
    
    CGFloat compression = 0.9f;
    CGFloat maxCompression = 0.1f;
    NSData *imageData = UIImageJPEGRepresentation(resImage, compression);
    while ([imageData length] > specifiedSize * 1024 && compression > maxCompression) {
        compression = compression * 0.8;
        imageData = UIImageJPEGRepresentation(resImage, compression);
    }
    
    return imageData;
}

/**
 //Added by Pavan
 * Convert time(seconds) to display format
 */

+(NSString *)getRemainTimeFormat:(NSTimeInterval)time{
    int seconds = time;
    NSString * strFinalTime =@"";
    NSString *strMin = @"";
    NSString *strSec = @"";
    if (seconds > 0) {
        int min = 0, sec = 0;

        if (seconds > 60) {
            min = (int)seconds / 60;
            seconds = seconds % 60;
        }
        
        sec = (int)seconds;
        
        //Formating
        strMin = [NSString stringWithFormat:@"%d",min];
        if (min < 10) {
            strMin = [NSString stringWithFormat:@"0%d",min];
        }
        
        strSec = [NSString stringWithFormat:@"%d",sec];
        if (sec < 10) {
            strSec = [NSString stringWithFormat:@"0%d",sec];
        }
        
        strFinalTime = [NSString stringWithFormat:@"%@:%@",strMin,strSec];
        return strFinalTime;
        
    } else {
        return @"";
    }
    
}

@end
