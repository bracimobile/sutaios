//
//  STWeekSeleteViewController.h
//  SUTA
//
//  Created by 张海军 on 2017/5/19.
//  Copyright © 2017年 ZLB. All rights reserved.
//  重复响铃时间选择

#import "STBaseListViewController.h"
@class NewClockEditModel;

@interface STWeekSeleteViewController : STBaseListViewController
/// 编辑模型
@property (nonatomic, strong) NewClockEditModel *editModel;

/// 返回的回调
@property (nonatomic, copy) void(^checkBlackBlock)(NSMutableArray *weekNumArray, NSString *seleteWeekStr);

@end
