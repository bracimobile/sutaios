//
//  NSData+TrimReturn.m
//  Diver
//
//  Created by tentinet on 14-4-9.
//  Copyright (c) 2014年 Tentinet. All rights reserved.
//

#import "NSData+TrimReturn.h"

@implementation NSData (TrimReturn)

-(NSData *) trimReturn
{
    
    NSString * str=[[NSString alloc] initWithData:self encoding:NSUTF8StringEncoding];
    str=[str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSData * result=[str dataUsingEncoding:NSUTF8StringEncoding];
    
    return  result;
}
@end
