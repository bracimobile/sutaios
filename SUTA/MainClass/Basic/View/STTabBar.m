//
//  LYGTabBar.m
//  LeYaoGO
//
//  Created by 张海军 on 16/11/3.
//  Copyright © 2016年 ZLB. All rights reserved.
//

#import "STTabBar.h"

@implementation STTabBar

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    for (UIControl *tabButton in self.subviews) {
        Class tabbar = NSClassFromString(@"UITabBarButton");
        
        if ([tabButton isKindOfClass:[tabbar class]]) {
            // 点击动画
            [tabButton addTarget:self action:@selector(tabBarButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        }
    }
}


- (void)tabBarButtonClick:(UIControl *)tabBarButton
{
    for (UIView *imageView in tabBarButton.subviews) {
        if ([imageView isKindOfClass:NSClassFromString(@"UITabBarSwappableImageView")]) {
            CAKeyframeAnimation *animation = [CAKeyframeAnimation animation];
            animation.keyPath = @"transform.scale";
            animation.values = @[@1.0,@1.3,@0.9,@1.15,@0.95,@1.02,@1.0];
            animation.duration = 0.5;
            animation.calculationMode = kCAAnimationCubic;
            [imageView.layer addAnimation:animation forKey:nil];
        }
    }
}

@end
